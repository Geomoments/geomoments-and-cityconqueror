package xjtlu.geomomentsdev;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.CameraPosition;
import com.amap.api.maps2d.model.Circle;
import com.amap.api.maps2d.model.CircleOptions;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.maps2d.model.MyLocationStyle;

import java.util.ArrayList;
import java.util.HashMap;

import xjtlu.geomomentsdev.tools.ImageHandle;
import xjtlu.geomomentsdev.tools.InternetConnectionHandler;
import xjtlu.geomomentsdev.tools.LocationDependentActivity;
import xjtlu.geomomentsdev.tools.LocationHandler;
import xjtlu.geomomentsdev.tools.Moment;
import xjtlu.geomomentsdev.tools.NoConnectionException;

/**
 * A fragment used as part of the MainNavigationActivity, showing Geomoments nearby on a Map.
 */
public class MapViewFragment extends Fragment implements LocationDependentActivity {
    private static int REQUEST_SELECT_CONTENT = 2;
    private static int REQUEST_PREVIEW_MOMENT = 3;
    public HashMap<Marker, Moment> markerMap = null;
    private AMap aMap = null;
    private ViewGroup container;
    private Circle circle;
    private MapView fragmentMapView;
    private boolean initialized = false;
    private boolean doneCreating;

    public MapViewFragment() {


    }

    /**
     * returns a new instance of the MapViewFragment.
     */
    public static MapViewFragment newInstance() {
        MapViewFragment fragment = new MapViewFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        this.container = container;
        View fragmentView = inflater.inflate(R.layout.fragment_map_view, container, false);
        doneCreating = false;
        Log.d("MapViewFragment", "CreatingMapViewFragment");
        //setup buttons
        //refresh Button
        FloatingActionButton refreshButton = (FloatingActionButton) fragmentView.findViewById(R.id.floating_button_map_view_fragment_refresh);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    plotData(true);
                } catch (Exception e) {
                    handleGetMomentsException(e);
                }


            }
        });
        // addMoment Button
        FloatingActionButton addMoment = (FloatingActionButton) fragmentView.findViewById(R.id.floating_button_map_view_fragment_map_add);
        addMoment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainNavigationActivity) container.getContext()).triggerSelectLocation();
            }
        });

        //recycle mapView if possible
        if (fragmentMapView != null) {
            fragmentMapView.onDestroy();
            fragmentMapView = null;
        }

        //setup mapView
        fragmentMapView = (MapView) fragmentView.findViewById(R.id.map_view_fragment_map_map);
        fragmentMapView.onCreate(savedInstanceState);


        setUpMap();

        LocationHandler.registerLocationDependentActivity(this);
     //   plotData(false);
        initialized = false;
        doneCreating = true;
        return fragmentView;
    }

    /**
     * sets up the fragmentMapView
     */
    private void setUpMap() {

        //recycle aMap if possible
        if (aMap != null) {
            aMap.clear();
            aMap.setMyLocationEnabled(true);
            aMap = fragmentMapView.getMap();

        }
        aMap = fragmentMapView.getMap();

        //set MyLocationStyle
        MyLocationStyle myLocationStyle = new MyLocationStyle();

        myLocationStyle.myLocationIcon(
                BitmapDescriptorFactory.fromBitmap(ImageHandle.bitmapFromVectorAsset(this.getContext(), R.drawable.ic_radio_button_checked_black_16dp,this.getResources().getColor(R.color.colorAccent))));
        //color standard circle transparent to hide it (circle with range is drawn later
        myLocationStyle.strokeColor(Color.TRANSPARENT);
        myLocationStyle.radiusFillColor(Color.TRANSPARENT);
        myLocationStyle.strokeWidth(1.0f);

        aMap.setLocationSource(LocationHandler.getInstance());
        aMap.getUiSettings().setMyLocationButtonEnabled(true);
        aMap.setMyLocationEnabled(true);

        aMap.setMyLocationStyle(myLocationStyle);

        //set marker click
        aMap.setOnMarkerClickListener(new AMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                Log.d("Click", "onMarkerClick: ");
                return true;

            }
        });



        //set info window that is shown on marker click
        aMap.setInfoWindowAdapter(new AMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                View infoWindow = getActivity().getLayoutInflater().inflate(
                        R.layout.marker_info_window, null);
                fillMomentInfoWindow(marker, infoWindow);
                return infoWindow;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View infoContent = getActivity().getLayoutInflater().inflate(
                        R.layout.marker_info_content, null);
                fillMomentInfoWindow(marker, infoContent);
                return infoContent;
            }
        });

        //hide all info windows when map is clicked
        aMap.setOnMapClickListener(new AMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                hideAllInfoWindows();

            }
        });


        //go to moment in browser on info window click
        aMap.setOnInfoWindowClickListener(new AMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                showMomentInBrowser(marker);
            }
        });

        //set map long click to create new moment
        aMap.setOnMapLongClickListener(new AMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                ((MainNavigationActivity) container.getContext()).triggerSelectLocation(latLng.latitude, latLng.longitude);
            }
        });


        //zoom in camera
        aMap.moveCamera(CameraUpdateFactory.zoomTo(18));


    }

    /**
     * Plots the given moments on the Map by creating marker.
     * @param moments  the list of moments to create markers on the map for.
     */
    public void plotOnMap(ArrayList<Moment> moments) {
        //destroy all existing markers
        if (markerMap != null) {
            for (Marker m : markerMap.keySet()) {
                m.destroy();
            }
        }
        markerMap = new HashMap<Marker, Moment>();

        //add marker for each moment
        if (moments != null && moments.size()>0) {
            for (Moment moment : moments) {
                Marker marker = createMarkerAtLocation(moment.getLocation(), (moment.getImageContent() != null));
                //add marker and moment to markerMap for later usage
                markerMap.put(marker, moment);
            }

            //show active moment infowindow
            showActiveMomentInfo();
        }
        initialized = true;

    }


    /**
     * gets the nearby moments from the LocationHandler and plots them on the map.
     * @param forceUpdate true if the nearby moments should be forced to be downloaded from the server, false if the current nearbyMoments of the LocationHandler should be used.
     */
    public void plotData(boolean forceUpdate) throws Exception{
        Log.d("MapView", "plotData : " + forceUpdate);

//        drawCircle(LocationHandler.getMyLat(), LocationHandler.getMyLng());
        if (LocationHandler.getInstance() != null && LocationHandler.getMyLat() != null && LocationHandler.getMyLng() != null) {
            //draw range circle accoarding to current location
            drawCircle(LocationHandler.getMyLat(), LocationHandler.getMyLng());
            //plot nearby moments on the map


                plotOnMap((LocationHandler.getMomentsNearby(this, forceUpdate)));



        }else{
            Log.d("plotDataMap", "location not available");
        }

    }


    /**
     * creates an image or text marker at the given location.
     * @param latLng the location to create the marker at.
     * @param hasImage true to create marker with image icon, false to create marker with text icon.
     * @return the newly created marker.
     */
    private Marker createMarkerAtLocation(LatLng latLng, boolean hasImage) {
        Marker marker = null;

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("test");

        //set marker icon to image or text respectively
        if (hasImage) {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageHandle.bitmapFromVectorAsset(getContext(), R.drawable.ic_image_black_24dp, R.color.colorAccent)));

        } else {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageHandle.bitmapFromVectorAsset(getContext(), R.drawable.ic_format_size_black_24dp, R.color.colorAccent)));
        }

        marker = aMap.addMarker(markerOptions);
        return (marker);

    }

    /**
     * Fills the moment infop window in the given view with the content of the moment represented by the given marker.
     * @param marker the marker representing the moment to display in the view.
     * @param view the info window to show the moment info in.
     */

    public void fillMomentInfoWindow(Marker marker, View view) {
        //get Moment represented by the given marker
        Moment moment = markerMap.get(marker);

        ImageView imageView = (ImageView) view.findViewById(R.id.image_view_marker_info_moment_image);
        TextView textView = (TextView) view.findViewById(R.id.text_view_marker_info_moment_text);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress_bar_marker_info_progressBar);

        //set image content if available
        if (moment.getImageContent() != null) {
            moment.getImageContent().showThisThumbImageInView(imageView, progressBar);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }

        //set text content if available
        if (moment.getTextContent() != null) {
            textView.setText(moment.getTextContent());
            textView.setTextSize(15);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }



    /**
     * draws a circle to illustrate the 200m range around the current location in the mapView
     * @param lat the latitude of the circle center.
     * @param lng the longitude of the circle center
     */
    private void drawCircle(double lat, double lng) {
        if (circle != null) {
            circle.remove();
        }
        if(aMap != null) {
            circle = aMap.addCircle(
                    new CircleOptions().center(new LatLng(lat, lng))
                            .strokeColor(Color.BLUE).strokeWidth(2).radius(LocationHandler.RANGE_IN_METERS)
            );
        }
    }


    /**
     * hides the info window of all markers on the map
     */
    private void hideAllInfoWindows() {
        if(markerMap != null){
        for (Marker m : markerMap.keySet()) {
            m.hideInfoWindow();

        }}
    }

    /**
     * shows the info window of the moment activated by show on map from the browser fragment or by being newly created.
     */
    private void showActiveMomentInfo() {
        if (container != null) {
            //take the id of the moment to show as active from the MainNavigation activity
            Long activeMoment = ((MainNavigationActivity) (container.getContext())).takeActiveMoment();

            if (activeMoment != null) {
                //find active moment in markerMap and show the info window of the corresponding marker
                for (Marker marker : markerMap.keySet()) {
                    if (markerMap.get(marker).getId() == activeMoment) {
                        marker.showInfoWindow();
                        aMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(marker.getPosition(), aMap.getCameraPosition().zoom)));
                        return;

                    }
                }
            }

        }
    }


    /**
     * shows the moment represented by the given marker in the BrowserFragment.
     * @param marker the Marker representing the Moment to show in the BrowserFragment.
     */
    public void showMomentInBrowser(Marker marker) {
        //get Moment for marker
        Moment moment = markerMap.get(marker);

        //set moment as active moment and show momentBrowserFragment
        ((MainNavigationActivity) (container.getContext())).setActiveMoment(moment.getId());
        ((ViewPager) container).setCurrentItem(MainNavigationActivity.SectionsPagerAdapter.POSITION_BROWSER_FRAGMENT);

    }

    /**
     * called if location updated in LocationHandler.
     * updates the range circle and plots new data when the location is updated.
     */
    @Override
    public void locationDependentRefresh() {
        try {
            plotData(false);
        } catch (Exception e) {
            handleGetMomentsException(e);
        }

        if (LocationHandler.getInstance() != null && LocationHandler.getMyLat() != null && LocationHandler.getMyLng() != null) {
            drawCircle(LocationHandler.getMyLat(), LocationHandler.getMyLng());

        } else {

            Toast.makeText(this.getContext(), getString(R.string.location_not_available), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void manageMomentsDownloadException(Exception e) {
        handleGetMomentsException(e);

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        //when set visible, plot new data if deprecated or ask for asap update
        if (isVisibleToUser) {

            if ((LocationHandler.isDeprecated(this) || !initialized)) {
                try {
                    plotData(false);
                }catch (Exception e){
                    handleGetMomentsException(e);
                    LocationHandler.updateMeAsap(this);
                }
            }
            showActiveMomentInfo();
        }
    }



    /**
     * shows a toast notification that moments could not be retrieved
     * @param e
     */
    public void handleGetMomentsException(Exception e){
        if(e instanceof  NoConnectionException){
            handleException((NoConnectionException) e);
        }else{
        e.printStackTrace();
        //TODO:: replace text with string value
   //     Toast.makeText(getContext(), "could not get nearby moments", Toast.LENGTH_SHORT).show();


    }}

    public void handleException(NoConnectionException e){
        e.printStackTrace(); InternetConnectionHandler.waitForConnection(getContext(), getActivity());
    }


    @Override
    public void onDestroyView() {
        //recycle Map and map view on destroy
        aMap.clear();
        aMap = null;
        fragmentMapView.onDestroy();
        markerMap = null;
        container = null;
        circle = null;
        initialized = false;
        doneCreating = false;
        //stop update on location changed
        LocationHandler.dropLocationDependentActivity(this);


        super.onDestroyView();
    }


}