package xjtlu.geomomentsdev;

import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.util.HashMap;

import xjtlu.geomomentsdev.tools.AsyncTaskResult;
import xjtlu.geomomentsdev.tools.HttpRequestHandler;
import xjtlu.geomomentsdev.tools.InternetConnectionHandler;
import xjtlu.geomomentsdev.tools.LocationHandler;
import xjtlu.geomomentsdev.tools.NoConnectionException;
import xjtlu.geomomentsdev.tools.PreferenceHandler;
import xjtlu.geomomentsdev.tools.ServerException;
import xjtlu.geomomentsdev.tools.User;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button registerButton = (Button) findViewById(R.id.button_register_register);

        InternetConnectionHandler.setContext(this);
        final AppCompatActivity findViewContext = this;

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressBar progressBar = (ProgressBar) findViewContext.findViewById(R.id.progress_bar_register_progress);
                LinearLayout layout = (LinearLayout)  findViewContext.findViewById(R.id.layout_register);
                layout.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                try {

                    if(inputIsValid()){
                        if(register()) {
                            //if registered successfully, go to MainNavigation
                            navigateToMainNavigation();

                        }
                    }
                } catch (Exception e) {

                    handleException(e);
                }finally{
                    progressBar.setVisibility(View.GONE);

                    layout.setVisibility(View.VISIBLE);
                }
            }
        });


        Button loginButton = (Button) findViewById(R.id.button_register_login);
        loginButton.setPaintFlags(loginButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToLogin();
            }
        });


    }


    /**
     * navigates to the main navigation activity, finishes this activity
     */
    private void navigateToMainNavigation(){

        Intent mainNavigation = new Intent(this, MainNavigationActivity.class);
        startActivity(mainNavigation);
        finish();


    }


    /**
     * navigates to the login activity, finishes this activity
     */
    private void navigateToLogin(){

        Intent login = new Intent(this, LoginActivity.class);
        startActivity(login);
        finish();


    }

    /**
     * registers a user with the given information, creating a new user in the database
     */
    private boolean register() throws Exception {

        EditText uniqueNameEdit = (EditText) findViewById(R.id.edit_text_register_username);
        EditText emailEdit = (EditText) findViewById(R.id.edit_text_register_email);
        EditText passwordEdit = (EditText) findViewById(R.id.edit_text_register_password1);

        //start location handler
        LocationHandler.instantiate(this.getApplicationContext());

        // set language
        PreferenceHandler.instantiate(this.getApplicationContext());

        User.initializeUserDatabase(this.getApplicationContext());

        if(uniqueNameEdit.getText() == null
               || emailEdit.getText() == null
                || passwordEdit.getText() == null){

            return false;
        }
        String uniqueName = uniqueNameEdit.getText().toString().trim();

        String email  = emailEdit.getText().toString().trim();

        String password = passwordEdit.getText().toString();


        AsyncTask<String, Void, AsyncTaskResult<Boolean>> registerTask = new AsyncTask<String, Void, AsyncTaskResult<Boolean>>() {
            @Override
            protected AsyncTaskResult<Boolean> doInBackground(String... taskParams) {
                HashMap<String, String> params = new HashMap<String, String>();

                params.put(HttpRequestHandler.SIGNUP_PARAMS[0],taskParams[0]);
                params.put(HttpRequestHandler.SIGNUP_PARAMS[1],taskParams[1]);
                params.put(HttpRequestHandler.SIGNUP_PARAMS[2],taskParams[2]);



                AsyncTaskResult<Boolean> result = new AsyncTaskResult<>();
                //send post request to server
                String response = null;
                try {

                    response = HttpRequestHandler.post(HttpRequestHandler.SIGNUP_URL, params);


                    User user = User.parseUser( new JSONObject(response));
                    Log.d("registerUser", user.toString());
                    User.getUserDatabase().insertOrUpdateUser(user);
                    PreferenceHandler.setLogin(true);
                    PreferenceHandler.setLoggedInUser(user.getId());

                    result.putResult(true);


                } catch (Exception e) {
                    result.addException(e);
                }
                finally{
                    return result;
                }
            }
        }.execute(uniqueName,email,password);
        AsyncTaskResult<Boolean> taskResult = registerTask.get();

        if(taskResult.getExceptions() != null){
            for(Exception e: taskResult.getExceptions()){
                throw e;
            }

            return false;
        }else{
            return taskResult.getResult();
        }




    }

    /**
     * validates the user input and sets error messages if necessary
     * @return true if the user input is valid for registration, false if not.
     */
    private boolean inputIsValid() throws Exception {
        //check if username is taken or email is taken

        EditText userNameEdit = (EditText) findViewById(R.id.edit_text_register_username);
        userNameEdit.setError(null);
        //validate username
        String userName;
        if(userNameEdit.getText() != null && ! userNameEdit.getText().toString().trim().matches("")){
            userName = userNameEdit.getText().toString().trim();
            //check if available


            //check if valid (long enough)
            if(!userNameValid(userName)){
                userNameEdit.setError(getString(R.string.error_username_invalid));
                userNameEdit.requestFocus();
                return false;
            }


        }else{
            //if not specified
            userNameEdit.setError(getString(R.string.error_username_invalid));

            return false;
        }

        EditText emailEdit = (EditText) findViewById(R.id.edit_text_register_email);
        emailEdit.setError(null);

        //validate email
        String email;
        if(emailEdit.getText() != null && !emailEdit.getText().toString().trim().matches("")){
            email = emailEdit.getText().toString().trim();



            //check if valid
            if(!isEmailValid(email)){
                emailEdit.setError(getString(R.string.error_invalid_email));
                emailEdit.requestFocus();
                return false;
            }
        }else{
            //if not specified
            emailEdit.setError(getString(R.string.error_invalid_email));
            emailEdit.requestFocus();
            return false;
        }

        //validate password
        EditText passwordEdit = (EditText) findViewById(R.id.edit_text_register_password1);
        passwordEdit.setError(null);
        //check if fst password is valid
        if(passwordEdit.getText() != null &&! passwordEdit.getText().toString().matches("")){
            String password  = passwordEdit.getText().toString();

            //check if valid
            if(!isPasswordValid(password)) {
                passwordEdit.setError(getString(R.string.error_invalid_password));
                passwordEdit.requestFocus();
                return false;
            }
        }else{
            passwordEdit.setError(getString(R.string.error_invalid_password));
            return  false;
        }


        //check if scnd password equals the first
        String password  = passwordEdit.getText().toString();

        EditText repeatPasswordEdit =  (EditText) findViewById(R.id.edit_text_register_password2);
        repeatPasswordEdit.setError(null);
        if(repeatPasswordEdit.getText() != null && ! repeatPasswordEdit.getText().toString().matches("")){
            String repeatPassword = repeatPasswordEdit.getText().toString();

            if(!repeatPassword.equals(password)){
                repeatPasswordEdit.setError(getString(R.string.error_password_not_matching));
                repeatPasswordEdit.requestFocus();
                return false;
            }
        }else{
            repeatPasswordEdit.setError(getString(R.string.error_password_not_matching));
            repeatPasswordEdit.requestFocus();
            return false;
        }


        //check if available
        if(!emailAvailable(email)){
            emailEdit.setError(getString(R.string.error_email_taken));
            emailEdit.requestFocus();
            return false;
        }
        if(!userNameAvailable(userName)) {
            userNameEdit.setError(getString(R.string.error_username_taken));
            userNameEdit.requestFocus();
            return false;
        }


        return true;



    }



    public static boolean isPasswordValid(String password) {

        return password.length() > 7;
    }
    public static boolean isEmailValid(String email) {
        return (email.contains("@")
                && email.toLowerCase().endsWith("xjtlu.edu.cn"));


    }


    /**
     * checks if there already exists a user for this user name
     * @username the username to check if it already exists.
     * @return true if a user with the given username is available, false if the user already exists.
     */
    private boolean userNameAvailable(String username) throws Exception {
        AsyncTask<String, Void, AsyncTaskResult<Boolean>> userNameAvailableTask = new AsyncTask<String, Void, AsyncTaskResult<Boolean>>() {
            @Override
            protected AsyncTaskResult<Boolean> doInBackground(String... taskParams) {

                HashMap<String, String> params = new HashMap<String, String>();
                params.put(HttpRequestHandler.USERNAME_AVAILABLE_PARAMS[0],taskParams[0]);


            AsyncTaskResult<Boolean> result = new AsyncTaskResult<>();
            //send post request to server
            String response = null;
            try {

                response = HttpRequestHandler.post(HttpRequestHandler.USERNAME_AVAILABLE, params);
                //try to parse result as boolean
                Boolean isAvailable = Boolean.parseBoolean(response.trim());

                result.putResult(isAvailable);

            } catch (Exception e) {
                Log.d("ExceptionHandling", "addingException: " + e.toString());
                result.addException(e);
            }
            finally{
                return result;
            }
        }
        }.execute(username);

            AsyncTaskResult<Boolean> taskResult = userNameAvailableTask.get();
            if(taskResult.getExceptions() != null){
                for(Exception e: taskResult.getExceptions()){
                    throw e;
                }

                return false;
            }else{
                return taskResult.getResult();
            }



    }


    /**
     * checks if the username is valid
     * @param username the username to validate
     * @return true if the username is valid, false if not
     */
    private boolean userNameValid(String username){
        return(username.length() >=5);
    }

    /**
     * checks if there already exists a user for this email address
     * @email the email to check if it already exists.
     * @return true if there is no user with the given email, false if a user with the given email already exists.
     */
    private boolean emailAvailable(String email) throws Exception {
        AsyncTask<String, Void, AsyncTaskResult<Boolean>> emailAvailableTask = new AsyncTask<String, Void, AsyncTaskResult<Boolean>>() {
            @Override
            protected AsyncTaskResult<Boolean> doInBackground(String... taskParams) {

                HashMap<String, String> params = new HashMap<String, String>();
                params.put(HttpRequestHandler.EMAIL_AVAILABLE_PARAMS[0],taskParams[0]);


                AsyncTaskResult<Boolean> result = new AsyncTaskResult<>();
                //send post request to server
                String response = null;
                try {

                    response = HttpRequestHandler.post(HttpRequestHandler.EMAIL_AVAILABLE, params);
                    //try to parse result as boolean
                    Boolean isAvailable = Boolean.parseBoolean(response.trim());

                    result.putResult(isAvailable);

                } catch (Exception e) {
                    result.addException(e);
                }
                finally{
                    return result;
                }
            }
        }.execute(email);

        AsyncTaskResult<Boolean> taskResult = emailAvailableTask.get();
        if(taskResult.getExceptions() != null){
            for(Exception e: taskResult.getExceptions()){
                throw e;
            }

            return false;
        }else{
            Log.d("email available", "returning available: " + taskResult.getResult());
            return taskResult.getResult();
        }


    }


    //============================================================================================//
    //--- Methods used for Exception handling --------------------------------------------------------//
    //============================================================================================//

    /**
     * Method implementing the standard war of reacting to an Exception
     *
     * @param e the Exception to react to
     */
    //TODO: extend for other Exceptions if necessary
    public void handleException(Exception e) {

        Log.d("ExceptionHandling", "handling exception: " + e.toString());

        if (e instanceof ServerException) {
            handleException((ServerException) e);
        } else if (e instanceof  ConnectException){
            handleException((ConnectException) e);

        } else if (e instanceof IOException) {
            handleException((IOException) e);

        } else if (e instanceof  NoConnectionException){
            handleException((NoConnectionException) e);

        }else{
            e.printStackTrace();
        }
    }

    /**
     * Method implementing the standard way of reacting to a ConnectException.
     *
     * @param e the ConnectException to react to.
     */
    //TODO: replace with splash screen excepiton
    public void handleException(ConnectException e){
        e.printStackTrace();
        Toast.makeText(this, "Could not connect to server", Toast.LENGTH_SHORT).show();
    }

    /**
     * Method implementing the standard way of reacting to a ServerException.
     *
     * @param e the ServerException to react to.
     */
    //TODO: replace with splash screen excepiton
    public void handleException(ServerException e) {
        e.printStackTrace();
        Toast.makeText(this, "Server Exception occured", Toast.LENGTH_SHORT).show();
    }


    /**
     * Method implementing the standard way of reacting to anm IOException.
     *
     * @param e the IOException to react to.
     */
    //TODO: replace with splash screen excepiton, split into network error etc.
    public void handleException(IOException e) {
        e.printStackTrace();
        Toast.makeText(this, "IOException Exception occured", Toast.LENGTH_SHORT).show();
    }

    public void handleException(NoConnectionException e){
        e.printStackTrace();
        InternetConnectionHandler.waitForConnection(this, this);
    }


}
