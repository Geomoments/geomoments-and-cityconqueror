package xjtlu.geomomentsdev;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import xjtlu.geomomentsdev.tools.ImageHandle;
import xjtlu.geomomentsdev.tools.InternetConnectionHandler;

/**
 * A activity to edit the text and image content of as part of creating a new Geomoment.
 *
 */
public class EditContentActivity extends AppCompatActivity {


    public static final String INTENT_EXTRA_IMAGE_URI = "IMAGE_URI";
    public static final String INTENT_EXTRA_IMAGE_PATH = "IMAGE_PATH";
    public static final String INTENT_EXTRA_TEXT = "TEXT";
    public static final String MAX_TEXT_CONTENT_LENGTH = "320" ;
    public ImageView imageView;
    private int REQUEST_IMAGE_PICKER = 1;
    private String imageContentPath;
    private Uri imageContentUri;
    private int textContentCharCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_content);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        Bundle extras = getIntent().getExtras();


        imageView = (ImageView) findViewById(R.id.image_view_edit_content_image);
        if(extras != null){

            // set textContent if available
            if(extras.containsKey(INTENT_EXTRA_TEXT)){

                EditText editText = (EditText) findViewById(R.id.edit_text_edit_content_text);
                editText.setText(extras.getString(INTENT_EXTRA_TEXT), TextView.BufferType.EDITABLE);
                textContentCharCount = editText.getText().toString().length();


            }




            // set imageContent if available
            if( extras.containsKey(INTENT_EXTRA_IMAGE_PATH)
                    && extras.containsKey(INTENT_EXTRA_IMAGE_URI)) {

                imageContentPath = extras.getString(INTENT_EXTRA_IMAGE_PATH);
                imageContentUri = Uri.parse(extras.getString(INTENT_EXTRA_IMAGE_URI));

                ViewTreeObserver vto = imageView.getViewTreeObserver();
                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        imageView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        imageView.setImageBitmap(ImageHandle.getSampleImage(imageContentPath,
                                imageView.getMeasuredWidth()));

                        imageView.setVisibility(View.VISIBLE);

                    }
                });


            }else {
                imageContentUri = null;
                imageView.setVisibility(View.GONE);

            }}else{

                imageView.setVisibility(View.GONE);

                imageContentPath = null;
                imageContentUri = null;
            }


        TextWatcher textContentWatcher = new TextWatcher(){

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                TextView textContentRemainingChars = (TextView) findViewById(R.id.text_view_edit_content_remaining_chars);
                textContentRemainingChars.setText(String.valueOf(s.length()) + "/" + MAX_TEXT_CONTENT_LENGTH);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }


        };

        EditText editText = (EditText) findViewById(R.id.edit_text_edit_content_text);
        editText.addTextChangedListener(textContentWatcher);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_content, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.menu_item_edit_content_submit:
                submit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * starts an ImagePickerActivity to select the imageContent.
     */
    public void addImage(View v){

        Intent imagePickerIntent = new Intent(this, ImagePickerActivity.class);
        startActivityForResult(imagePickerIntent, REQUEST_IMAGE_PICKER);

    }


    /**
     * method to return the selected text and image content and finish this activity if any content is selected.
     */
    private void submit(){

        EditText editText = (EditText) findViewById(R.id.edit_text_edit_content_text);
        String contentText = editText.getText().toString().trim();

        if((contentText == null || contentText.equals("")) && imageContentPath == null && imageContentUri == null){
            Toast.makeText(this, getString(R.string.no_content_selected), Toast.LENGTH_LONG).show();

        }else{

            Intent resultIntent = new Intent();

            if(contentText !=  null && ! contentText.equals("")){
                resultIntent.putExtra(EditContentActivity.INTENT_EXTRA_TEXT, editText.getText().toString());

            }

            if(imageContentUri != null && imageContentPath != null ){
                resultIntent.putExtra(EditContentActivity.INTENT_EXTRA_IMAGE_PATH, imageContentPath);
                resultIntent.putExtra(EditContentActivity.INTENT_EXTRA_IMAGE_URI, imageContentUri.toString());
            }
            setResult(RESULT_OK, resultIntent);

            finish();

        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // if image picker returns
        if (requestCode == REQUEST_IMAGE_PICKER && resultCode == RESULT_OK) {

            //get image data
            imageContentUri = Uri.parse(data.getStringExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGE_URI));
            imageContentPath = data.getStringExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGE_PATH);

            imageView.setVisibility(View.VISIBLE);

            try{
                ((BitmapDrawable)imageView.getDrawable()).getBitmap().recycle();

            }catch(Exception e){
                e.printStackTrace();
            }
            //workaround: sample image for width from text view if imageView was not made visible yet
            imageView.setImageBitmap(ImageHandle.getSampleImage(imageContentPath, findViewById(R.id.text_view_edit_content_text_title).getWidth()));
            imageView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onDestroy(){

        try{
            ((BitmapDrawable)imageView.getDrawable()).getBitmap().recycle();

        }catch(Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }




}
