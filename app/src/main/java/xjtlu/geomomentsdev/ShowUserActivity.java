package xjtlu.geomomentsdev;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import xjtlu.geomomentsdev.tools.AsyncTaskResult;
import xjtlu.geomomentsdev.tools.DatabaseHandler;
import xjtlu.geomomentsdev.tools.HttpRequestHandler;
import xjtlu.geomomentsdev.tools.InternetConnectionHandler;
import xjtlu.geomomentsdev.tools.LocationHandler;
import xjtlu.geomomentsdev.tools.NoConnectionException;
import xjtlu.geomomentsdev.tools.ServerException;
import xjtlu.geomomentsdev.tools.User;

/**
 * an activity to show the information of a user that is not the logged in user
 */
public class ShowUserActivity extends AppCompatActivity {

    //intent extras
    public static final String INTENT_EXTRA_USER_ID = "userId";

    public User selectedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_user);

        Bundle extras = getIntent().getExtras();
        InternetConnectionHandler.setContext(this);

        if(extras != null && extras.containsKey(INTENT_EXTRA_USER_ID)) {

            //download user information
            AsyncTask<Long, Void, AsyncTaskResult<User>> userDownloadTask = new AsyncTask<Long, Void, AsyncTaskResult<User>>() {



                @Override
                protected AsyncTaskResult<User> doInBackground(Long... params) {
                    AsyncTaskResult<User> result = new AsyncTaskResult<User>();
                    try {
                        User downloadedUser = User.downloadUser(params[0]);
                        result.putResult(downloadedUser);


                    } catch (Exception e) {
                        result.addException(e);

                    }
                    finally {
                        return result;
                    }

                }

                @Override
                protected void onPostExecute(AsyncTaskResult<User>  userDownloadResult) {
                    if (userDownloadResult.getExceptions() != null) {
                        for (Exception e : userDownloadResult.getExceptions()) {
                            handleException(e);
                        }
                    } else {
                        selectedUser = userDownloadResult.getResult();


                        TextView nameTextView = (TextView) findViewById(R.id.text_view_show_user_name);
                        nameTextView.setText(Html.fromHtml(selectedUser.getName() + " <small>(" + selectedUser.getUniqueName() + ")</small>"));

                        TextView statusTextView = (TextView) findViewById(R.id.text_view_show_user_status);
                        statusTextView.setText(selectedUser.getStatus());

                        if (selectedUser.getProfilePicture() != null) {
                            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_show_user_progress);

                            ImageView profilePictureView = (ImageView) findViewById(R.id.image_view_show_user_profile_picture);

                            selectedUser.getProfilePicture().showThisThumbImageInView(profilePictureView, progressBar);
                        }
                    }
                }




            }.execute(extras.getLong(INTENT_EXTRA_USER_ID));

        }



    }

    @Override
    public void onDestroy(){
        ImageView profilePictureView = (ImageView) findViewById(R.id.image_view_show_user_profile_picture);
        try{
            ((BitmapDrawable)profilePictureView.getDrawable()).getBitmap().recycle();

        }catch(Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * triggers a FullImageActivity to show the profile picture of the user.
     * @param view
     */
    public void showFullProfilePicture(View view){

        if(selectedUser.getProfilePicture() != null) {
            Intent fullImage = new Intent(this, FullImageActivity.class);
            fullImage.putExtra(FullImageActivity.INTENT_EXTRA_IMAGE_ID, selectedUser.getProfilePicture().getId());
            startActivity(fullImage);
        }else{
            Toast.makeText(this, getString(R.string.error_no_image_selcted), Toast.LENGTH_LONG);
        }
    }

    /**
     * triggers a MomentBrowserActivity to show the newrby moment created gby the selected user.
     * @param view
     */
    public void showMoments (View view){

        Intent browseMoment = new Intent(this, MomentBrowserActivity.class);
        browseMoment.putExtra(MomentBrowserActivity.INTENT_EXTRA_WHERE_STMT,   DatabaseHandler.MOMENTS_CREATOR + " = " + Long.toString(selectedUser.getId()) + " and " + HttpRequestHandler.getMomentRangeFilter(LocationHandler.getMyLat(), LocationHandler.getMyLng()));

        browseMoment.putExtra(MomentBrowserActivity.INTENT_EXTRA_ORDER_BY_STMT, " " + DatabaseHandler.MOMENTS_TIMESTAMP + " DESC");
        browseMoment.putExtra(MomentBrowserActivity.INTENT_EXTRA_IS_LOCATION_DEPENDENT, true);

        startActivity(browseMoment);

    }


    //============================================================================================//
    //--- Methods used for Exception handling --------------------------------------------------------//
    //============================================================================================//

    /**
     * Method implementing the standard war of reacting to an Exception
     *
     * @param e the Exception to react to
     */
    //TODO: extend for other Exceptions if necessary
    public void handleException(Exception e) {

        if (e instanceof ServerException) {
            handleException((ServerException) e);
        } else if (e instanceof IOException) {
            handleException((IOException) e);
        } else if (e instanceof  NoConnectionException){
            handleException((NoConnectionException) e);
        }

        e.printStackTrace();
    }


    /**
     * Method implementing the standard way of reacting to a ServerException.
     *
     * @param e the ServerException to react to.
     */
    //TODO: replace with splash screen excepiton
    public void handleException(ServerException e) {
        e.printStackTrace();
        Toast.makeText(this, "Server Exception occured", Toast.LENGTH_SHORT).show();
    }


    /**
     * Method implementing the standard way of reacting to anm IOException.
     *
     * @param e the IOException to react to.
     */
    //TODO: replace with splash screen excepiton, split into network error etc.
    public void handleException(IOException e) {
        e.printStackTrace();
        Toast.makeText(this, "IOException Exception occured", Toast.LENGTH_SHORT).show();
    }



    public void handleException(NoConnectionException e){

        e.printStackTrace();
        Toast.makeText(this, getString(R.string.please_connect_to_internet), Toast.LENGTH_SHORT).show();
        finish();
        //InternetConnectionHandler.waitForConnection(this, this);
    }
}
