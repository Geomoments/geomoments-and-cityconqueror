package xjtlu.geomomentsdev;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import xjtlu.geomomentsdev.tools.InternetConnectionHandler;

public class EditTextActivity extends AppCompatActivity {

    public static final String INTENT_EXTRA_TEXT = "TEXT";
    public static final String INTENT_EXTRA_MAX_LENGTH = "MAX_LENGTH";
    private int maxLength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text);

        //show text if passed

        Bundle extras = getIntent().getExtras();

        if(extras.containsKey(INTENT_EXTRA_TEXT)){

            EditText editText = (EditText) findViewById(R.id.edit_text_edit_text_text);
            editText.setText(extras.getString(INTENT_EXTRA_TEXT),  TextView.BufferType.EDITABLE);


        }
        if(extras.containsKey(INTENT_EXTRA_MAX_LENGTH)){
            maxLength = extras.getInt(INTENT_EXTRA_MAX_LENGTH);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_text, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.menu_item_edit_text_submit:
                submit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void submit(){

        Intent resultIntent = new Intent();

        EditText editText = (EditText) findViewById(R.id.edit_text_edit_text_text);

        if((! editText.getText().toString().equals("") ) && editText.getText().toString() != null){

            if (editText.getText().toString().trim().length() > maxLength){
                Toast.makeText(this, getString(R.string.text_to_long), Toast.LENGTH_LONG);
            }else {

                resultIntent.putExtra(INTENT_EXTRA_TEXT, editText.getText().toString().trim());
                setResult(RESULT_OK, resultIntent);

                finish();
            }
        }else{
            Toast.makeText(this, getString(R.string.missing_text), Toast.LENGTH_LONG).show();
        }

    }

}
