package xjtlu.geomomentsdev;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import xjtlu.geomomentsdev.R;
import xjtlu.geomomentsdev.tools.ImageHandle;

/**
 * A activity to select an image from the camera or the gallery of the device.
 */
public class ImagePickerActivity extends AppCompatActivity {

    public static final int IMAGE_GALLERY_REQUEST = 1;
    public static final int IMAGE_CAMERA_REQUEST = 2;
    public static final String INTENT_EXTRA_SELECTED_IMAGE_URI = "SELECTED_IMAGE_URI";
    public static final String INTENT_EXTRA_SELECTED_IMAGE_PATH = "SELECTED_IMAGE_PATH";

    public static final String INSTANCE_STATE_PARAM_IMAGE_URI = "IMAGE_PICKER_IMAGE_URI";
    public static final String INSTANCE_STATE_PARAM_IMAGE_PATH = "IMAGE_PATH";

    private Uri selectedImageUri;
    private String selectedImagePath;
    private boolean imageSelected;
    private ImageView imagePreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_picker);

        selectedImageUri = null;
        selectedImagePath = null;
        imageSelected = false;
        imagePreview = (ImageView) findViewById(R.id.image_view_image_picker_preview);

        // enable select from camera, only if device hasCamera
        Button selectFromCamera = (Button) findViewById(R.id.button_image_picker_from_camera);
        boolean hasCamera = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);

        selectFromCamera.setEnabled(hasCamera);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_picker, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.menu_item_image_picker_confirm:
                submitSelection();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        if(selectedImagePath != null) {
            savedInstanceState.putString(INSTANCE_STATE_PARAM_IMAGE_PATH, selectedImagePath);
        }
        if(selectedImageUri != null){
            savedInstanceState.putString(INSTANCE_STATE_PARAM_IMAGE_URI, selectedImageUri.toString());
        }
        super.onSaveInstanceState(savedInstanceState);
    }


   /* @Override
    public void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);

        if(savedInstanceState.containsKey(INSTANCE_STATE_PARAM_IMAGE_PATH)){
            selectedImagePath = savedInstanceState.getString(INSTANCE_STATE_PARAM_IMAGE_PATH);

        }else{
            selectedImagePath = null;
        }

        if(savedInstanceState.containsKey(INSTANCE_STATE_PARAM_IMAGE_URI)){
            selectedImageUri = Uri.parse(savedInstanceState.getString(INSTANCE_STATE_PARAM_IMAGE_URI));
        }else{
            selectedImageUri = null;
        }
        if(selectedImagePath != null && selectedImageUri != null){
            imageSelected = true;
        }
        imagePreview = (ImageView) findViewById(R.id.image_view_image_picker_preview);

        if(imageSelected){
            imagePreview.setImageBitmap(ImageHandle.getSampleImage(selectedImagePath, imagePreview.getWidth()));
        }
    }



    @Override
    public void onConfigurationChanged(Configuration config){
        super.onConfigurationChanged(config);
        if(config.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_image_picker);
            imagePreview = (ImageView) findViewById(R.id.image_view_image_picker_preview);
            imagePreview.setImageBitmap(ImageHandle.getSampleImage(selectedImagePath, imagePreview.getWidth()));



        }else if(config.orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.activity_image_picker);
            imagePreview = (ImageView) findViewById(R.id.image_view_image_picker_preview);
            ViewTreeObserver vto = imagePreview.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {

                    imagePreview.setImageBitmap(ImageHandle.getSampleImage(selectedImagePath, imagePreview.getWidth()));

                    return true;
                }
            });



        }
    }*/

    /**
     * starts an intent to pick an image from the devices gallery
     * @param view
     */
    public void selectImageFromGallery(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);


    }

    /**
     * from https://developer.android.com/training/camera/photobasics.html
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        selectedImagePath = image.getAbsolutePath();
        return image;
    }



    /**
     * from https://developer.android.com/training/camera/photobasics.html
     */
    public void dispatchTakePictureIntent(View v) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "xjtlu.geomomentsdev.fileprovider" ,
                        photoFile);

                selectedImageUri = photoURI;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                Log.d("dispatchTakePicture: ", "" + photoURI.toString());

                startActivityForResult(takePictureIntent, IMAGE_CAMERA_REQUEST);
            }
        }
    }

    /**
     * according to: http://stackoverflow.com/questions/13209494/how-to-get-the-full-file-path-from-uri
     * gets the filepath for the specified Uri.
     * @param uri the Uri to get the filepath for
     * @return the filepath for the specified Uri
     */
    private String pathFromUri(Uri uri){
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Log.d("ImagePicker", "stringUri: " + uri);
        // Get the cursor
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        //Move to first row
        cursor.moveToFirst();
        Log.d("ImagePicker", "uri.path: " + uri.getPath());


        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        return(cursor.getString(columnIndex));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {


            //When image is selected form Gallery
            if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == RESULT_OK && null != data) {
                // Get Uri from intent

                selectedImageUri = data.getData();

               // imagePreview.setImageURI(selectedImageUri);
                selectedImagePath = pathFromUri(selectedImageUri);

                //try to recycle bitmap
                try{
                    ((BitmapDrawable) imagePreview.getDrawable()).getBitmap().recycle();

                }catch(Exception e){
                    e.printStackTrace();
                }
                imagePreview.setImageBitmap(ImageHandle.getSampleImage(selectedImagePath, imagePreview.getWidth()));

                imageSelected = true;

            }else{

                //When image is selected form CAMERA
                if(requestCode == IMAGE_CAMERA_REQUEST && resultCode == RESULT_OK){
                    Log.d("camera activiy result", "selectedImageURi= " + selectedImageUri);
                    //try to recycle bitmap
                    try{
                        ((BitmapDrawable) imagePreview.getDrawable()).getBitmap().recycle();

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    imagePreview.setImageBitmap(ImageHandle.getSampleImage(selectedImagePath, imagePreview.getWidth()));

                    imageSelected = true;
                }


            }


        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }


    /**
     * method to return the selection and finish this activity
     */
    public void submitSelection(){

        // check if image selected
        if((!imageSelected) || selectedImageUri == null){
            Toast.makeText(this, "no image selected" , Toast.LENGTH_LONG).show();
        }else {

            // add image to intent
            Intent intentImage = new Intent();


            intentImage.putExtra(INTENT_EXTRA_SELECTED_IMAGE_URI, selectedImageUri.toString());
            intentImage.putExtra(INTENT_EXTRA_SELECTED_IMAGE_PATH, selectedImagePath);

            // return intent
            setResult(RESULT_OK, intentImage);

            finish();
        }
    }

    @Override
    public void onDestroy(){
        //try to recycle bitmap in imagePreview
        try{
            ((BitmapDrawable) imagePreview.getDrawable()).getBitmap().recycle();

        }catch(Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }





}
