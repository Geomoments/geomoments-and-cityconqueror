package xjtlu.geomomentsdev;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;

import xjtlu.geomomentsdev.tools.PreferenceHandler;
import xjtlu.geomomentsdev.tools.User;

/**
 * Created by tmp on 4/25/2016.
 */
public class ProfileFragment extends Fragment {

    /**
     * the root view showing the fragment.
     */
    private View rootView;
    /**
     * the progress bar indicating the progress of displaying the profile picture.
     */
    private ProgressBar profilePictureProgress;


    /**
     * constructor
     */
    public ProfileFragment() {


    }

    /**
     * returns new instance of ProfileFragment
     *
     * @return a new ProfileFragment
     */
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();

        return fragment;
    }


    /**
     * returns the progressBar for the profile picture
     *
     * @return the progressBar for the profile picture
     */

    public ProgressBar getProfilePictureProgress() {
        return (ProgressBar) rootView.findViewById(R.id.progress_bar_fragment_profile_image_progress);
    }

    /**
     * refreshes the Fragment view by setting the profile picture, name, email and status of the logged in user.
     *
     * @param refreshProfilePicture true, if profilePicture should be refreshed , false if not.
     */
    public void refresh(boolean refreshProfilePicture) {
        ImageView profilePictureView = (ImageView) rootView.findViewById(R.id.image_view_fragment_profile_profile_picture);

        if (refreshProfilePicture) {

            //try to recycle previously displayed image
            try {
                ((BitmapDrawable) profilePictureView.getDrawable()).getBitmap().recycle();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        User loggedInUser = User.getLoggedInUser();
        if (loggedInUser != null) {

            //set name
            Button nameButton = (Button) rootView.findViewById(R.id.button_fragment_profile_user_name);
            nameButton.setText(Html.fromHtml("<b>" + loggedInUser.getName() + "</b> <small>(" + loggedInUser.getUniqueName() + ")</small>"));

            //set status
            Button statusButton = (Button) rootView.findViewById(R.id.button_fragment_profile_status);
            statusButton.setText(loggedInUser.getStatus());

            //set email
            Button emailButton = (Button) rootView.findViewById(R.id.button_fragment_profile_email);
            emailButton.setText(loggedInUser.getEmail());


            if (refreshProfilePicture) {
                //set profile picture if available
                if (loggedInUser.getProfilePicture() != null) {
                    loggedInUser.getProfilePicture().showThisThumbImageInView(profilePictureView, profilePictureProgress);
                }
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        profilePictureProgress = (ProgressBar) rootView.findViewById(R.id.progress_bar_fragment_profile_image_progress);

        //set profile picture onClick activity
        ImageView profilePictureView = (ImageView) rootView.findViewById(R.id.image_view_fragment_profile_profile_picture);
        profilePictureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainNavigationActivity) container.getContext()).triggerShowFullProfilePicture();
            }
        });

        //set edit profile picture button onClick activity
        FloatingActionButton editProfilePictureButton = (FloatingActionButton) rootView.findViewById(R.id.floating_button_profile_fragment_edit_picture);
        editProfilePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainNavigationActivity) container.getContext()).editProfilePicture();
            }
        });


        //set name Button onClick activity
        Button nameButton = (Button) rootView.findViewById(R.id.button_fragment_profile_user_name);
        nameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainNavigationActivity) container.getContext()).editUserName();
            }
        });

        //set status Button onClick activity
        Button statusButton = (Button) rootView.findViewById(R.id.button_fragment_profile_status);
        statusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainNavigationActivity) container.getContext()).editUserStatus();
            }
        });

        //set email Butotn onClick activity
        Button emailButton = (Button) rootView.findViewById(R.id.button_fragment_profile_email);
      /*  emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainNavigationActivity) container.getContext()).editUserEmail();
            }
        });
*/

        //set manage Moments onClick Activity
        Button manageMoments = (Button) rootView.findViewById(R.id.button_fragment_profile_manage_moments);
        manageMoments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((MainNavigationActivity) container.getContext()).manageMyMoments();

            }
        });

        Button logoutButton = (Button) rootView.findViewById(R.id.button_fragment_profile_logout);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainNavigationActivity) container.getContext()).logout();
            }
        });
        final Button changeLanguage = (Button) rootView.findViewById(R.id.button_fragment_profile_language);
        changeLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu languageSelection = new PopupMenu(container.getContext(), changeLanguage);
                languageSelection.getMenuInflater().inflate(R.menu.menu_language, languageSelection.getMenu());
                languageSelection.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        MainNavigationActivity mainActivity = ((MainNavigationActivity) container.getContext());
                        switch (item.getItemId()) {
                            case R.id.menu_item_language_english:

                                mainActivity.setLanguage("en_US");
                                PreferenceHandler.setLanguage("en_US");
                                mainActivity.refresh();
                                return true;

                            case R.id.menu_item_language_german:

                                mainActivity.setLanguage("de_DE");
                                PreferenceHandler.setLanguage("de_DE");
                                mainActivity.refresh();
                                return true;

                            case R.id.menu_item_language_chinese:
                                PreferenceHandler.setLanguage("zh_CN");
                                mainActivity.setLanguage("zh_CN");
                                mainActivity.refresh();
                                return true;

                            case R.id.menu_item_main_menu_logout:
                                mainActivity.logout();
                                return true;
                            default:
                                return false;
                        }
                    }});
                languageSelection.show();


            }
        });


        refresh(true);
        return rootView;
    }


}
