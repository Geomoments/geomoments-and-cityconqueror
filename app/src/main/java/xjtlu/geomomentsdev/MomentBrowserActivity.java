package xjtlu.geomomentsdev;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import xjtlu.geomomentsdev.tools.AsyncMomentsDownloader;
import xjtlu.geomomentsdev.tools.AsyncTaskResult;
import xjtlu.geomomentsdev.tools.HttpRequestHandler;
import xjtlu.geomomentsdev.tools.InternetConnectionHandler;
import xjtlu.geomomentsdev.tools.LocationDependentActivity;
import xjtlu.geomomentsdev.tools.LocationHandler;
import xjtlu.geomomentsdev.tools.Moment;
import xjtlu.geomomentsdev.tools.MomentArrayAdapter;
import xjtlu.geomomentsdev.tools.NoConnectionException;
import xjtlu.geomomentsdev.tools.ServerException;
import xjtlu.geomomentsdev.tools.User;

public class MomentBrowserActivity extends AppCompatActivity implements LocationDependentActivity {

    //intent extras
    public static final String INTENT_EXTRA_WHERE_STMT = "whereStmt";
    public static final String INTENT_EXTRA_ORDER_BY_STMT = "orderByStmt";
    public static final String INTENT_EXTRA_IS_LOCATION_DEPENDENT = "LOCATION_DEPENDENT";

    private MomentArrayAdapter momentArrayAdapter;
    private String whereStmt;
    private String orderByStmt;
    private boolean isLocationDependent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moment_browser);

        InternetConnectionHandler.setContext(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(INTENT_EXTRA_WHERE_STMT)) {
                whereStmt = extras.getString(INTENT_EXTRA_WHERE_STMT);
            } else {
                whereStmt = null;
            }
            if (extras.containsKey(INTENT_EXTRA_ORDER_BY_STMT)) {
                orderByStmt = extras.getString(INTENT_EXTRA_ORDER_BY_STMT);
            } else {
                orderByStmt = null;
            }
            //set locationDependent only if required
            if (extras.containsKey(INTENT_EXTRA_IS_LOCATION_DEPENDENT)) {
                isLocationDependent = extras.getBoolean(INTENT_EXTRA_IS_LOCATION_DEPENDENT);
                if (isLocationDependent) {

                    LocationHandler.registerLocationDependentActivity(this);
                }
            } else {
                isLocationDependent = false;
            }
        }

        //set swipe refresh
        final SwipeRefreshLayout swipeRefresher = (SwipeRefreshLayout) findViewById(R.id.siwpe_refresh_moment_browser_activity_content);

        swipeRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                downloadMomentsToListView();
                swipeRefresher.setRefreshing(false);
            }
        });

        downloadMomentsToListView();

    }

    /**
     * sets the given moment list to be displayed in the list view of this activity
     *
     * @param downloadedMoments the moments to display in the list view

    */
    private void setListViewContent(ArrayList<Moment> downloadedMoments) {

        TextView noMomentsTextView = (TextView) findViewById(R.id.text_view_moment_browser_activity_no_moments);
        ListView momentListView = (ListView) findViewById(R.id.list_view_moment_browser_activity_content);
        if (downloadedMoments != null) {
            //hide no moments message
            noMomentsTextView.setVisibility(View.GONE);

            momentArrayAdapter = new MomentArrayAdapter(this, downloadedMoments, User.getLoggedInUser().getId());
            momentListView.setAdapter(momentArrayAdapter);
            momentListView.setVisibility(View.VISIBLE);

        } else {
            //moments -> hide list view, show no moments text
            momentListView.setVisibility(View.GONE);
            noMomentsTextView.setVisibility(View.VISIBLE);
        }
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_moment_browser_activity_progress);
        progressBar.setVisibility(View.GONE);


    }

    /**
     * downloads moments from the server, filtered by the where statement given in the activity intent and the current location (if activity is set to be location dependent),
     * ordered by the order bt statement given in the activity intent.
     */
    public void downloadMomentsToListView() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_moment_browser_activity_progress);
        progressBar.setVisibility(View.VISIBLE);

        String queryFilter = null;

        if (isLocationDependent) {
            if (LocationHandler.getMyLat() != null && LocationHandler.getMyLng() != null) {
                //get where statement according to location

                queryFilter = whereStmt + " and " + HttpRequestHandler.getMomentRangeFilter(LocationHandler.getMyLat(), LocationHandler.getMyLng());
            }
        } else {
            queryFilter = whereStmt;
        }

        final LocationDependentActivity lda = this;


        AsyncMomentsDownloader momentsDownloader = (AsyncMomentsDownloader) new AsyncMomentsDownloader(new AsyncMomentsDownloader.AsyncResponse() {
            @Override
            public void processFinish(AsyncTaskResult<ArrayList<Moment>> downloadedMoments) {

                if (downloadedMoments.getExceptions() != null) {
                    for (Exception e : downloadedMoments.getExceptions()) {
                        handleException(e);
                    }
                } else {
                    if (isLocationDependent) {
                        LocationHandler.setNotDeprecated(lda);
                    }
                    setListViewContent(downloadedMoments.getResult());
                }
            }

        }, queryFilter, orderByStmt, User.getLoggedInUser(), (ProgressBar) findViewById(R.id.progress_bar_moment_browser_progress)).execute();

    }

    @Override
    public void locationDependentRefresh() {
        //download and show new data
        downloadMomentsToListView();
    }

    @Override
    public void manageMomentsDownloadException(Exception e) {
        //TODO: replace with proper exception handling
        e.printStackTrace();handleException( e);
    }

    @Override
    public void onDestroy() {

        LocationHandler.dropLocationDependentActivity(this);

        super.onDestroy();
    }


    //============================================================================================//
    //--- Methods used for Exception handling --------------------------------------------------------//
    //============================================================================================//

    /**
     * Method implementing the standard war of reacting to an Exception
     *
     * @param e the Exception to react to
     */
    //TODO: extend for other Exceptions if necessary
    public void handleException(Exception e) {

        if (e instanceof ServerException) {
            handleException((ServerException) e);
        } else if (e instanceof IOException) {
            handleException((IOException) e);
        } else if (e instanceof  NoConnectionException){
            handleException((NoConnectionException) e);
        }
        else {
            e.printStackTrace();
        }
    }


    /**
     * Method implementing the standard way of reacting to a ServerException.
     *
     * @param e the ServerException to react to.
     */
    //TODO: replace with splash screen excepiton
    public void handleException(ServerException e) {
        e.printStackTrace();
        Toast.makeText(this, "Server Exception occured", Toast.LENGTH_LONG);
    }


    /**
     * Method implementing the standard way of reacting to anm IOException.
     *
     * @param e the IOException to react to.
     */
    //TODO: replace with splash screen excepiton, split into network error etc.
    public void handleException(IOException e) {
        e.printStackTrace();
        Toast.makeText(this, "IOException Exception occured", Toast.LENGTH_LONG);
    }




    public void handleException(NoConnectionException e){
        e.printStackTrace(); InternetConnectionHandler.waitForConnection(this, this);
    }

}
