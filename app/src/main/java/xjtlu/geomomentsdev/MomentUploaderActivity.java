package xjtlu.geomomentsdev;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import xjtlu.geomomentsdev.tools.AsyncImageUploader;
import xjtlu.geomomentsdev.tools.AsyncTaskResult;
import xjtlu.geomomentsdev.tools.InternetConnectionHandler;
import xjtlu.geomomentsdev.tools.Moment;
import xjtlu.geomomentsdev.tools.NoConnectionException;
import xjtlu.geomomentsdev.tools.ServerException;

/**
 * an activity to upload a Geomoment given in the intent extras.
 */
public class MomentUploaderActivity extends AppCompatActivity {


    //intent extras
    public static final String INTENT_EXTRA_IMAGE_PATH = "IMAGE_PATH";
    public static final String INTENT_EXTRA_TEXT = "TEXT";
    public static final String INTENT_EXTRA_CREATOR_ID = "CREATOR_ID";

    public static final String INTENT_EXTRA_LOCATION_LATITUDE = "LOCATION_LATITUDE";
    public static final String INTENT_EXTRA_LOCATION_LONGITUDE = "LOCATION_LONGITUDE";

    public static final String INTENT_EXTRA_MOMENT_ID = "MOMENT_ID";

    private double lat;
    private double lng;
    private String textContent;
    private String imageContentPath;
    private long creatorId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moment_uploader);
        uploadMoment(getIntent().getExtras());


        Button retryButton = (Button) findViewById(R.id.button_moment_uploader_retry);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRetry(false);
                uploadMoment(getIntent().getExtras());
            }
        });
    }


    /**
     * gets the moment data from intent extras.
     *
     * @param extras the intent extras holding the moments data.
     */
    public void getMomentData(Bundle extras) {


        if (extras != null) {
            //get location
            if (extras.containsKey(INTENT_EXTRA_LOCATION_LONGITUDE)
                    && extras.containsKey(INTENT_EXTRA_LOCATION_LATITUDE)) {
                lng = extras.getDouble(INTENT_EXTRA_LOCATION_LONGITUDE);
                lat = extras.getDouble(INTENT_EXTRA_LOCATION_LATITUDE);


            }

            //get text content if available
            if (extras.containsKey(INTENT_EXTRA_TEXT)) {
                textContent = extras.getString(INTENT_EXTRA_TEXT);
            } else {
                textContent = null;
            }

            //get image content if available
            if (extras.containsKey(INTENT_EXTRA_IMAGE_PATH)) {
                imageContentPath = extras.getString(PreviewMomentActivity.INTENT_EXTRA_IMAGE_PATH);
            } else {
                imageContentPath = null;
            }

            //get creator if available
            if (extras.containsKey(INTENT_EXTRA_CREATOR_ID)) {
                creatorId = extras.getLong(INTENT_EXTRA_CREATOR_ID);
            }


        }


    }

    /**
     * takes the moment data from the given intent extras and uloads the corresponding moment to the server.
     *
     * @param extras the intent extras holding the moment data.
     */
    public void uploadMoment(Bundle extras) {

        getMomentData(extras);
        // if has image, upload image, add moment containing image,
        if (imageContentPath != null) {



            // Upload image





















            AsyncImageUploader imageUploader = new AsyncImageUploader(new AsyncImageUploader.AsyncResponse() {

                @Override
                public void processFinish(AsyncTaskResult<Long> imageUploaderResult) {
                    // handle exceptions from image upload
                    Toast.makeText(getApplicationContext(), "image Upload success: " + imageUploaderResult.getResult(), Toast.LENGTH_LONG).show();
                    if (imageUploaderResult.getExceptions() != null) {
                        for (Exception e : imageUploaderResult.getExceptions()) {
                            handleException(e);
                        }
                    } else {

                        // upload moment after image upload
                        if (textContent != null) {
                            // moment has image and text -> upload moment with image and text

                            AsyncTask createMoment = new AsyncTask<Long, Void, AsyncTaskResult<Long>>() {

                                @Override
                                protected AsyncTaskResult<Long> doInBackground(Long... params) {

                                    AsyncTaskResult<Long> result = new AsyncTaskResult<Long>();


                                    try {
                                        long momentId = Moment.createMoment(creatorId, lat, lng, textContent, params[0]);
                                        result.putResult(momentId);

                                    } catch (IOException e) {
                                        result.addException(e);
                                    } catch (ServerException e) {
                                        result.addException(e);
                                    } catch (NumberFormatException e) {
                                        result.addException(e);

                                    } finally {
                                        return result;
                                    }
                                }

                                @Override
                                protected void onPostExecute(AsyncTaskResult<Long> createMomentResult) {
                                    if (createMomentResult.getExceptions() != null) {
                                        for (Exception e : createMomentResult.getExceptions()) {
                                            handleException(e);
                                        }
                                    } else {
                                        returnIntentResult(createMomentResult.getResult());
                                    }
                                }
                            }.execute(imageUploaderResult.getResult());


                        } else {
                            // moment has image no text -> upload moment with image only
                            AsyncTask createMoment = new AsyncTask<Long, Void, AsyncTaskResult<Long>>() {

                                @Override
                                protected AsyncTaskResult<Long> doInBackground(Long... params) {

                                    AsyncTaskResult<Long> result = new AsyncTaskResult<Long>();
                                    try {

                                        long momentId = Moment.createMoment(creatorId, lat, lng, params[0]);
                                        result.putResult(momentId);


                                    } catch (IOException e) {
                                        result.addException(e);
                                    } catch (ServerException e) {
                                        result.addException(e);
                                    } catch (NumberFormatException e) {
                                        result.addException(e);

                                    } finally {
                                        return result;
                                    }

                                }

                                @Override
                                protected void onPostExecute(AsyncTaskResult<Long> createMomentResult) {
                                    if (createMomentResult.getExceptions() != null) {
                                        for (Exception e : createMomentResult.getExceptions()) {
                                            handleException(e);
                                        }
                                    } else {
                                        returnIntentResult(createMomentResult.getResult());
                                    }
                                }
                            }.execute(imageUploaderResult.getResult());

                        }

                 }
                }

            });
            imageUploader.execute(imageContentPath);

        } else {
            // moment does not have image content
            if (textContent != null) {
                // moment has no image but has text -> upload moment only text
                AsyncTask createMoment = new AsyncTask<Void, Void, AsyncTaskResult<Long>>() {

                    @Override
                    protected AsyncTaskResult<Long> doInBackground(Void... params) {
                        AsyncTaskResult<Long> result = new AsyncTaskResult<Long>();
                        try {

                            long momentId = Moment.createMoment(creatorId, lat, lng, textContent);
                            result.putResult(momentId);

                        } catch (IOException e) {
                            result.addException(e);
                        } catch (ServerException e) {
                            result.addException(e);
                        } catch (NumberFormatException e) {
                            result.addException(e);

                        } finally {
                            return result;
                        }
                    }

                    @Override
                    protected void onPostExecute(AsyncTaskResult<Long> createMomentResult) {
                        if (createMomentResult.getExceptions() != null) {
                            for (Exception e : createMomentResult.getExceptions()) {
                                handleException(e);
                            }
                        } else {
                            returnIntentResult(createMomentResult.getResult());
                        }
                    }
                }.execute();
            } else {
                Log.d("uploadMoment", "has no image & no text");
                //TODO:: replace with xml string valuer
                Toast.makeText(this, "soemething went wrong...", Toast.LENGTH_LONG).show();
            }
        }

    }

    /**
     * returns the id of the uploaded moment and finishes this activity
     *
     * @param momentId the moment id to return;
     */
    public void returnIntentResult(long momentId) {


        Intent resultIntent = new Intent();

        resultIntent.putExtra(INTENT_EXTRA_MOMENT_ID, momentId);
        setResult(RESULT_OK, resultIntent);


        finish();
    }

    public void showRetry(boolean show){

        Button retryButton = (Button) findViewById(R.id.button_moment_uploader_retry);
        if(show){
            retryButton.setVisibility(View.VISIBLE);
        }else{
            retryButton.setVisibility(View.GONE);
        }

        TextView retryText = (TextView) findViewById(R.id.text_view_moment_uploader_retry);
        if(show){
            retryText.setVisibility(View.VISIBLE);
        }else{
            retryText.setVisibility(View.GONE);
        }

        TextView uploadText = (TextView) findViewById(R.id.text_view_moment_uploader);
        if(show){
            uploadText.setVisibility(View.GONE);
        }else{
            uploadText.setVisibility(View.VISIBLE);
        }

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_moment_uploader);
        if(show){
            progressBar.setVisibility(View.GONE);
        }else{
            progressBar.setVisibility(View.VISIBLE);
        }


    }


    //============================================================================================//
    //--- Methods used for Exception handling --------------------------------------------------------//
    //============================================================================================//

    /**
     * Method implementing the standard war of reacting to an Exception
     *
     * @param e the Exception to react to
     */
    //TODO: extend for other Exceptions if necessary
    public void handleException(Exception e) {

        if (e instanceof ServerException) {
            handleException((ServerException) e);
        } else if (e instanceof IOException) {
            handleException((IOException) e);
        } else if (e instanceof NoConnectionException){
            handleException((NoConnectionException) e);
        }
            else {
            e.printStackTrace();

            Toast.makeText(this, getString(R.string.error_something_went_wrong), Toast.LENGTH_SHORT).show();


        }
        showRetry(true);

        }


    /**
     * Method implementing the standard way of reacting to a ServerException.
     *
     * @param e the ServerException to react to.
     */
    //TODO: replace with splash screen excepiton
    public void handleException(ServerException e) {
        e.printStackTrace();
        Toast.makeText(this, "Server Exception occured", Toast.LENGTH_SHORT).show();
    }


    /**
     * Method implementing the standard way of reacting to anm IOException.
     *
     * @param e the IOException to react to.
     */
    //TODO: replace with splash screen excepiton, split into network error etc.
    public void handleException(IOException e) {
        e.printStackTrace();
        Toast.makeText(this, "IOException Exception occured", Toast.LENGTH_SHORT).show();
    }



    public void handleException(NoConnectionException e){
        e.printStackTrace();
        InternetConnectionHandler.waitForConnection(this, this);
    }

}
