package xjtlu.geomomentsdev;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.CameraPosition;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;

import xjtlu.geomomentsdev.tools.LocationHandler;
import xjtlu.geomomentsdev.tools.ImageHandle;


/**
 * An activity to preview a moment.
 *
 */
public class PreviewMomentActivity extends AppCompatActivity {

    //intent extras
    public static final String INTENT_EXTRA_IMAGE_URI = "IMAGE_URI";
    public static final String INTENT_EXTRA_IMAGE_PATH = "IMAGE_PATH";

    public static final String INTENT_EXTRA_TEXT = "TEXT";

    public static final String INTENT_EXTRA_LOCATION_LATITUDE = "LOCATION_LATITUDE";
    public static final String INTENT_EXTRA_LOCATION_LONGITUDE = "LOCATION_LONGITUDE";


    public static final String INTENT_EXTRA_CREATOR_ID = "CREATOR_ID";

    public static final String INTENT_EXTRA_CREATOR_NAME = "USER_NAME";
    public static final String INTENT_EXTRA_CREATOR_IMAGE_ID = "USER_IMAGE_ID";





    private static int REQUEST_EDIT_CONTENT = 1;
    private static int REQUEST_EDIT_LOCATION = 2;


    private String textContent;
    private String imageContentPath;
    private Uri imageContentUri;
    private Marker marker;
    private double lng;
    private double lat;
    private AMap aMap;
    private MapView mMapView;
    private long creatorId;
    private ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_moment);

        Bundle extras = getIntent().getExtras();

        //setup mapp
        mMapView = (MapView) findViewById(R.id.map_view_preview_moment_map);

        mMapView.onCreate(savedInstanceState);


        if (aMap == null) {
            aMap = mMapView.getMap();

            aMap.getUiSettings().setMyLocationButtonEnabled(false);
            aMap.setMyLocationEnabled(false);
            aMap.setTrafficEnabled(false);
            aMap.getUiSettings().setAllGesturesEnabled(false);
            aMap.getUiSettings().setZoomControlsEnabled(false);


        } else {
            aMap.clear();
            aMap.setLocationSource(LocationHandler.getInstance());
            aMap.setMyLocationEnabled(true);
            aMap = mMapView.getMap();

            aMap.getUiSettings().setMyLocationButtonEnabled(false);
            aMap.setMyLocationEnabled(false);
            aMap.setTrafficEnabled(false);
            aMap.getUiSettings().setAllGesturesEnabled(false);
            aMap.getUiSettings().setZoomControlsEnabled(false);

        }
        aMap.moveCamera(CameraUpdateFactory.zoomTo(18));



        if(extras != null){
            setContent(extras);
            setLocation(extras);

        }else{
            marker = null;
            textContent = null;
            imageContentPath = null;
            imageContentUri = null;
        }

        // fill creator view
        View creatorView = (View) findViewById(R.id.user_preview_preview_moment_creator);

        //set creator name
        if(extras.containsKey(INTENT_EXTRA_CREATOR_NAME )) {

            TextView userName = (TextView) creatorView.findViewById(R.id.text_view_user_preview_name);
            userName.setText(extras.getString(INTENT_EXTRA_CREATOR_NAME));
        }

        //set creator image
        if(extras.containsKey(INTENT_EXTRA_CREATOR_IMAGE_ID)){
            ProgressBar progressBar = (ProgressBar) creatorView.findViewById(R.id.progress_bar_user_preview_progress);
            ImageView userPicture = (ImageView) creatorView.findViewById(R.id.image_view_user_preview_profile);
            ImageHandle.showThumbImageInView(extras.getLong(INTENT_EXTRA_CREATOR_IMAGE_ID, -1), userPicture, progressBar);
        }

        //set creator id
        if(extras.containsKey(INTENT_EXTRA_CREATOR_ID)){
            creatorId = extras.getLong(INTENT_EXTRA_CREATOR_ID, -1);
        }



    }



    private void setContent(Bundle extras){

        // set text content if available
        TextView textContentView = (TextView) findViewById(R.id.text_view_preview_moment_content_text);
        if(extras.containsKey(EditContentActivity.INTENT_EXTRA_TEXT)){

            textContent = extras.getString(EditContentActivity.INTENT_EXTRA_TEXT) ;
            textContentView.setText(textContent);
            textContentView.setVisibility(View.VISIBLE);
        }else{

            textContent = null;
            textContentView.setVisibility(View.GONE);

        }

        // set image content if available
        imageView = (ImageView) findViewById(R.id.image_view_preview_moment_content_image);

        if( extras.containsKey(EditContentActivity.INTENT_EXTRA_IMAGE_PATH)
                && extras.containsKey(EditContentActivity.INTENT_EXTRA_IMAGE_URI)) {

            imageContentPath = extras.getString(EditContentActivity.INTENT_EXTRA_IMAGE_PATH);
            imageContentUri = Uri.parse(extras.getString(EditContentActivity.INTENT_EXTRA_IMAGE_URI));

            //try to recycle imageView bitmap
            try{
                ((BitmapDrawable)imageView.getDrawable()).getBitmap().recycle();

            }catch(Exception e){
                e.printStackTrace();
            }

            //sample image content with imageView width if available
            if(imageView.getMeasuredWidth() > 0){
                imageView.setImageBitmap(ImageHandle.getSampleImage(imageContentPath,
                        imageView.getMeasuredWidth()));
            }else{

            //if imageView width  is not available, wait until layout is available and set image sample for new imageView width
            ViewTreeObserver vto = imageView.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {


                @Override
                public void onGlobalLayout() {
                    imageView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    imageView.setImageBitmap(ImageHandle.getSampleImage(imageContentPath,
                            imageView.getMeasuredWidth()));
                    // imageView.setImageURI(imageContentUri);
                    imageView.setVisibility(View.VISIBLE);

                }
            });}
            imageView.setVisibility(View.VISIBLE);

        }else {

            imageContentPath = null;
            imageContentUri = null;
            imageView.setVisibility(View.GONE);
        }

    }

    /**
     * shows the location of the moment contained in the extras on the MapView
     * @param extras intent extras holding moment information
     */
    private void setLocation(Bundle extras){
        //create marker at location if available
        if(extras.containsKey(INTENT_EXTRA_LOCATION_LATITUDE)
                && extras.containsKey(INTENT_EXTRA_LOCATION_LONGITUDE)){

            lat = extras.getDouble(INTENT_EXTRA_LOCATION_LATITUDE);
            lng = extras.getDouble(INTENT_EXTRA_LOCATION_LONGITUDE);
            LatLng location = new LatLng(lat, lng);

            createMarkerAtLocation(location);

        }else{
            marker = null;
        }
    }

    /**
     * Creates a marker on the map at the given location if the location is within the users range
     * @param latLng the LatLng representation of the location to create a marker at
     * @return the created marker
     */
    private Marker createMarkerAtLocation(LatLng latLng){
        if(marker != null) {
            marker.destroy();
            marker = null;
        }
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageHandle.bitmapFromVectorAsset(this, R.drawable.ic_place_white_24dp, R.color.black)));
        marker = aMap.addMarker(markerOptions);
        aMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(latLng, aMap.getCameraPosition().zoom)));

        return(marker);

    }
    /**
     * starts an EditLocationActivity with the location of the moment.
     * @param view
     */
    public void triggerEditLocation(View view){

        Intent editLocation = new Intent(this, EditLocationActivity.class);

        //add moment location to intent
        if(marker!= null){
            editLocation.putExtra(EditLocationActivity.INTENT_EXTRA_LOCATION_LATITUDE, marker.getPosition().latitude);
            editLocation.putExtra(EditLocationActivity.INTENT_EXTRA_LOCATION_LONGITUDE, marker.getPosition().longitude);
        }
        startActivityForResult(editLocation, REQUEST_EDIT_LOCATION);


    }


    /**
     * starts an EditContentActivity with the content of the moment.
     * @param view
     */
    public void triggerEditContent(View view){

        Intent editContent = new Intent(this, EditContentActivity.class);

        // add moment content to intent
        if(textContent != null) {
            editContent.putExtra(EditContentActivity.INTENT_EXTRA_TEXT, textContent);

        }
        if(imageContentPath != null && imageContentUri != null) {
            editContent.putExtra(EditContentActivity.INTENT_EXTRA_IMAGE_PATH, imageContentPath);
            editContent.putExtra(EditContentActivity.INTENT_EXTRA_IMAGE_URI, imageContentUri.toString());
        }

        startActivityForResult(editContent, REQUEST_EDIT_CONTENT);

    }

    /**
     * returns the moments creator, location, text- and image content and finishes this activity.
     */
    private void submit(){

        Intent resultIntent = new Intent();

        resultIntent.putExtra(INTENT_EXTRA_LOCATION_LATITUDE, lat);
        resultIntent.putExtra(INTENT_EXTRA_LOCATION_LONGITUDE, lng);

        //set text content if available
        if(textContent != null) {
            resultIntent.putExtra(INTENT_EXTRA_TEXT, textContent);
        }
        //set image content if available
        if(imageContentUri != null && imageContentPath != null){
            resultIntent.putExtra(INTENT_EXTRA_IMAGE_PATH, imageContentPath);
            resultIntent.putExtra(INTENT_EXTRA_IMAGE_URI, imageContentUri);
        }

        //set creator id if valid
        if(creatorId != -1){
            resultIntent.putExtra(INTENT_EXTRA_CREATOR_ID, creatorId);
        }

        setResult(RESULT_OK, resultIntent);
        finish();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_EDIT_LOCATION && resultCode == RESULT_OK) {
            //if edit location returns, set new location

            Bundle extras = data.getExtras();
            setLocation(extras);
        }
        if(requestCode == REQUEST_EDIT_CONTENT && resultCode == RESULT_OK){
            //if edit content returns, set new content
            Bundle extras = data.getExtras();
            setContent(extras);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_preview_moment, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.menu_item_preview_moment_submit:
                submit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onDestroy(){
        mMapView.onDestroy();
        try{

            View creatorView = (View) findViewById(R.id.user_preview_preview_moment_creator);
            ImageView userPicture = (ImageView) creatorView.findViewById(R.id.image_view_user_preview_profile);

            ((BitmapDrawable)userPicture.getDrawable()).getBitmap().recycle();

        }catch(Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



}
