package xjtlu.geomomentsdev;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;

import xjtlu.geomomentsdev.tools.AsyncImageUploader;
import xjtlu.geomomentsdev.tools.AsyncTaskResult;
import xjtlu.geomomentsdev.tools.AsyncUserUpdater;
import xjtlu.geomomentsdev.tools.DatabaseHandler;
import xjtlu.geomomentsdev.tools.InternetConnectionHandler;
import xjtlu.geomomentsdev.tools.LocationHandler;
import xjtlu.geomomentsdev.tools.NoConnectionException;
import xjtlu.geomomentsdev.tools.PreferenceHandler;
import xjtlu.geomomentsdev.tools.ServerException;
import xjtlu.geomomentsdev.tools.User;

public class MainNavigationActivity extends AppCompatActivity {

    private static final int REQUEST_SELECT_LOCATION = 1;
    private static final int REQUEST_SELECT_CONTENT = 2;
    private static final int REQUEST_PREVIEW_MOMENT = 3;
    private static final int REQUEST_UPLOAD_MOMENT = 4;
    private static final int REQUEST_EDIT_PROFILE_PICTURE = 5;
    private static final int REQUEST_EDIT_USER_NAME = 6;
    private static final int REQUEST_EDIT_USER_EMAIL = 7;
    private static final int REQUEST_EDIT_USER_STATUS = 8;
    private static final int REQUEST_MANAGE_MY_MOMENTS = 9;


    private double lng;
    private double lat;
    private User loggedInUser;
    private long downloadMomentId;
    private SectionsPagerAdapter sectionsPagerAdapter;
    private MapViewFragment mapViewFragment;
    private ProfileFragment profileFragment;
    private BrowserFragment browserFragment;
    private Long activeMoment = null;
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //start location handler
        LocationHandler.instantiate(this.getApplicationContext());

        // set language
        PreferenceHandler.instantiate(this.getApplicationContext());

        InternetConnectionHandler.setContext(this);

        User.initializeUserDatabase(this.getApplicationContext());
        databaseHandler = DatabaseHandler.getInstance(this);

        String language = PreferenceHandler.getLanguage();
        if (language != null) {
            setLanguage(language);
        }

        setContentView(R.layout.activity_main_navigation);

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager_main_navigation_container);
        viewPager.setAdapter(sectionsPagerAdapter);

        // set up TabLayout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout_main_navigation_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(SectionsPagerAdapter.POSITION_MAP_VIEW_FRAGMENT).setIcon(R.drawable.ic_explore_white_24dp);

        tabLayout.getTabAt(SectionsPagerAdapter.POSITION_BROWSER_FRAGMENT).setIcon(R.drawable.ic_view_list_white_24dp);
        tabLayout.getTabAt(SectionsPagerAdapter.POSITION_PROFILE_FRAGMENT).setIcon(R.drawable.ic_person_white_24dp);


        loggedInUser = User.getLoggedInUser();
    }

    public ProfileFragment getProfileFragment(){
        return ((ProfileFragment) this.sectionsPagerAdapter.getItem(sectionsPagerAdapter.POSITION_PROFILE_FRAGMENT));
    }

    public MapViewFragment getMapViewFragment(){
        return ((MapViewFragment) this.sectionsPagerAdapter.getItem(SectionsPagerAdapter.POSITION_MAP_VIEW_FRAGMENT));
    }

    public BrowserFragment getBrowserFragment(){
        return((BrowserFragment) this.sectionsPagerAdapter.getItem(SectionsPagerAdapter.POSITION_BROWSER_FRAGMENT));
    }
/*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_navigation, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_language_english:

                setLanguage("en");
                PreferenceHandler.setLanguage("en");
                refresh();
                return true;

            case R.id.menu_item_language_german:

                setLanguage("de");
                PreferenceHandler.setLanguage("de");
                refresh();
                return true;

            case R.id.menu_item_language_chinese:
                PreferenceHandler.setLanguage("cn");
                setLanguage("cn");
                refresh();
                return true;

            case R.id.menu_item_main_menu_logout:
                logout();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    /**
     * refreshes the activity by starting a new instance of the activity and finishing this one.
     */
    public void refresh() {
        Intent refresh = new Intent(this, MainNavigationActivity.class);
        startActivity(refresh);
        finish();
    }

    /**
     * sets the Locale to for the specified languageCOde to change the language.
     *
     * @param languageCode the language code to set the Locale to.
     */
    public void setLanguage(String languageCode) {

        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

    }

    /**
     * logout the logged in user and start the LoginActivity.
     */
    public void logout() {
        User.logout();
        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }

    //============================================================================================//
    //--- Methods used in MapViewFragment --------------------------------------------------------//
    //============================================================================================//

    /**
     * starts the EditLocationActivity as part of adding a new Geomoment without passing anh initial location.
     */
    public void triggerSelectLocation() {
        // navigate to edit location
        Log.d("selectLocation", "trigger select location");
        Intent selectLocation = new Intent(this, EditLocationActivity.class);
        startActivityForResult(selectLocation, REQUEST_SELECT_LOCATION);


    }

    /**
     * starts the EditLocationActivity as part of adding a new Geomoment and passes the given lat and lng as initial location.
     *
     * @param lat the latitude of the initial location.
     * @param lng the longitude of the initial location.
     */
    public void triggerSelectLocation(double lat, double lng) {
        Intent selectLocation = new Intent(this, EditLocationActivity.class);
        selectLocation.putExtra(EditLocationActivity.INTENT_EXTRA_LOCATION_LATITUDE, lat);
        selectLocation.putExtra(EditLocationActivity.INTENT_EXTRA_LOCATION_LONGITUDE, lng);
        startActivityForResult(selectLocation, REQUEST_SELECT_LOCATION);
    }


    /**
     * starts the select content activity as part of adding a new Geomoment.
     */
    public void triggerSelectContent() {
        Intent selectContent = new Intent(this, EditContentActivity.class);
        startActivityForResult(selectContent, REQUEST_SELECT_CONTENT);

    }


    /**
     * starts the PreviewMomentActivity as part of adding a new Geomoment.
     *
     * @param contentActivityextras the intent extras holding the moment data as returned form a select content activity.
     */
    public void triggerPreviewMoment(Bundle contentActivityextras) {

        Intent previewMoment = new Intent(this, PreviewMomentActivity.class);
        if (contentActivityextras != null) {

            //get location
            previewMoment.putExtra(PreviewMomentActivity.INTENT_EXTRA_LOCATION_LATITUDE, lat);
            previewMoment.putExtra(PreviewMomentActivity.INTENT_EXTRA_LOCATION_LONGITUDE, lng);

            //get text content if exists
            if (contentActivityextras.containsKey(EditContentActivity.INTENT_EXTRA_TEXT)) {
                previewMoment.putExtra(EditContentActivity.INTENT_EXTRA_TEXT, contentActivityextras.getString(EditContentActivity.INTENT_EXTRA_TEXT));
            }

            //get imagecontent if exists
            if (contentActivityextras.containsKey(EditContentActivity.INTENT_EXTRA_IMAGE_PATH)
                    && contentActivityextras.containsKey(EditContentActivity.INTENT_EXTRA_IMAGE_URI)) {
                previewMoment.putExtra(EditContentActivity.INTENT_EXTRA_IMAGE_PATH, contentActivityextras.getString(EditContentActivity.INTENT_EXTRA_IMAGE_PATH));
                previewMoment.putExtra(EditContentActivity.INTENT_EXTRA_IMAGE_URI, contentActivityextras.getString(EditContentActivity.INTENT_EXTRA_IMAGE_PATH));


            }

            //get logged in user as creator
            if (loggedInUser != null) { // this should always be the case

                previewMoment.putExtra(PreviewMomentActivity.INTENT_EXTRA_CREATOR_NAME, loggedInUser.getName());

                //get creator profile picture if exists
                if (loggedInUser.getProfilePicture() != null) {
                    previewMoment.putExtra(PreviewMomentActivity.INTENT_EXTRA_CREATOR_IMAGE_ID, loggedInUser.getProfilePicture().getId());

                }
                previewMoment.putExtra(PreviewMomentActivity.INTENT_EXTRA_CREATOR_ID, loggedInUser.getId());
            }


        } else {
            //debugging..
            Log.d("previewMoment", "trigger preview moment extras is null");
        }
        startActivityForResult(previewMoment, REQUEST_PREVIEW_MOMENT);
    }


    /**
     * starts the MomentUploaderActivity as part of adding a new Geomoment.
     *
     * @param previewActivityExtras the intent extras holding the moment data as returned form a  PreviewMomentActivity.
     */
    public void triggerUploadMoment(Bundle previewActivityExtras) {
        Intent uploadMoment = new Intent(this, MomentUploaderActivity.class);


        if (previewActivityExtras != null) {
            //get location
            if (previewActivityExtras.containsKey(PreviewMomentActivity.INTENT_EXTRA_LOCATION_LONGITUDE)
                    && previewActivityExtras.containsKey(PreviewMomentActivity.INTENT_EXTRA_LOCATION_LATITUDE)) {

                uploadMoment.putExtra(MomentUploaderActivity.INTENT_EXTRA_LOCATION_LONGITUDE,
                        previewActivityExtras.getDouble(PreviewMomentActivity.INTENT_EXTRA_LOCATION_LONGITUDE));

                uploadMoment.putExtra(MomentUploaderActivity.INTENT_EXTRA_LOCATION_LATITUDE,
                        previewActivityExtras.getDouble(PreviewMomentActivity.INTENT_EXTRA_LOCATION_LATITUDE));

            }

            // get text content if exists
            if (previewActivityExtras.containsKey(PreviewMomentActivity.INTENT_EXTRA_TEXT)) {
                uploadMoment.putExtra(MomentUploaderActivity.INTENT_EXTRA_TEXT, previewActivityExtras.getString(PreviewMomentActivity.INTENT_EXTRA_TEXT));
            }

            //get image content if exists
            if (previewActivityExtras.containsKey(PreviewMomentActivity.INTENT_EXTRA_IMAGE_PATH)) {
                uploadMoment.putExtra(MomentUploaderActivity.INTENT_EXTRA_IMAGE_PATH,
                        previewActivityExtras.getString(PreviewMomentActivity.INTENT_EXTRA_IMAGE_PATH));
            }

            //get creator if exists
            if (previewActivityExtras.containsKey(PreviewMomentActivity.INTENT_EXTRA_CREATOR_ID)) {
                Log.d("triggerUploadMoment", "taking creator id from preview: " + Long.toString(previewActivityExtras.getLong(PreviewMomentActivity.INTENT_EXTRA_CREATOR_ID)));
                uploadMoment.putExtra(MomentUploaderActivity.INTENT_EXTRA_CREATOR_ID,
                        previewActivityExtras.getLong(PreviewMomentActivity.INTENT_EXTRA_CREATOR_ID));
            } else {
                Log.d("triggerUploadMoment", "o creator id from preview");
            }
        }
        startActivityForResult(uploadMoment, REQUEST_UPLOAD_MOMENT);

    }

    //============================================================================================//
    //--- Methods used in MapViewFragment and BrowserFragment ------------------------------//
    //============================================================================================//

    /**
     * sets the currentyl active moment, used to show moments from MapViewFragment in BrowserFragment and vice versa.
     * @param momentId the id of the active moment.
     */
    public void setActiveMoment(Long momentId){
        activeMoment = momentId;
    }

    /**
     * returns the id of the currently active moment, sets the active Moment to null afterwards
     * @return the id of the currently active moment
     */
    public Long takeActiveMoment(){
        Long tmp = activeMoment;
        activeMoment = null;

        return tmp;
    }


    //============================================================================================//
    //--- Methods used in ProfileFragment --------------------------------------------------------//
    //============================================================================================//


    /**
     * starts an FullImageActivity to show the logged in user's profile picture in full size.
     */
    public void triggerShowFullProfilePicture() {

        if (loggedInUser.getProfilePicture() != null) {
            Intent fullImage = new Intent(this, FullImageActivity.class);
            fullImage.putExtra(FullImageActivity.INTENT_EXTRA_IMAGE_ID, loggedInUser.getProfilePicture().getId());
            startActivity(fullImage);
        } else {
            Toast.makeText(this, getString(R.string.error_no_image_selcted), Toast.LENGTH_LONG);
        }
    }

    /**
     * starts an ImagePickerActivity to edit the logged in user's profile picture.
     */
    public void editProfilePicture() {

        Intent imagePickerIntent = new Intent(this, ImagePickerActivity.class);
        startActivityForResult(imagePickerIntent, REQUEST_EDIT_PROFILE_PICTURE);

    }

    /**
     * sets the logged in users profile picture to the image at the specified path.
     *
     * @param imagePath the path of the new profile picture.
     */
    public void editProfilePicture(String imagePath) {

        //upload profile Picture

        profileFragment.getProfilePictureProgress().setVisibility(View.VISIBLE);
        AsyncImageUploader imageUploader = (AsyncImageUploader) new AsyncImageUploader(new AsyncImageUploader.AsyncResponse() {
            @Override
            public void processFinish(AsyncTaskResult<Long> imageUploaderResult) {
                //handle image upload exceptions
                if(imageUploaderResult.getExceptions() != null) {
                    for (Exception e : imageUploaderResult.getExceptions()) {
                        handleException(e);

                        profileFragment.getProfilePictureProgress().setVisibility(View.GONE);
                    }
                }else{



                //update User data
                loggedInUser.setProfilePicture(imageUploaderResult.getResult());
                databaseHandler.insertOrUpdateUser(loggedInUser);

                //upload updated user
                AsyncUserUpdater userUpdater = (AsyncUserUpdater) new AsyncUserUpdater(new AsyncUserUpdater.AsyncResponse() {
                    @Override
                    public void processFinish(AsyncTaskResult<String> userUpdaterResult) {
                        //handle exceptions
                        if (userUpdaterResult.getExceptions() != null) {
                            for (Exception e : userUpdaterResult.getExceptions()) {
                                handleException(e);

                                profileFragment.getProfilePictureProgress().setVisibility(View.GONE);
                            }
                        }else{
                            profileFragment.refresh(true);
                        }

                        profileFragment.getProfilePictureProgress().setVisibility(View.GONE);
                    }
                }).execute(loggedInUser);


            }}

        }).execute(imagePath);
    }

    /**
     * starts a EditTextActivity to edit the user name.
     */
    public void editUserName() {
        Intent editTextIntent = new Intent(this, EditTextActivity.class);
        editTextIntent.putExtra(EditTextActivity.INTENT_EXTRA_TEXT, loggedInUser.getName());
        editTextIntent.putExtra(EditTextActivity.INTENT_EXTRA_MAX_LENGTH, 64);
        startActivityForResult(editTextIntent, REQUEST_EDIT_USER_NAME);
    }

    /**
     * sets the logged in users name to the specified value.
     *
     * @param userName the new user name.
     */
    public void editUserName(final String userName) {

        AsyncUserUpdater userUpdater = (AsyncUserUpdater) new AsyncUserUpdater(new AsyncUserUpdater.AsyncResponse() {
            @Override
            public void processFinish(AsyncTaskResult<String> result) {
                //handle exceptions
                if (result.getExceptions() != null) {
                    for (Exception e : result.getExceptions()) {
                        handleException(e);
                    }
                }else{
                    loggedInUser.setName(userName);
                    databaseHandler.insertOrUpdateUser(loggedInUser);
                    profileFragment.refresh(false);
                }
            }
        }).execute(loggedInUser);


    }

    /**
     * starts a EditTextActivity to edit the user email.
     */
    public void editUserEmail() {
        Intent editTextIntent = new Intent(this, EditTextActivity.class);
        editTextIntent.putExtra(EditTextActivity.INTENT_EXTRA_TEXT, loggedInUser.getEmail());
        editTextIntent.putExtra(EditTextActivity.INTENT_EXTRA_MAX_LENGTH, 64);
        startActivityForResult(editTextIntent, REQUEST_EDIT_USER_EMAIL);
    }

    /**
     * sets the logged in users email to the specified value.
     *
     * @param userEmail the new user email.
     */
    public void editUserEmail(final String userEmail) {
        AsyncUserUpdater userUpdater = (AsyncUserUpdater) new AsyncUserUpdater(new AsyncUserUpdater.AsyncResponse() {
            @Override
            public void processFinish(AsyncTaskResult<String> result) {
                //handle exceptions
                if (result.getExceptions() != null) {
                    for (Exception e : result.getExceptions()) {
                        handleException(e);
                    }
                }else{
                    loggedInUser.setEmail(userEmail);
                    databaseHandler.insertOrUpdateUser(loggedInUser);
                    profileFragment.refresh(false);
                }
            }
        }).execute(loggedInUser);




    }

    /**
     * starts a EditTextActivity to edit the user status.
     */
    public void editUserStatus() {
        Intent editTextIntent = new Intent(this, EditTextActivity.class);
        editTextIntent.putExtra(EditTextActivity.INTENT_EXTRA_TEXT, loggedInUser.getStatus());
        editTextIntent.putExtra(EditTextActivity.INTENT_EXTRA_MAX_LENGTH, 128);
        startActivityForResult(editTextIntent, REQUEST_EDIT_USER_STATUS);
    }

    /**
     * sets the logged in users status to the specified value.
     *
     * @param userStatus
     */
    public void editUserStatus(final String userStatus) {

        AsyncUserUpdater userUpdater = (AsyncUserUpdater) new AsyncUserUpdater(new AsyncUserUpdater.AsyncResponse() {
            @Override
            public void processFinish(AsyncTaskResult<String> result) {
                //handle exceptions
                if (result.getExceptions() != null) {
                    for (Exception e : result.getExceptions()) {
                        handleException(e);
                    }
                }else{
                    loggedInUser.setStatus(userStatus);
                    databaseHandler.insertOrUpdateUser(loggedInUser);
                    profileFragment.refresh(false);
                }
            }
        }).execute(loggedInUser);


    }

    /**
     * starts a MomentManagerActivity to manage the logged in users moments.
     */
    public void manageMyMoments() {
        Intent browseMoment = new Intent(this, MomentManagerActivity.class);
        browseMoment.putExtra(MomentManagerActivity.INTENT_EXTRA_WHERE_STMT, " " + DatabaseHandler.MOMENTS_CREATOR + " = " + Long.toString(loggedInUser.getId()));
        browseMoment.putExtra(MomentManagerActivity.INTENT_EXTRA_ORDER_BY_STMT, " " + DatabaseHandler.MOMENTS_TIMESTAMP + " DESC");

        startActivityForResult(browseMoment, REQUEST_MANAGE_MY_MOMENTS);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            // When location is selected
            if (requestCode == REQUEST_SELECT_LOCATION && resultCode == RESULT_OK && null != data) {

                lat = data.getDoubleExtra(EditLocationActivity.INTENT_EXTRA_LOCATION_LATITUDE, -1);
                lng = data.getDoubleExtra(EditLocationActivity.INTENT_EXTRA_LOCATION_LONGITUDE, -1);

                triggerSelectContent();
            }

            if (requestCode == REQUEST_SELECT_CONTENT && resultCode == RESULT_OK && null != data) {

                Bundle extras = data.getExtras();
                triggerPreviewMoment(extras);
            }

            if (requestCode == REQUEST_PREVIEW_MOMENT && resultCode == RESULT_OK && null != data) {

                Bundle extras = data.getExtras();
                triggerUploadMoment(extras);

            }

            if (requestCode == REQUEST_UPLOAD_MOMENT && resultCode == RESULT_OK && null != data) {
                long momentId = data.getExtras().getLong(MomentUploaderActivity.INTENT_EXTRA_MOMENT_ID);
                if (momentId == -1) {

                    Toast.makeText(mapViewFragment.getContext(), "moment something went wrong", Toast.LENGTH_LONG).show();
                } else {
                    setActiveMoment(momentId);
                    mapViewFragment.plotData(true);
                    downloadMomentId = momentId;
                    Toast.makeText(mapViewFragment.getContext(), "moment created successfully", Toast.LENGTH_LONG).show();
                }
            }

            if (requestCode == REQUEST_EDIT_PROFILE_PICTURE && resultCode == RESULT_OK && null != data) {
                if (data.getExtras().containsKey(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGE_PATH)) {
                    Log.d("editProfilePicture", "edit profile picture returned");
                    editProfilePicture(data.getExtras().getString(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGE_PATH));
                }
            }


            if (requestCode == REQUEST_EDIT_USER_NAME && resultCode == RESULT_OK && null != data) {
                if (data.getExtras().containsKey(EditTextActivity.INTENT_EXTRA_TEXT)) {
                    editUserName(data.getExtras().getString(EditTextActivity.INTENT_EXTRA_TEXT));
                }
            }
            if (requestCode == REQUEST_EDIT_USER_STATUS && resultCode == RESULT_OK && null != data) {
                if (data.getExtras().containsKey(EditTextActivity.INTENT_EXTRA_TEXT)) {
                    editUserStatus(data.getExtras().getString(EditTextActivity.INTENT_EXTRA_TEXT));
                }
            }
            if (requestCode == REQUEST_EDIT_USER_EMAIL && resultCode == RESULT_OK && null != data) {
                if (data.getExtras().containsKey(EditTextActivity.INTENT_EXTRA_TEXT)) {
                    editUserEmail(data.getExtras().getString(EditTextActivity.INTENT_EXTRA_TEXT));
                }
            }
            if (requestCode == REQUEST_MANAGE_MY_MOMENTS && resultCode == RESULT_OK && null != data) {
                if (data.getExtras().containsKey(MomentManagerActivity.INTENT_EXTRA_DELETED_MOMENTS)) {
                    if (data.getExtras().getInt(MomentManagerActivity.INTENT_EXTRA_DELETED_MOMENTS) > 0) {
                        LocationHandler.refreshMoments();
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        //    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
          //          .show();
        }
    }


    //============================================================================================//
    //--- Methods used for Exception handling --------------------------------------------------------//
    //============================================================================================//

    /**
     * Method implementing the standard war of reacting to an Exception
     *
     * @param e the Exception to react to
     */
    //TODO: extend for other Exceptions if necessary
    public void handleException(Exception e) {

        if (e instanceof ServerException) {
            handleException((ServerException) e);
        } else if (e instanceof IOException) {
            handleException((IOException) e);
        } else if (e instanceof NoConnectionException){
            handleException((NoConnectionException) e);
        }
        else {
            Toast.makeText(this, getString(R.string.error_something_went_wrong), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }


    /**
     * Method implementing the standard way of reacting to a ServerException.
     *
     * @param e the ServerException to react to.
     */
    //TODO: replace with splash screen excepiton
    public void handleException(ServerException e) {
        e.printStackTrace();
        Toast.makeText(this, "Server Exception occured", Toast.LENGTH_SHORT).show();
    }


    /**
     * Method implementing the standard way of reacting to anm IOException.
     *
     * @param e the IOException to react to.
     */
    //TODO: replace with splash screen exception, split into network error etc.
    public void handleException(IOException e) {
        e.printStackTrace();
        Toast.makeText(this, "IOException Exception occured", Toast.LENGTH_SHORT).show();
    }


    public void handleException(NoConnectionException e){
        e.printStackTrace();

        InternetConnectionHandler.waitForConnection(this, this);
    }


    /**
     * Adapter to instantiate the fragments
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public static final int POSITION_MAP_VIEW_FRAGMENT = 0;
        public static final int POSITION_BROWSER_FRAGMENT = 1;
        public static final int POSITION_PROFILE_FRAGMENT = 2;


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }



        ///helper function to get Fragment Tag as found at: http://stackoverflow.com/questions/14035090/how-to-get-existing-fragments-when-using-fragmentpageradapter
        private String getFragmentTag(int viewPagerId, int fragmentPosition){
            return "android:switcher:" + viewPagerId + ":" +  fragmentPosition;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case POSITION_MAP_VIEW_FRAGMENT:

                    mapViewFragment = MapViewFragment.newInstance();
                    Log.d("Fragments", "mapViewFragment new instance -> id: " + mapViewFragment.getId());
                    return mapViewFragment;

                case POSITION_BROWSER_FRAGMENT:

                    browserFragment = BrowserFragment.newInstance(loggedInUser.getId());
                    Log.d("Fragments", "browserFragment new instance -> id: " + mapViewFragment.getId());
                    return browserFragment;

                case POSITION_PROFILE_FRAGMENT:

                    profileFragment = ProfileFragment.newInstance();
                    return (profileFragment);
            }
            return null;


        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
         /*   switch (position) {
                case POSITION_MAP_VIEW_FRAGMENT:
                    return getString(R.string.map);
                case POSITION_BROWSER_FRAGMENT:
                    return getString(R.string.nearby);
                case POSITION_PROFILE_FRAGMENT:
                    return getString(R.string.profile);
            }*/
            return null;
        }
    }
}


