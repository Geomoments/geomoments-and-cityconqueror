package xjtlu.geomomentsdev;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.CameraPosition;
import com.amap.api.maps2d.model.Circle;
import com.amap.api.maps2d.model.CircleOptions;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.maps2d.model.MyLocationStyle;

import xjtlu.geomomentsdev.tools.ImageHandle;
import xjtlu.geomomentsdev.tools.InternetConnectionHandler;
import xjtlu.geomomentsdev.tools.LocationDependentActivity;
import xjtlu.geomomentsdev.tools.LocationHandler;

/**
 * A activity to select a location or edit a given location within range as part of creating a new Geomoment.
 * implements LocationDependentActivity to update the location range according to location changes in LocationHandler.
 */
public class EditLocationActivity extends AppCompatActivity implements LocationDependentActivity {

    // intent extra keys
    public static final String INTENT_EXTRA_LOCATION_LATITUDE = "LOCATION_LATITUDE";
    public static final String INTENT_EXTRA_LOCATION_LONGITUDE = "LOCATION_LONGITUDE";


    private MapView mMapView;
    private AMap aMap;
    private Marker marker;
    private Circle circle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_location);

        LocationHandler.registerLocationDependentActivity(this);

        //initialize Map

        //recycle MapView if possible
        if (mMapView != null) {
            mMapView.onDestroy();
            mMapView = null;
        }

        mMapView = (MapView) findViewById(R.id.map_view_fragment_map_map);
        mMapView.onCreate(savedInstanceState);

        if (aMap == null) {
            aMap = mMapView.getMap();
        }


        //set my location stlye
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.myLocationIcon(BitmapDescriptorFactory
                .fromBitmap(ImageHandle.bitmapFromVectorAsset(this, R.drawable.ic_radio_button_checked_black_16dp,this.getResources().getColor(R.color.colorAccent))));
        //hide standard location circle
        myLocationStyle.strokeColor(Color.TRANSPARENT);
        myLocationStyle.radiusFillColor(Color.TRANSPARENT);
        myLocationStyle.strokeWidth(1.0f);

        aMap.setMyLocationStyle(myLocationStyle);

        aMap.setLocationSource(LocationHandler.getInstance());
        aMap.getUiSettings().setMyLocationButtonEnabled(true);
        aMap.setMyLocationEnabled(true);

        //set map longClick
        aMap.setOnMapLongClickListener(new AMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                createMarkerAtLocation(latLng);

            }
        });

        aMap.moveCamera(CameraUpdateFactory.zoomTo(18));


        //if intent has LatLng then create initial marker, else set marker null
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(INTENT_EXTRA_LOCATION_LATITUDE)
                    && extras.containsKey(INTENT_EXTRA_LOCATION_LONGITUDE)) {

                double lat = extras.getDouble(INTENT_EXTRA_LOCATION_LATITUDE);
                double lng = extras.getDouble(INTENT_EXTRA_LOCATION_LONGITUDE);

                LatLng location = new LatLng(lat, lng);
                createMarkerAtLocation(location);


            } else {
                marker = null;
                if(LocationHandler.getMyLat() != null && LocationHandler.getMyLng() != null){
                    LatLng location = new LatLng(LocationHandler.getMyLat(), LocationHandler.getMyLng());
                    createMarkerAtLocation(location);
                }
            }
        } else {
            marker = null;
            if(LocationHandler.getMyLat() != null && LocationHandler.getMyLng() != null){
                LatLng location = new LatLng(LocationHandler.getMyLat(), LocationHandler.getMyLng());
                createMarkerAtLocation(location);
            }
        }

        drawCircle(LocationHandler.getMyLat(), LocationHandler.getMyLng());

        InternetConnectionHandler.setContext(this);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_edit_location_submit:
                submit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * draws a circle to illustrate the 200m range around the current location in the mapView
     * @param lat the latitude of the circle center.
     * @param lng the longitude of the circle center
     */
    private void drawCircle(double lat, double lng) {
        //remove existing circle, if any
        if (circle != null) {
            circle.remove();
        }

        circle = aMap.addCircle(
                new CircleOptions().center(new LatLng(lat, lng))
                        .strokeColor(Color.BLUE).strokeWidth(2).radius(LocationHandler.RANGE_IN_METERS)
        );//set the radius of the circle on the map.
    }

    /**
     * Creates a marker on the map at the given location if the location is within the users range
     * @param latLng the LatLng representation of the location to create a marker at
     * @return the created marker
     */
    private Marker createMarkerAtLocation(LatLng latLng) {
        if (LocationHandler.getMyDistanceTo(latLng.latitude, latLng.longitude) > LocationHandler.RANGE_IN_METERS) {
            Toast.makeText(this, getString(R.string.position_out_of_range), Toast.LENGTH_LONG).show();

        } else {
            if (marker != null) {
                marker.destroy();
                marker = null;
            }
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageHandle.bitmapFromVectorAsset(this, R.drawable.ic_add_location_black_24dp, Color.BLACK)));
            marker = aMap.addMarker(markerOptions);
            aMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(latLng, aMap.getCameraPosition().zoom)));

            return (marker);
        }
        return (null);
    }


    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();

    }

    /**
     * Method to return the selected location and finish this activity
     */
    private void submit() {

        if (marker != null) {

            Intent resultIntent = new Intent();
            //add marker position to intent extras
            resultIntent.putExtra(INTENT_EXTRA_LOCATION_LATITUDE, marker.getPosition().latitude);
            resultIntent.putExtra(INTENT_EXTRA_LOCATION_LONGITUDE, marker.getPosition().longitude);

            setResult(RESULT_OK, resultIntent);

            //unlink from location handler
            LocationHandler.dropLocationDependentActivity(this);

            finish();
        } else {// alert if no marker is set
            Toast.makeText(this, getString(R.string.no_location_selected), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * called if location updated in LocationHandler.
     * updates the range circle and marker when the location is updated.
     */
    @Override
    public void locationDependentRefresh() {
        drawCircle(LocationHandler.getMyLat(), LocationHandler.getMyLng());
        createMarkerAtLocation(new LatLng(LocationHandler.getMyLat(), LocationHandler.getMyLng()));
    }

    @Override
    public void manageMomentsDownloadException(Exception e) {
        //do nothing because downloaded Moments don't matter in this activity
    }
}
