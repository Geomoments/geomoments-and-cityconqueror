package xjtlu.geomomentsdev.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import xjtlu.geomomentsdev.R;

/**
 * A class implementing an Async task to download an image from the server and returning a bitmap sampled to a specified width.
 * Created by tmp on 6/21/2016.
 */
public class AsyncBitmapImageDownloader extends AsyncTask<Object, Void, AsyncTaskResult<Bitmap>> {

    public static Long FULL_IMAGE = new Long(0);
    public static Long THUMB_IMAGE = new Long(1);

    private HashMap<String, String> params = new HashMap<String, String>();
    private String downloadImageResponse;
    private ImageView downloadImageView;

    public int imageViewWidth;
    public AsyncBitmapImageDownloader(){

    }



    public AsyncBitmapImageDownloader(int reqWidth){
        imageViewWidth = reqWidth;

    }


    @Override
    protected  void onPreExecute(){ }

    /**
     * downloads image from server
     * @param taskParams    taskParams[0] = (Long) id of image to download in server database,
     *                      taskParams[1] = (Long) FULL_IMAGE or THUMB_IMAGE to specify which to download,
     *                      taskParams[2] = (ImageView) imageView to set image in
     *
     * @return Bitmap of downloaded image
     * **/
    @Override
    protected AsyncTaskResult<Bitmap> doInBackground(Object... taskParams) {

        params.put(HttpRequestHandler.DOWNLOAD_IMAGE_PARAMS[0], Long.toString((Long) taskParams[0]));
        if (((Long) taskParams[1]).equals(FULL_IMAGE)) {
            params.put(HttpRequestHandler.DOWNLOAD_IMAGE_PARAMS[1], "full");
        }
        if (((Long) taskParams[1]).equals(THUMB_IMAGE)) {
            params.put(HttpRequestHandler.DOWNLOAD_IMAGE_PARAMS[1], "thumb");
        }



            downloadImageResponse = null;
        AsyncTaskResult<Bitmap> response = new AsyncTaskResult<Bitmap>();

            try {
                downloadImageResponse = HttpRequestHandler.post(HttpRequestHandler.DOWNLOAD_IMAGE_URL, params);

                response.putResult(decodeSampleImage(downloadImageResponse, imageViewWidth));

            } catch (Exception e) {
                response.addException(e);
            }

            return response;
        }

        @Override
        protected void onPostExecute (AsyncTaskResult<Bitmap> result) {

        }

    /**
     * A method to decode an image encoded in a string to a bitmap sampled for the given width.
     * @param encodedImage the encoded Image string to decode to bitmap.
     * @param reqWidth the required width of the resulting bitmap.
     * @return the bitmap with the required width sampled from the encodedImage.
     */
    public static Bitmap decodeSampleImage(String encodedImage, int reqWidth) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        byte[] byte_arr = Base64.decode(encodedImage, 0);

        BitmapFactory.decodeByteArray(byte_arr, 0, byte_arr.length, options);

        // Calculate inSampleSize
        options.inSampleSize = ImageHandle.calculateInSampleSize(options, reqWidth);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;


        return BitmapFactory.decodeByteArray(byte_arr, 0, byte_arr.length, options);
    }


}

