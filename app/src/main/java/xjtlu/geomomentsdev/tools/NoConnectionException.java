package xjtlu.geomomentsdev.tools;

/**
 * Created by VRLAB_DevThree on 7/20/2016.
 */

    public class NoConnectionException extends Exception{


        public NoConnectionException(String message){
            super(message);
        }
        public NoConnectionException(){
            super();
        }

    }
