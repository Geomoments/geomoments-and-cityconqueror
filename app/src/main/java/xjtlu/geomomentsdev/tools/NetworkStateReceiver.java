package xjtlu.geomomentsdev.tools;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

/** from: http://stackoverflow.com/questions/3767591/check-intent-internet-connection
 * Created by VRLAB_DevThree on 7/4/2016.
 */
public class NetworkStateReceiver extends BroadcastReceiver {
    private static final String TAG = "NetworkStateReceiver";

    @Override
    public void onReceive(final Context context, final Intent intent) {

        Log.d(TAG, "Network connectivity change");

            if (intent.getExtras() != null) {
                final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();

                if (ni != null && ni.isConnected()) {
                    Log.i(TAG, "Network " + ni.getTypeName() + " connected");
                    Toast.makeText(context, "Network " + ni.getTypeName() + " connected", Toast.LENGTH_LONG);
                }else{
                    if(ni != null && ni.isConnectedOrConnecting()){
                        Toast.makeText(context, "Network " + ni.getTypeName() + " connecting", Toast.LENGTH_LONG);
                        Log.i(TAG, "Network " + ni.getTypeName() + " connecting");

                    }
                    if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                        Toast.makeText(context, "no network connectivity", Toast.LENGTH_LONG);
                        Log.d(TAG, "There's no network connectivity");
                    }}
            }
    }
}