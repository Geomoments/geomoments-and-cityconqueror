package xjtlu.geomomentsdev.tools;

/**
 * Created by VRLAB_DevThree on 7/11/2016.
 */
public class ServerException extends Exception{
    public ServerException(String message){
        super(message);
    }
    public ServerException(){
        super();
    }
}
