package xjtlu.geomomentsdev.tools;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * A Class to manage interactions with SharedPreferences such storing and retrieving as logged in user id and language key.
 * Created by VRLAB_DevThree on 7/12/2016.
 */
public class PreferenceHandler {

    private static final String logTag = "PreferenceHandler";

    private static final String PREF_NAME = "GeoMoments";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String KEY_LOGGEDIN_USER = "loggedInUser";
    private static final int PRIVATE_MODE = 0;


    private static final String KEY_LANGUAGE = "languageKey";

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    private static PreferenceHandler instance = null;

    private Context context;
    /**
     * private constructor to implement PreferenceHandler as singleton.
     * @param context the context from which sharedPreferences will be accessed.
     */
    private PreferenceHandler(Context context){
        this.context = context;
        this.pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.editor = pref.edit();

    }

    /**
     * returns the singleton instance of the PreferenceHandler, instantiates if necessary.
     * @param context the context from which sharedPreferences will be accessed.
     * @return the singleton instance of PreferenceHandler
     */
    public static PreferenceHandler getInstance(Context context){
        if(instance == null){
            instantiate(context);
        }
        return(instance);
    }

    /**
     * initiates the singelton instance of PreferenceHandler with the given context.
     * @param context the context from which sharedPreferences will be accessed.
     */
    public static void instantiate(Context context){
        instance = new PreferenceHandler(context);
    }

    /**
     * gets logged in user id from sharedPreferences.
     * @return the logged in user id from sharedPreferences. -1 if not found.
     */
    public static long getLoggedInUser() {

        return instance.pref.getLong(KEY_LOGGEDIN_USER, -1);
    }


    /**
     * sets a login flag in sharedPreferences.
     * @param isLoggedIn the value of the login flag in sharedPreferences.
     */
    public static void setLogin(boolean isLoggedIn){
        instance.editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        instance.editor.commit();

    }

    /**
     * reads the login flag in sharedPreferences.
     * @return the login flag from sharedPreferences.
     */
    public static boolean isLoggedIn(){
        return instance.pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    /**
     * stores the logged in user id in sharedPreferences.
     * @param userId the logged in user id
     */
    public static void setLoggedInUser(Long userId){
        if(userId != null) {
            instance.editor.putLong(KEY_LOGGEDIN_USER, userId);
            instance.editor.commit();
        }
        else{
            instance.editor.remove(KEY_LOGGEDIN_USER);
        }
    }

    /**
     * stores the language key in sharedPreferences
     * @param language the language key to store in sharedPreferences
     */
    public static void setLanguage(String language){
        instance.editor.putString(KEY_LANGUAGE, language);
        instance.editor.commit();

    }
    /**
     * gets the language key from sharedPreferences
     */
    public static String getLanguage(){
        return instance.pref.getString(KEY_LANGUAGE, null);
    }



}
