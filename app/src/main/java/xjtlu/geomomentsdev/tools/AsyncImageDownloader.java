package xjtlu.geomomentsdev.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;
import java.net.ConnectException;
import java.util.HashMap;

import xjtlu.geomomentsdev.R;

/**
 * Created by tmp on 6/21/2016.
 */
public class AsyncImageDownloader extends AsyncTask<Object, Void, AsyncTaskResult<String>> {

    public static Long FULL_IMAGE = new Long(0);
    public static Long THUMB_IMAGE = new Long(1);

    private HashMap<String, String> params = new HashMap<String, String>();
    private String downloadImageResponse;
    private ImageView downloadImageView;
    private ProgressBar progressBar;
    private HashMap<Long, Bitmap> imageMap;
    private Long imageId = null;

    public AsyncImageDownloader(ProgressBar progressBar, HashMap<Long, Bitmap> imageMap){
        this.imageMap = imageMap;
        this.progressBar = progressBar;
    }
    public AsyncImageDownloader(ProgressBar progressBar){
        this.progressBar = progressBar;
    }
    public AsyncImageDownloader(){
        this.progressBar = null;
        this.imageMap = null;
    }




    @Override
    protected  void onPreExecute(){
        if(progressBar != null){
            progressBar.setVisibility(View.VISIBLE);
        }

    }

    /**
     * downloads image from server using DOWNLOAD_IMAGEU_URL php script
     * @param taskParams    taskParams[0] = (Long) id of image to download in server database,
     *                      taskParams[1] = (Long) FULL_IMAGE or THUMB_IMAGE to specify which to download,
     *                      taskParams[2] = (ImageView) imageView to set image in
     *
     * @return Bitmap of downloaded image
     */

    @Override
    protected AsyncTaskResult<String> doInBackground(Object... taskParams) {
        imageId = ((Long) taskParams[0]);
        params.put(HttpRequestHandler.DOWNLOAD_IMAGE_PARAMS[0], Long.toString((Long) taskParams[0]));
        if (((Long) taskParams[1]).equals(FULL_IMAGE)) {
            params.put(HttpRequestHandler.DOWNLOAD_IMAGE_PARAMS[1], "full");
        }
        if (((Long) taskParams[1]).equals(THUMB_IMAGE)) {
            params.put(HttpRequestHandler.DOWNLOAD_IMAGE_PARAMS[1], "thumb");
        }

        downloadImageView = (ImageView) taskParams[2];



            downloadImageResponse = null;

        AsyncTaskResult<String> result = new AsyncTaskResult<String>();
            try {
                downloadImageResponse = HttpRequestHandler.post(HttpRequestHandler.DOWNLOAD_IMAGE_URL, params).trim();
                result.putResult(downloadImageResponse.trim());

            } catch (IOException e) {
                result.addException(e);

            } catch (Exception e) {
                result.addException(e);
            }
        finally{
                return result;
            }

        }

        @Override
        protected void onPostExecute (final AsyncTaskResult<String> result) {

            if(result.getExceptions() != null){

                //TODO:: think over structure

            }else{


            if (result.getResult() != "" && result.getResult() != null ) {
                // new Task, decode image sample
                AsyncTask<Integer, Void, AsyncTaskResult<Bitmap>> setImageTask = new AsyncTask<Integer, Void, AsyncTaskResult<Bitmap>>() {


                    @Override
                    protected AsyncTaskResult<Bitmap> doInBackground(Integer... params) {
                        AsyncTaskResult<Bitmap> decodeResult = new AsyncTaskResult<Bitmap>();

                        if (result.getExceptions() != null) {
                            for (Exception e : result.getExceptions()) {
                                decodeResult.addException(e);
                            }
                            return decodeResult;
                        } else {
                            try{
                                Bitmap decodedImage = ImageHandle.decodeSampleImage(downloadImageResponse.trim(), params[0]);

                                decodeResult.putResult(decodedImage);
                            }catch (Exception e){
                                decodeResult.addException(e);
                            }

                            return decodeResult;
                        }
                    }


                    @Override
                    protected void onPostExecute(AsyncTaskResult<Bitmap> result) {


                        if (result.getExceptions() == null) {
                            if (imageMap != null) {
                                imageMap.remove(imageId);
                                imageMap.put(imageId, result.getResult());
                            }
                            downloadImageView.setImageBitmap(result.getResult());
                            int visibility = downloadImageView.getVisibility();
                            downloadImageView.setVisibility(View.GONE);
                            downloadImageView.setVisibility(visibility);

                            if (progressBar != null) {
                                progressBar.setVisibility(View.GONE);
                            } else {
                                Log.d("AsyncImageDownload", "progressBar is null");
                            }

                        } else {
                            for (Exception e : result.getExceptions()) {
                                e.printStackTrace();
                            }
                            Toast.makeText(downloadImageView.getContext(), downloadImageView.getContext().getString(R.string.error_something_went_wrong), Toast.LENGTH_LONG);

                        }
                    }
                }.execute(downloadImageView.getWidth(), downloadImageView.getHeight());
            }
            }


        }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        Log.d("imageDownloadSample", "height: " + height + " width: " + width);
        float h = new Float(height);
        float w = new Float(width);
        float ratio = h/w;
        Log.d("imageDownloadSample", "ratio: "+ ratio);



        int reqHeight = (int) (ratio * reqWidth);
        Log.d("imageDownloadSample", "ReqHight: " + reqHeight + " ReqWidth: " +reqWidth);
        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }



    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


}

