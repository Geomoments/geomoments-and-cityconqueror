package xjtlu.geomomentsdev.tools;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xjtlu.geomomentsdev.R;

/**
 * Created by VRLAB_DevThree on 7/10/2016.
 */
public class TestFragment extends Fragment {

    public TestFragment() {


    }

    public static TestFragment newInstance() {
        TestFragment fragment = new TestFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_profile, container, false);
    }


}
