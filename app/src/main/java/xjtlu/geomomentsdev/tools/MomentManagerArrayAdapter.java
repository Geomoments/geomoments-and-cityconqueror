package xjtlu.geomomentsdev.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.CameraPosition;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import xjtlu.geomomentsdev.FullImageActivity;
import xjtlu.geomomentsdev.MomentManagerActivity;
import xjtlu.geomomentsdev.R;
import xjtlu.geomomentsdev.ShowUserActivity;

/**
 * Created by VRLAB_DevThree on 6/28/2016.
 */
public class MomentManagerArrayAdapter extends ArrayAdapter {

    ViewGroup viewContainer;
    Long loggedInUserId;
    private ImageDownloadQuery imageDownloadQuery;
    private HashMap<View, Marker> markerMap;



    public MomentManagerArrayAdapter(Context context, ArrayList<Moment> moments) {
        super(context, R.layout.list_item_manage_moment, moments);
        this.loggedInUserId = null;
        this.viewContainer = null;
        this.imageDownloadQuery = new ImageDownloadQuery();
        this.markerMap = new HashMap<>();
    }

    public void showCreatorImage(Moment moment, ImageView imageView, ProgressBar progressBar) {



        ImageRequest imageRequest = new ImageRequest(moment.getCreator().getProfilePicture().getId(), imageView, progressBar, 56 );

        try {
            imageDownloadQuery.resolveImageRequest(imageRequest);
        } catch (Exception e) {
            //TODO: replace string with xml string value
            Toast.makeText(getContext(), "image could not be downloaded",  Toast.LENGTH_LONG).show();
        }



    }




    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {


        LayoutInflater inflater = LayoutInflater.from(getContext());

        final View customItem;
        if(convertView != null){ // if view is recycled


            // remover markers on recycled mapView
            if(markerMap.containsKey(convertView)){

                markerMap.get(convertView).destroy();
                markerMap.remove(convertView);

            }

            //reset recycled view
            ((Button) convertView.findViewById(R.id.button_manage_moment_item_show_location)).setText(getContext().getResources().getString(R.string.show_location));

            ImageView creatorProfilePicture = (ImageView) convertView.findViewById(R.id.image_view_user_preview_profile);

            creatorProfilePicture.setImageBitmap(ImageHandle.bitmapFromVectorAsset(getContext(), R.drawable.ic_face_grey_96dp, R.color.lightGrey));


            MapView itemMapView = (MapView) convertView.findViewById(R.id.map_view_manage_moment_item_map);
            itemMapView.setVisibility(View.GONE);

            TextView mapLabel = (TextView) convertView.findViewById(R.id.text_view_manage_moment_item_content_text);
            mapLabel.setVisibility(View.GONE);


            ImageView imageContent = (ImageView) convertView.findViewById(R.id.image_view_manage_moment_item_content_image);

            //try to recycle imageContent

            imageContent.setImageBitmap(ImageHandle.bitmapFromVectorAsset(getContext(), R.drawable.ic_photo_black_96dp, R.color.lightGrey));



            customItem = convertView;


        }else{//not recycled -> inflate new view

            customItem = inflater.inflate(R.layout.list_item_manage_moment, parent, false);
            customItem.setId(position);
        }




        final Moment itemMoment = (Moment) getItem(position);

        //set moment creator info
        if (itemMoment.getCreator() != null) {


            View creatorView = customItem.findViewById(R.id.user_preview_manage_moment_item_creator);

            // set creator profile picture if available
            if (itemMoment.getCreator().getProfilePicture() != null) {

                ImageView creatorProfilePicture = (ImageView) creatorView.findViewById(R.id.image_view_user_preview_profile);
                ProgressBar creatorProgressBar = (ProgressBar) creatorView.findViewById(R.id.progress_bar_user_preview_progress);

                showCreatorImage(itemMoment.getCreator().getProfilePicture().getId(), creatorProfilePicture, creatorProgressBar, 56);
            }

            //set creator name
            final TextView creatorName = (TextView) creatorView.findViewById(R.id.text_view_user_preview_name);
            creatorName.setText(Html.fromHtml(itemMoment.getCreator().getName() + " <small>(" + itemMoment.getCreator().getUniqueName() + ")</small>"));


            //set click user event
            //if has loggedInUser and viewContainer => created from BrowserFragment
            creatorView.setOnClickListener(new View.OnClickListener() {

                                               @Override
                                               public void onClick(View v) {

                                                   Intent showUser = new Intent(getContext(), ShowUserActivity.class);
                                                   showUser.putExtra(ShowUserActivity.INTENT_EXTRA_USER_ID, itemMoment.getCreator().getId());
                                                   getContext().startActivity(showUser);

                                               }
                                           }


            );
        }

            //set text content if available
            TextView textContent = (TextView) customItem.findViewById(R.id.text_view_manage_moment_item_content_text);
            if (itemMoment.getTextContent() != null) {

                textContent.setText(itemMoment.getTextContent());
                textContent.setVisibility(View.VISIBLE);
            } else {
                textContent.setVisibility(View.GONE);
            }

            //set image content if available
            final ProgressBar imageProgress = (ProgressBar) customItem.findViewById(R.id.progress_bar_manage_moment_item_image_progress);
            final ImageView imageView = (ImageView) customItem.findViewById(R.id.image_view_manage_moment_item_content_image);

            if (itemMoment.getImageContent() != null) {


                // set click image content event
                imageView.setOnClickListener(new View.OnClickListener() {

                                                 @Override
                                                 public void onClick(View v) {

                                                     if (itemMoment.getImageContent() != null) {
                                                         Intent fullImage = new Intent(getContext(), FullImageActivity.class);
                                                         fullImage.putExtra(FullImageActivity.INTENT_EXTRA_IMAGE_ID, itemMoment.getImageContent().getId());
                                                         getContext().startActivity(fullImage);
                                                     } else {
                                                         Toast.makeText(getContext(), getContext().getString(R.string.error_no_image_selcted), Toast.LENGTH_LONG);

                                                     }
                                                 }
                                             }

                );
                imageView.setVisibility(View.VISIBLE);

               // showCreatorImage( itemMoment.getImageContent().getId(), imageView, imageProgress, 650);

                itemMoment.getImageContent().showThisThumbImageInView(imageView, imageProgress);

            } else {
                imageProgress.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
            }


            //show timestamp and distance
            TextView timestampAndLocation = (TextView) customItem.findViewById(R.id.text_view_manage_moment_item_timestamp);

            float distance = LocationHandler.getMyDistanceTo(itemMoment.getLatitude(), itemMoment.getLongitude());
            String distanceString = "";

            //convert distance to km if large
            if(distance > 999){
                distanceString = String.format("%.1f", distance/1000) + "km away";
            }else{
                distanceString = String.format("%.0f", distance) + "m away";
            }

            timestampAndLocation.setText(itemMoment.getPrettyTimestamp() + ", " + distanceString );




            //set showLocationButton
            Button showLocationButton = (Button) customItem.findViewById(R.id.button_manage_moment_item_show_location);


            showLocationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    MapView itemMapView = (MapView) ((View) v.getParent()).findViewById(R.id.map_view_manage_moment_item_map);
                    if (itemMapView.getVisibility() == View.GONE) {
                        //show map and change button to hide location map is hidden
                        itemMapView.onCreate(null);

                        itemMapView.setVisibility(View.VISIBLE);

                        TextView mapLabel = (TextView) ((View) v.getParent()).findViewById(R.id.text_view_manage_moment_item_content_location_label);
                        mapLabel.setVisibility(View.VISIBLE);

                        AMap itemAMap = itemMapView.getMap();

                        itemAMap.getUiSettings().setMyLocationButtonEnabled(false);
                        itemAMap.setMyLocationEnabled(false);
                        itemAMap.setTrafficEnabled(false);
                        itemAMap.getUiSettings().setAllGesturesEnabled(false);
                        itemAMap.getUiSettings().setZoomControlsEnabled(false);


                        itemAMap.moveCamera(CameraUpdateFactory.zoomTo(18));


                        createMarkerAtLocation(getContext(), itemMoment.getLocation(), itemAMap);
                        ((Button) v).setText(getContext().getResources().getString(R.string.hide_location));

                    } else {
                        //hide map and change button to show location if map is already showing

                        itemMapView.setVisibility(View.GONE);
                        TextView mapLabel = (TextView) ((View) v.getParent()).findViewById(R.id.text_view_manage_moment_item_content_location_label);
                        mapLabel.setVisibility(View.GONE);
                        ((Button) v).setText(getContext().getResources().getString(R.string.show_location));


                    }
                }


                /**
                 * creates a marker at the given location
                 * @param context
                 * @param latLng the location where the marker is to be created
                 * @param aMap the map on wich the marker is to be created
                 */
                private void createMarkerAtLocation(Context context, LatLng latLng, AMap aMap) {

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageHandle.bitmapFromVectorAsset(context, R.drawable.ic_place_white_24dp, R.color.black)));
                    aMap.addMarker(markerOptions);
                    aMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(latLng, aMap.getCameraPosition().zoom)));

                }

            });


        //delete Moment button

        ImageButton deleteMoment = (ImageButton) customItem.findViewById(R.id.button_manage_moment_item_delete);
        deleteMoment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setMessage(R.string.dialog_delete_moment)
                        .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                AsyncTask<Long, Void, AsyncTaskResult<Boolean>> deleteMomentTask = new AsyncTask<Long, Void, AsyncTaskResult<Boolean>>() {
                                    @Override
                                    protected AsyncTaskResult<Boolean> doInBackground(Long... params) {
                                        AsyncTaskResult<Boolean> result = new AsyncTaskResult<Boolean>();
                                        try {


                                            boolean deleteResult = Moment.deleteMoment(params[0]);
                                            result.putResult(deleteResult);
                                        } catch (Exception e) {
                                            result.addException(e);
                                        } finally {
                                            return result;
                                        }
                                    }

                                    @Override
                                    protected void onPostExecute(AsyncTaskResult<Boolean> result) {
                                        if (result.getExceptions() != null) {
                                            for (Exception e : result.getExceptions()) {
                                                handleException(e);
                                            }
                                        } else {
                                            ((MomentManagerActivity) getContext()).momentDeleted(position);
                                        }
                                    }
                                }.execute(((Moment) getItem(position)).getId());
                            }})
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            }
        });




        return customItem;
    }

    /**
     * function to show a an image in a given imagView, using the imageDownloadCache
     * @param imageId the id of the image to show
     * @param imageView the image view to show the image in
     * @param progressBar the progress bar to show the progress of downloading the image if necessary
     * @param reqWidth the required width of the image.
     */
    public void showCreatorImage(final Long imageId, final ImageView imageView, final ProgressBar progressBar, final int reqWidth) {
        ImageRequest imageRequest = new ImageRequest(imageId, imageView, progressBar, reqWidth );


        try{
                    imageDownloadQuery.resolveImageRequest(imageRequest);
                } catch (Exception e) {
                    //TODO: replace string with xml string value
                    Toast.makeText(getContext(), "image could not be downloaded",  Toast.LENGTH_LONG).show();
                }






}

    //============================================================================================//
    //--- Methods used for Exception handling --------------------------------------------------------//
    //============================================================================================//

    /**
     * Method implementing the standard war of reacting to an Exception
     *
     * @param e the Exception to react to
     */
    //TODO: extend for other Exceptions if necessary
    public void handleException(Exception e) {

        if (e instanceof ServerException) {
            handleException((ServerException) e);
        } else if (e instanceof IOException) {
            handleException((IOException) e);
        } else if (e instanceof  NoConnectionException){
            handleException((NoConnectionException) e);
        }
        else {
            Toast.makeText(getContext(), getContext().getString(R.string.error_something_went_wrong), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    /**
     * Method implementing the standard way of reacting to a ServerException.
     *
     * @param e the ServerException to react to.
     */
    //TODO: replace with splash screen excepiton
    public void handleException(ServerException e) {
        e.printStackTrace();
        Toast.makeText(getContext(), "Server Exception occured", Toast.LENGTH_LONG).show();
    }


    /**
     * Method implementing the standard way of reacting to anm IOException.
     *
     * @param e the IOException to react to.
     */
    //TODO: replace with splash screen excepiton, split into network error etc.
    public void handleException(IOException e) {
        e.printStackTrace();
        Toast.makeText(getContext(), "IOException Exception occured", Toast.LENGTH_LONG).show();
    }
    public void handleException(NoConnectionException e){
        e.printStackTrace();
        InternetConnectionHandler.waitForConnection(getContext(), (Activity) getContext());
    }




}
