package xjtlu.geomomentsdev.tools;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

/**
 * A class extending an AsyncTask to download moments from the server
 * Created by VRLAB_DevThree on 6/26/2016.
 */
public class AsyncMomentsDownloader extends AsyncTask<Void, Void, AsyncTaskResult<ArrayList<Moment>>> {


    public AsyncResponse delegate = null;
    private ProgressBar progressBar;
    private String wheresStmt;
    private String orderByStmt;
    private HashMap<Long, User> downloadedUsers = new HashMap<Long, User>();
    private AsyncTaskResult<String> downloadedContent;


    /**
     * constructor
     *
     * @param delegate     the AsyncResponse interface to override the onPostExecute Method of the AsyncTask.
     * @param wheresStmt   the sql where statement used to filter the downloaded moments.
     * @param orderByStmt  the sql order by statement used to order the downlaoded moments
     * @param loggedInUser the currently logged in user.
     */
    public AsyncMomentsDownloader(AsyncResponse delegate, String wheresStmt, String orderByStmt, User loggedInUser) {
        this.delegate = delegate;
        this.wheresStmt = wheresStmt;
        this.orderByStmt = orderByStmt;
        downloadedUsers = new HashMap<Long, User>();
        downloadedUsers.put(loggedInUser.getId(), loggedInUser);
    }

    /**
     * constructor
     *
     * @param delegate     the AsyncResponse interface to override the onPostExecute Method of the AsyncTask.
     * @param wheresStmt   the sql where statement used to filter the downloaded moments.
     * @param orderByStmt  the sql order by statement used to order the doownlaoded moments
     * @param loggedInUser the currently logged in user.
     * @param progressBar  the progressBar to indicate the progress of the download process.
     */
    public AsyncMomentsDownloader(AsyncResponse delegate, String wheresStmt, String orderByStmt, User loggedInUser, ProgressBar progressBar) {
        this.delegate = delegate;
        this.progressBar = progressBar;
        this.wheresStmt = wheresStmt;
        downloadedUsers = new HashMap<Long, User>();
        // add the logged in user to the set of already downloaded users to not download this already existing information again when parsing users.
        downloadedUsers.put(loggedInUser.getId(), loggedInUser);
        this.orderByStmt = orderByStmt;
    }

    @Override
    protected void onPreExecute() {
        // show progressBar if available before starting download
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
        this.downloadedContent = new AsyncTaskResult<String>();

        AsyncTask<Void, Void, AsyncTaskResult<String>> downloadContent = new AsyncTask<Void, Void, AsyncTaskResult<String>>() {
            @Override
            protected AsyncTaskResult<String> doInBackground(Void... voidParams) {
                    //download moment data
                    ArrayList<Moment> downloadedMoments = new ArrayList<Moment>();

                    // set params
                    HashMap<String, String> params = new HashMap<String, String>();
                    if (wheresStmt != null) {
                        params.put(HttpRequestHandler.DOWNLOAD_MOMENTS_PARAMS[0], wheresStmt);

                    }
                    if (orderByStmt != null) {
                        params.put(HttpRequestHandler.DOWNLOAD_MOMENTS_PARAMS[1], orderByStmt);
                    }

                    AsyncTaskResult<String> result = new AsyncTaskResult<String>();
                    String downloadMomentResponse = null;

                    try {

                        downloadMomentResponse = HttpRequestHandler.post(HttpRequestHandler.DOWNLOAD_MOMENTS_URL, params);
                        Log.d("MomentDownloader", "downloadMomentResponse = " + downloadMomentResponse);

                        result.putResult(downloadMomentResponse);


                    } catch (Exception e) {
                        result.addException(e);
                    } finally {
                        return result;
                    }
            }
            }.execute();

        try {
            AsyncTaskResult<String> contentDownloadResult = downloadContent.get();
            if(contentDownloadResult.getExceptions() != null){
                for(Exception e: contentDownloadResult.getExceptions()) {


                    this.downloadedContent.addException(e);
                }
            }else{
                this.downloadedContent.putResult(contentDownloadResult.getResult());
            }

        } catch (InterruptedException e) {
            this.downloadedContent.addException(e);
        } catch (ExecutionException e) {
            this.downloadedContent.addException(e);
        }

    }

    /**
     * parses a JSONObject representing a Moment.
     *
     * @param jObj the JSONObject representing a Moment to be parsed to a Moment.
     * @return the Moment parsed from the given JSONObject.
     * @throws JSONException
     * @throws IOException
     * @throws ServerException
     */
    private Moment parseMoment(JSONObject jObj) throws Exception  {

        // get moment image content if available
        Object imageNullCheck = jObj.get(DatabaseHandler.MOMENTS_IMAGE);

        ImageHandle momentImageContent;
        if (imageNullCheck != JSONObject.NULL) {
            Long imageId = jObj.getLong(DatabaseHandler.MOMENTS_IMAGE);

            momentImageContent = new ImageHandle(imageId);
        } else {
            momentImageContent = null;

        }

        // get moment text content if available.
        Object textNullCheck = jObj.get(DatabaseHandler.MOMENTS_TEXT);

        String momentTextContent;
        if (textNullCheck != JSONObject.NULL) {
            momentTextContent = jObj.getString(DatabaseHandler.MOMENTS_TEXT);
        } else {
            momentTextContent = null;
        }

        // parse the creating user.
        Long creatorId = jObj.getLong(DatabaseHandler.MOMENTS_CREATOR);
        User momentCreator = null;

        momentCreator = parseUser(creatorId);

        //format timestamp to date.
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp timestamp = null;

        timestamp = new Timestamp(sdf.parse(jObj.getString(DatabaseHandler.MOMENTS_TIMESTAMP)).getTime());


        return new Moment(jObj.getLong(DatabaseHandler.MOMENTS_ID), jObj.getDouble(DatabaseHandler.MOMENTS_LAT), jObj.getDouble(DatabaseHandler.MOMENTS_LNG), momentTextContent, momentImageContent, momentCreator, timestamp);


    }

    /**
     * downloads the data User for the given user id or returns the user data if already downloaded
     *
     * @param userId the id of the User to be downloaded
     * @return the User that was downloaded and parsed for the given userId
     * @throws IOException
     * @throws ServerException
     */
    private User parseUser(Long userId) throws Exception  {
        //if user already parsed and downloaded, return existing user, else download user
        if (downloadedUsers.containsKey(userId)) {
            return (downloadedUsers.get(userId));
        } else {
            User user = User.downloadUser(userId);
            Log.d("parseUser", "downloadedUser: " + user);
            downloadedUsers.put(userId, user);
            return (user);
        }

    }


    @Override
    protected AsyncTaskResult<ArrayList<Moment>> doInBackground(Void... voidParams) {


        AsyncTaskResult<ArrayList<Moment>> result = new AsyncTaskResult<ArrayList<Moment>>();
        ArrayList<Moment> parsedMoments = new ArrayList<Moment>();

        if(this.downloadedContent.getExceptions() != null ) {
            for (Exception e : this.downloadedContent.getExceptions()) {
                result.addException(e);

            }
        }else {

            Log.d("MomentDownloader", "downloadedContentResult = " + downloadedContent.getResult());
            JSONArray jArr = null;
            try {
                jArr = new JSONArray(this.downloadedContent.getResult());

                for (int i = 0; i < jArr.length(); i++) {
                    parsedMoments.add(parseMoment(jArr.getJSONObject(i)));

                }
                result.putResult(parsedMoments);

            } catch (Exception e) {
                result.addException(e);
            }

        }
        return result;
    }

    @Override
    protected void onPostExecute(AsyncTaskResult<ArrayList<Moment>> downloadMomentsResult) {
        downloadedContent = null;
        downloadedUsers = null;
        //hide progressBar
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            Log.d("momentDownloader", "progressBar hidden");
        }
        delegate.processFinish(downloadMomentsResult);
    }


    /**
     * an interface to override the onPostExecute Method of the AsyncTask
     */
    public interface AsyncResponse {
        void processFinish(AsyncTaskResult<ArrayList<Moment>> downloadedMoments);
    }




}
