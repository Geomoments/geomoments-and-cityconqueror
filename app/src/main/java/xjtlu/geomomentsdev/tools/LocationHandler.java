package xjtlu.geomomentsdev.tools;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps2d.LocationSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

/**
 * A class implementing a LocationSource and an AMapLicationListener, used for location dependent services throughout the app.
 * Created by VRLAB_DevThree on 7/5/2016.
 */
public class LocationHandler implements AMapLocationListener, LocationSource {


    public static final int RANGE_IN_METERS = 200;
    public static final long TIME_BETWEEN_UPDATES_IN_MINUTES = 2;

    private static LocationHandler instance = new LocationHandler();
    private OnLocationChangedListener mListener;
    private AMapLocationClient mlocationClient;
    private AMapLocationClientOption mLocationOption;
    private Double myLat = null;
    private Double myLng = null;
    private Context context;

    private static HashSet<LocationDependentActivity> asapUpdate = new HashSet<>();


    private Date lastUpdated = null;

    private static HashSet<LocationDependentActivity> deprecatedActivities = new HashSet<>();

    /**
     * the set of registered locationDependentActivities
     */
    private static HashSet<LocationDependentActivity> locationDependentActivities = new HashSet<LocationDependentActivity>();

    /**
     * the List of Moments nearby as downloaded on location update
     */
    private ArrayList<Moment> momentsNearby = null;

    /**
     * the sql where statement used in the download of momentsNearby
     */
    private String whereStmt;
    /**
     * the order by statement used in the download of momentsNearby
     */
    private String orderByStmt;


    private LocationHandler() {
        myLat = null;
        myLng = null;


    }

    /**
     * instantiates the singleton with the given context
     *
     * @param context the context to create the AMapLocationClient with
     */

    public static void instantiate(Context context) {
        instance.context = context;
    }

    /**
     * returns the singleton instance of LocationHandler
     *
     * @return
     */
    public static LocationHandler getInstance() {
        return instance;
    }


    public static void setLocation(Location loc) {
        instance.myLat = loc.getLatitude();
        instance.myLng = loc.getLongitude();
    }




    /**
     * registers a locationDependentActivity, to call the locationDependentRefresh method of the locationDependentActivity when the Location is updated.
     * Updates the locationDependentActivity as soon as possible even if there is no significant change in location.
     *
     * @param locationDependentActivity the LocationDependentActivity to register
     */

    public static void registerLocationDependentActivity(LocationDependentActivity locationDependentActivity) {

        //add locationDependentActivity to the set of locationDependentActivities
         locationDependentActivities.add(locationDependentActivity);
        deprecatedActivities.add(locationDependentActivity);

    }

    /**
     * checks if the given LocationDependentActivity is deprecated.
     * @param lda the LocationDependentActivity to check for deprecation.
     * @return true, if the LocationDependentActivity is deprecated, false if not.
     */
    public static boolean isDeprecated(LocationDependentActivity lda){
        return deprecatedActivities.contains(lda);
    }

    public static void setNotDeprecated(LocationDependentActivity lda){
        deprecatedActivities.remove(lda);
    }

    /**
     * drops a locationDependentActivity so that it is not updated on location changes anymore.
     *
     * @param locationDependentActivity the LocationDependentActivity to drop.
     */
    public static void dropLocationDependentActivity(LocationDependentActivity locationDependentActivity) {

        locationDependentActivities.remove(locationDependentActivity);
        deprecatedActivities.remove(locationDependentActivity);
    }


    public static void refreshMoments() throws Exception {
        instance.getMomentsNearby(null, true);
    }


    public static void updateMeAsap(LocationDependentActivity locationDependentActivity){
        asapUpdate.add(locationDependentActivity);

    }

    /**
     * a function to get the moments nearby if already downloaded, or download the moments nearby based on the current location
     *
     * @param requestingLda the LocationDependentActivity that is requesting the MomentsNearby, used to mark all other registered activities as deprecated, null if all registered activities shall be marked as deprecated.
     * @param forceRefresh true to force the moments nearby to be downloaded based on the current location, if false the existing(previously downloaded) moments nearby are returned
     * @return the List of moments nearby based on the current location.
     * @throws Exception
     */
    public static ArrayList<Moment> getMomentsNearby(LocationDependentActivity requestingLda, boolean forceRefresh) throws Exception {
        //if moments are not yet downloaded, or foreceRefresh is required download moments nearby
        if (instance.momentsNearby == null || forceRefresh) {
            //filter by range according to current location
            instance.whereStmt = HttpRequestHandler.getMomentRangeFilter(instance.myLat, instance.myLng);
            //order by timestamp
            instance.orderByStmt = " " + Moment.MOMENTS_TIMESTAMP_COLUMN + " DESC";


            AsyncMomentsDownloader momentsDownloader = new AsyncMomentsDownloader(new AsyncMomentsDownloader.AsyncResponse() {
                @Override
                public void processFinish(AsyncTaskResult<ArrayList<Moment>> downloadedMoments) {
                     Log.d("getMomentsNearby", "processFinish called");
                    if(downloadedMoments.getResult() != null) {
                        Log.d("getMomentsNearby", "result length: " + downloadedMoments.getResult().size());
                    }
                    if(downloadedMoments.getExceptions() != null) {
                        Log.d("getMomentsNearby", "exceptions length: " + downloadedMoments.getExceptions().size());
                    }


                }

            }, instance.whereStmt, instance.orderByStmt, User.getLoggedInUser());
            AsyncTaskResult<ArrayList<Moment>> result = momentsDownloader.execute().get();
            if(result.getExceptions() != null){
                //throw one of the occurred exceptions
                for(Exception e: result.getExceptions()){
                    throw(e);
                }
            }else {
                instance.momentsNearby = result.getResult();
                instance.lastUpdated = new Date();

                // note all other activities as deprecated
                deprecatedActivities.clear();
                for(LocationDependentActivity lda: locationDependentActivities ){
                    if(!lda.equals(requestingLda)){
                        deprecatedActivities.add(lda);
                    }
                }


            }
        }


        //remove requesting lda from deprecatedActivities
        deprecatedActivities.remove(requestingLda);

        //return nearbyMoments if updated or old version
        return instance.momentsNearby;
    }

    /**
     * reacts to location changed by downloading a new set of moments nearby and calling the locationDpendantUpdate of registered LocationDependentActivities
     *
     * @param aMapLocation the new location
     */
    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {

        if (mListener != null && aMapLocation != null) {
            if ( aMapLocation.getErrorCode() == 0) {

                //get movement movement from previous location in meters

                Float movementInMeters = null;
                if (instance.myLat != null && instance.myLng != null) {
                    movementInMeters = getMyDistanceTo(aMapLocation.getLatitude(), aMapLocation.getLongitude());
                }

                boolean timedUpdateRequired = false;
                if(lastUpdated  == null){
                    timedUpdateRequired = true;
                }else{
                    Date now = new Date();
                    long diff = now.getTime() - lastUpdated.getTime();

                    if((diff / (60 * 1000) % 60) >= TIME_BETWEEN_UPDATES_IN_MINUTES){
                        timedUpdateRequired = true;
                    }

                }

                //set my location to new location latitude and longitude
                instance.myLat = aMapLocation.getLatitude();
                instance.myLng = aMapLocation.getLongitude();

                instance.mListener.onLocationChanged(aMapLocation);

                //if my location changed by at least 10 meters, download new nearby moments
                if (movementInMeters == null || (movementInMeters !=null && movementInMeters > 10 || timedUpdateRequired)) {
                    try {
                        getMomentsNearby(null, true);
                    } catch (Exception e) { // if exception occurs, let locationDependentActivities handle it anmd cacel refresh
                        for(LocationDependentActivity lda: locationDependentActivities){
                            lda.manageMomentsDownloadException(e);
                        }
                        //cancel refresh
                        return;
                    }

                    //refresh all location dependent activities
                    for (LocationDependentActivity lda : locationDependentActivities) {
                        lda.locationDependentRefresh();

                    }
                }
            }

            //perform asap updates
            for(LocationDependentActivity lda: asapUpdate){

                lda.locationDependentRefresh();
                asapUpdate.remove(lda);

            }
        }
    }

    public static Double getMyLat() {

        return instance.myLat;

    }


    public static Double getMyLng() {
        return (instance.myLng);
    }

    /**
     * returns the distance from the current location to the given location in meters.
     * @param lat the latitude of the location to calculate the distance to.
     * @param lng the longitude of the location to calculate the distance to.
     * @return the distance between my location and the given location in meters
     */
    public static float getMyDistanceTo(double lat, double lng) {

        Location myLocation = new Location("myLocaton");
        myLocation.setLatitude(instance.myLat);
        myLocation.setLongitude(instance.myLng);

        Location targetLocation = new Location("target");
        targetLocation.setLatitude(lat);
        targetLocation.setLongitude(lng);

        return myLocation.distanceTo(targetLocation);
    }

    public static void setContext(Context context) {
        instance.context = context;
    }



    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        instance.mListener = onLocationChangedListener;
        if (instance.mlocationClient == null) {
            instance.mlocationClient = new AMapLocationClient(context);
            instance.mLocationOption = new AMapLocationClientOption();
            instance.mlocationClient.setLocationListener(this);
            instance.mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);

            instance.mlocationClient.setLocationOption(instance.mLocationOption);

            instance.mlocationClient.startLocation();
        }

    }

    @Override
    public void deactivate() {
        instance.mListener = null;
        if (instance.mlocationClient != null) {
            instance.mlocationClient.stopLocation();
            instance.mlocationClient.onDestroy();
        }
        instance.mlocationClient = null;

    }

    //============================================================================================//
    //--- Methods used for Exception handling --------------------------------------------------------//
    //============================================================================================//

    /**
     * Method implementing the standard war of reacting to an Exception
     *
     * @param e the Exception to react to
     */
    //TODO: extend for other Exceptions if necessary
    public static void handleException(Exception e) {

        if (e instanceof ServerException) {
            handleException((ServerException) e);
        } else if (e instanceof IOException) {
            handleException((IOException) e);
        }

        e.printStackTrace();
    }


    /**
     * Method implementing the standard way of reacting to a ServerException.
     *
     * @param e the ServerException to react to.
     */
    //TODO: replace with splash screen excepiton
    public static void handleException(ServerException e) {
        e.printStackTrace();
        Toast.makeText(instance.context, "Server Exception occured", Toast.LENGTH_LONG);
    }


    /**
     * Method implementing the standard way of reacting to anm IOException.
     *
     * @param e the IOException to react to.
     */
    //TODO: replace with splash screen excepiton, split into network error etc.
    public static void handleException(IOException e) {
        e.printStackTrace();
        Toast.makeText(instance.context, "IOException Exception occured", Toast.LENGTH_LONG);
    }

}
