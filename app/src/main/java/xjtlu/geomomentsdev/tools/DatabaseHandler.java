package xjtlu.geomomentsdev.tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 *
 */
public class DatabaseHandler extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "geoMomentsDB";

    // Users table name
    private static final String TABLE_USERS = "users";

    // Users table column names
    public static final String USERS_ID = "id";
    public static final String USERS_UNIQUE_NAME = "unique_name";
    public static final String USERS_NAME = "name";
    public static final String USERS_EMAIL = "email";
    public static final String USERS_STATUS = "status";
    public static final String USERS_PROFILE_PICTURE = "profile_picture";

    // Images table name
    private static final String TABLE_IMAGES = "images";

    // Images table column names
    public static final String IMAGES_ID = "id";
    public static final String IMAGES_PATH = "full_path";
    public static final String IMAGES_THUMBNAIL = "thumb_path";

    //Moment table name
    private static final String TABLE_MOMENTS = "geomoments";

    //Moment table column names
    public static final String MOMENTS_ID = "id";
    public static final String MOMENTS_CREATOR = "creator";
    public static final String MOMENTS_IMAGE = "image";
    public static final String MOMENTS_TEXT = "text";
    public static final String MOMENTS_TIMESTAMP = "timestamp";
    public static final String MOMENTS_LAT = "lat";
    public static final String MOMENTS_LNG = "lng";

    private static DatabaseHandler instance;



    public static synchronized DatabaseHandler getInstance(Context context){

        Log.d("Database", "getInstance");
        if(instance == null){
            instance = new DatabaseHandler(context.getApplicationContext());
        }
        return instance;
    }

    /**
     * constructor
     * @param context
     */
    private DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    /**
     * converts a User object to a representation in ContentValues for use in database methods.
     * @param user the User to convert to ContentValues.
     * @return a ContentValues representation of the user.
     */
    private static ContentValues userToContentValues(User user){
        ContentValues values = new ContentValues();
        values.put(USERS_ID, user.getId());
        values.put(USERS_UNIQUE_NAME, user.getUniqueName());
        values.put(USERS_NAME, user.getName());
        values.put(USERS_EMAIL, user.getEmail());
        values.put(USERS_STATUS, user.getStatus());
        if(user.getProfilePicture() != null) {
            values.put(USERS_PROFILE_PICTURE, user.getProfilePicture().getId());
      //  }else{
        //    values.put(USERS_PROFILE_PICTURE, null);
        }

        return values;
    }

    /**
     * deletes the given user form the database
     * @param user the user to delete from the database
     */
    public void deleteUser(User user){
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.delete(TABLE_USERS, USERS_ID + " = ? ", new String[]{Long.toString(user.getId())});
        }
        catch(Exception e){
            e.printStackTrace();
        }finally{
            db.close();
        }

    }

    /**
     * inserts the given user into the database or updates an existing user in the database with the same id to represent the given user data.
     * @param user the user to insert or update.
     */
    public void insertOrUpdateUser(User user){
        insertOrUpdateUser(userToContentValues(user));
    }

    /**
     * inserts the given user into the database or updates an existing user in the database with the same id to represent the given user data.
     * @param user a ContentValues representation of the user to insert or update.
     */
    private void insertOrUpdateUser(ContentValues user){
        Log.d("Database", "insertOrUpdateUser");
        //try to insert
        SQLiteDatabase db = getWritableDatabase();

        long insertId = db.insertWithOnConflict(TABLE_USERS, null, user, SQLiteDatabase.CONFLICT_IGNORE);
        //if insert returns -1: insert not successful because user already exists -> update existing user
        if(insertId == -1){

            Log.d("Database", "User exists..updating instead of insert..");
            updateUser(user);
        }else{

            Log.d("Database", "New user inserted into sqlite: " + insertId);
        }
        db.close();

    }

    /**
     * updates the user with same id as the given user in the database.
     * @param user a ContentValues representation of the user to insert or update.
     */
    private void updateUser(ContentValues user){
        SQLiteDatabase db = getWritableDatabase();

        long updateCount = db.update(TABLE_USERS, user, USERS_ID + "=" + user.get(USERS_ID), null);
        db.close();

    }


    /**
     * gets the user with the given id from the database.
     * @param userId the id of the user to get from the database.
     * @return the user from the database.
     */
    public User getUser(long userId){

        SQLiteDatabase db = getReadableDatabase();


        String[] cols = {USERS_ID, USERS_UNIQUE_NAME, USERS_NAME, USERS_EMAIL, USERS_STATUS, USERS_PROFILE_PICTURE};
        Cursor cursor = db.query(TABLE_USERS, cols, " "+USERS_ID +" = ?", new String[] {Long.toString(userId)}, null, null, null, null);

        if(cursor != null){
            cursor.moveToFirst();
        }

        //get image handle if image id available
        ImageHandle profileImage;

        if( cursor.getString(5) != null && (! cursor.getString(5).equals("null"))){
             profileImage = new ImageHandle(cursor.getLong(5));
        }else{
            profileImage = null;
        }

        User user = new User(Long.parseLong(cursor.getString(0)),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                profileImage);



        cursor.close();
        db.close();
        return user;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // create tables
        String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "( "
                + USERS_ID + " BIGINT PRIMARY KEY, "
                + USERS_UNIQUE_NAME + " TEXT, "
                + USERS_NAME + " TEXT, "
                + USERS_EMAIL + " TEXT, "
                + USERS_STATUS + " TEXT, "
                + USERS_PROFILE_PICTURE + " BIGINT "

                + ")";

        db.execSQL(CREATE_USERS_TABLE);
     //   db.close();
        Log.d("DatabaseHandler", "imagesTable created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //drop existing tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        //create new tables
        onCreate(db);

    }

}
