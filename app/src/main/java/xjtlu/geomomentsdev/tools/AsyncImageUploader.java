package xjtlu.geomomentsdev.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.util.HashMap;

/**
 * A class extending an AsyncTask to upload images to the server
 * Created by tmp on 6/24/2016.
 */
public class AsyncImageUploader extends AsyncTask<String, Void, AsyncTaskResult<Long>> {



    /**
     * an interface to override the onPostExecute Method of the AsyncTask
     */
    public interface AsyncResponse{
        void processFinish(AsyncTaskResult<Long> uploadedImageId);
    }

    public AsyncResponse delegate = null;
    public AsyncImageUploader(AsyncResponse delegate){
        this.delegate = delegate;
    }


    @Override
    protected AsyncTaskResult<Long> doInBackground(String... strings)  {
       //String imgPath = strings[0];
        //reduce to full hd resolution
        String imgPath =  ImageHandle.decodeFile(strings[0], 1920, 1080);

        String fileNameSegments[] = imgPath.split("/");
        String fileName = fileNameSegments[fileNameSegments.length -1];



        HashMap<String, String> params = new HashMap<String, String>();
        params.put(HttpRequestHandler.UPLOAD_IMAGE_PARAMS[0], fileName);
        String encodedImageString = ImageHandle.encodeImageToString(imgPath);
        Log.d("ImageUploader", "encodedImage: " + encodedImageString);

        params.put(HttpRequestHandler.UPLOAD_IMAGE_PARAMS[1], encodedImageString);




        String uploadImageResponse = "";
        AsyncTaskResult<Long> result = new AsyncTaskResult<Long>();
        try {
            uploadImageResponse = HttpRequestHandler.post(HttpRequestHandler.UPLOAD_IMAGE_URL, params);

            Log.d("imageUploader", "response = " + uploadImageResponse);
            Long uploadedImageId = Long.parseLong(uploadImageResponse.trim());
            result.putResult(uploadedImageId);

        } catch (Exception e) {
            result.addException(e);
        }
        finally {
            return result;
        }

    }

    @Override
    protected void onPostExecute(AsyncTaskResult<Long> result){
        delegate.processFinish(result);
    }


}
