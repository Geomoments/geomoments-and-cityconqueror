package xjtlu.geomomentsdev.tools;

/**
 * Created by VRLAB_DevThree on 7/6/2016.
 */
public interface LocationDependentActivity {

    public void locationDependentRefresh();

    public void manageMomentsDownloadException(Exception e);

}
