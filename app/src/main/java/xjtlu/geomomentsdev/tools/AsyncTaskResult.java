package xjtlu.geomomentsdev.tools;

import java.util.HashSet;

/**
 * adapted from: http://stackoverflow.com/questions/1739515/asynctask-and-error-handling-on-android
 */
public class AsyncTaskResult<T> {
    private T result;
    private HashSet<Exception> exceptions = null;

    public T getResult() {
        return result;
    }

    public HashSet<Exception> getExceptions() {
        return exceptions;
    }


    public AsyncTaskResult(){
        super();

    }

    public void putResult(T result){
        this.result = result;
    }

    public void addException(Exception e){
        if(exceptions == null){
            exceptions = new HashSet<Exception>();
        }
        exceptions.add(e);
    }
}