package xjtlu.geomomentsdev.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONObject;

import java.io.IOException;
import java.net.ConnectException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 * A class extending an AsyncTask to send updated user data to the server
 * Created by VRLAB_DevThree on 6/29/2016.
 */
public class AsyncUserUpdater  extends AsyncTask<User, Void, AsyncTaskResult<String>> {


    /**
     * an interface to override the onPostExecute Method of the AsyncTask
     */
    public interface AsyncResponse {
        void processFinish(AsyncTaskResult<String> result);
    }

    public AsyncResponse delegate = null;


    public AsyncUserUpdater(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected AsyncTaskResult<String> doInBackground(User... userParams) {

        //set post params
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(HttpRequestHandler.UPDATE_USER_PARAMS[0], Long.toString(userParams[0].getId()));
        params.put(HttpRequestHandler.UPDATE_USER_PARAMS[1], userParams[0].getUniqueName());
        params.put(HttpRequestHandler.UPDATE_USER_PARAMS[2], userParams[0].getName());

        //set profile picture param if a profile picture is available
        if (userParams[0].getProfilePicture() != null) {
            params.put(HttpRequestHandler.UPDATE_USER_PARAMS[3], Long.toString(userParams[0].getProfilePicture().getId()));
        }
        params.put(HttpRequestHandler.UPDATE_USER_PARAMS[4], userParams[0].getStatus());
        params.put(HttpRequestHandler.UPDATE_USER_PARAMS[5], userParams[0].getEmail());

        String updateUserResponse = null;
        AsyncTaskResult<String> response = new AsyncTaskResult<String>();

        try {
            updateUserResponse = HttpRequestHandler.post(HttpRequestHandler.UPDATE_USER_URL, params);

            //user AsyncResponse to return errors or result respecticvely
            response.putResult(updateUserResponse);
        } catch (Exception e) {
            response.addException(e);
        }finally{
            return response;
        }

    }


    @Override
    protected void onPostExecute(AsyncTaskResult<String> result) {
        // call the interface method to allow actions form the creating thread in onPostExecute.
        delegate.processFinish(  result);
    }
}
