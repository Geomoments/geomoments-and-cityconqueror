package xjtlu.geomomentsdev.tools;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;

import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ExecutionException;

/**
 * A class implementing a downloaded images cache
 * Created by VRLAB_DevThree on 7/8/2016.
 */
public class ImageDownloadQuery {

    LruCache<Long, Bitmap> downloadedImages;
    HashMap<Long, HashSet<ImageRequest>> imageRequests;
    public ImageDownloadQuery() {
        downloadedImages = new LruCache<Long, Bitmap>(20);
        imageRequests = new HashMap<Long, HashSet<ImageRequest>>();
    }

    /**
     * regsters an image request to be resolved by this ImageDownloadQuery.
     * @param imageRequest the image request to register.
     */
    private void registerImageRequest(ImageRequest imageRequest){
        if(imageRequests.containsKey(imageRequest.getImageId())){
            imageRequests.get(imageRequest.getImageId()).add(imageRequest);


        }else{
            HashSet<ImageRequest> imageRequestSet = new HashSet<ImageRequest>();
            imageRequestSet.add(imageRequest);

            imageRequests.put(imageRequest.getImageId(), imageRequestSet);
        }
    }

    /**
     * deregisters an image request from this ImageDownloadQuery.
     * @param imageRequest the imageRequest to deregister.
     */
    public void deregisterImageRequest(ImageRequest imageRequest){
        HashSet<ImageRequest> imageRequestSet = imageRequests.get(imageRequest.getImageId());
        imageRequestSet.remove(imageRequest);
        if(!(imageRequestSet.size() > 0) ){
            imageRequests.remove(imageRequest.getImageId());
        }
    }


    /**
     * checks if the ImageDownloadQuery already holds a request matching the given request,
     * @param imageRequest
     * @return true if this ImageDownloadQuery already holds a request matching the given request, false if not.
     */
    public boolean containsMatchingRequest(ImageRequest imageRequest){
        if(imageRequests.containsKey(imageRequest.getImageId())){
        HashSet<ImageRequest> imageRequestSet = imageRequests.get(imageRequest.getImageId());
            for (ImageRequest ir : imageRequestSet) {
                if (ir.getReqWidth() == imageRequest.getReqWidth()) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * resolves an image request by downloading the required image or setting it from the image cache if available.
     * @param imageRequest the imageRequest to resolve.
     * @throws Exception throws an exceptions of the triggered BitmapImageDownlaoder.
     */
    public void resolveImageRequest(ImageRequest imageRequest) throws Exception {

        // if (image already downloaded) downloaded images has requested id   -> get bitmap from downloadedImmge
        imageRequest.showProgressBar(true);
        if (downloadedImages.get(imageRequest.getImageId()) != null  ) {

             imageRequest.setBitmap(downloadedImages.get(imageRequest.getImageId()));
        }
        // else.. if image is already downloading image with required size: register to be set after download, wait for download
        else {
            if (containsMatchingRequest(imageRequest)) {
                 registerImageRequest(imageRequest);
                //do nothing (wait for download)
            } else { // not downloaded and not downloading

                registerImageRequest(imageRequest);

                //insert into imageRequests, start download task
                AsyncBitmapImageDownloader bitmapImageDownloader = (AsyncBitmapImageDownloader) new AsyncBitmapImageDownloader(imageRequest.getReqWidth()).execute(imageRequest.getImageId(), AsyncImageDownloader.THUMB_IMAGE);


                    AsyncTaskResult<Bitmap> downloaderResponse = bitmapImageDownloader.get();
                    if(downloaderResponse.getExceptions() != null){
                        for(Exception e: downloaderResponse.getExceptions()){
                            throw(e);
                        }

                    }else {
                        Log.d("imageDownloadQuery", "downloaded bitmap: " + downloaderResponse.getResult());

                        downloadedImages.put(imageRequest.getImageId(), downloaderResponse.getResult());
                        //for each image Request with same id: set Bitamp

                        for (ImageRequest ir : imageRequests.get(imageRequest.getImageId())) {
                             ir.setBitmap(downloaderResponse.getResult());

                            deregisterImageRequest(ir);
                        }
                    }



            }
        }


    }

}
