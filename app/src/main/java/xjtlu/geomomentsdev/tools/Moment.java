package xjtlu.geomomentsdev.tools;

import android.os.AsyncTask;
import android.util.Log;

import com.amap.api.maps2d.model.LatLng;

import org.ocpsoft.prettytime.PrettyTime;

import java.io.IOException;
import java.net.ConnectException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

/**
 * A class to represent, create, upload and delete Geomoments, having a location, text- and/or - image content, a creator and a timestamp
 * Created by VRLAB_DevThree on 6/23/2016.
 */
public class Moment {

    public static final String MOMENTS_TIMESTAMP_COLUMN = "timestamp";

    private long id;
    private LatLng location;
    private String textContent;
    private ImageHandle imageContent;
    private User Creator;
    private Timestamp timestamp;

    public Moment(long id, double lat, double lng, String textContent, ImageHandle imageContent, User creator, Timestamp timestamp) {

        this.id = id;
        LatLng tmp = new LatLng(lat, lng);
        this.location = tmp;
        this.textContent = textContent;
        this.imageContent = imageContent;
        Creator = creator;
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }

    public LatLng getLocation() {
        return location;
    }
    public double getLatitude(){
        return location.latitude;
    }

    public double getLongitude(){
        return location.longitude;
    }

    public String getTextContent() {
        return textContent;
    }

    public ImageHandle getImageContent() {
        return imageContent;
    }

    public User getCreator() {
        return Creator;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public String getFormattedTimestamp(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(timestamp);

    }

    public String getPrettyTimestamp(){
        PrettyTime prettyTime = new PrettyTime();
        String tmp = prettyTime.format(timestamp);
        return tmp ;
    }

    /**
     * deletes the moment with the given id from the server side database.
     * @param momentId the id of the moment that is to be deleted.
     * @return true if the moment was deleted successfully, false if not.
     * @throws IOException
     * @throws ServerException
     * @throws NumberFormatException
     */
    public static boolean deleteMoment(Long momentId) throws Exception, ServerException, NumberFormatException {
       // AsyncTask<Long, Void, Boolean> deleteMomentTask = new AsyncTask<Long, Void, Boolean>() {
         //   @Override
           // protected Boolean doInBackground(Long... longParams) {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put(HttpRequestHandler.DELETE_MOMENT_PARAMS[0], Long.toString(momentId));

                String deletedMoments = null;
                try {
                    deletedMoments = HttpRequestHandler.post(HttpRequestHandler.DELETE_MOMENT_URL, params);
                    Log.d("deleteMoments" , "response = "  + deletedMoments);

                    int momentsDeleted = Integer.parseInt(deletedMoments.trim());

                    return (momentsDeleted > 0);


                } catch (Exception e) {
                    throw e;
                }

    }


    /**
     * sends a post request to the server, creating a moment that has an image- and text content with the given data.
     * @param creatorId the id of the user that created the moment.
     * @param lat the latitude of the location of the moment.
     * @param lng the longitude of the location of the moment.
     * @param text the text content of the moment.
     * @param imageId the image id of the image content of the moment
     * @return the id of the created moment in the server side database.
     * @throws IOException
     * @throws ServerException
     */
    public static long createMoment(long creatorId, double lat, double lng, String text, long imageId)throws Exception {

        //set params
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[0], Long.toString(creatorId));

        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[1], Double.toString(lat));
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[2], Double.toString(lng));
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[3], text);
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[4], Long.toString(imageId));

        String createMomentResponse = null;

            createMomentResponse = HttpRequestHandler.post(HttpRequestHandler.UPLOAD_MOMENT_URL, params);
            Long result = Long.parseLong(createMomentResponse.trim());
            return result;


    }

    /**
     * sends a post request to the server, creating a moment that has an image content and no text content with the given data.
     * @param creatorId the id of the user that created the moment
     * @param lat the latitude of the location of the moment.
     * @param lng the longitude of the location of the moment.
     * @param imageId the image content of the moment
     * @return the id of the created moment in the server side database.
     * @throws IOException
     * @throws ServerException
     */
    public static long createMoment(long creatorId,  double lat, double lng, long imageId) throws Exception {
        //set params
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[0], Long.toString(creatorId));
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[1], Double.toString(lat));
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[2], Double.toString(lng));
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[4], Long.toString(imageId));

        String createMomentResponse = null;


            createMomentResponse = HttpRequestHandler.post(HttpRequestHandler.UPLOAD_MOMENT_URL, params);
            Long result = Long.parseLong(createMomentResponse.trim());
            return result;


    }


    /**
     * sends a poost request to the server, creating a moment that has a text content and no image content with the given data.
     * @param creatorId the id of the user that created the moment.
     * @param lat the latitude of the location of the moment.
     * @param lng the longitude of the location of the moment.
     * @param text the text content of the moment.
     * @return the id of the created moment in the server side database.
     * @throws ServerException
     * @throws IOException
     * @throws NumberFormatException
     */
    public static long createMoment(long creatorId, double lat , double lng, String text) throws Exception  {
        //set params
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[0], Long.toString(creatorId));
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[1], Double.toString(lat));
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[2], Double.toString(lng));
        params.put(HttpRequestHandler.UPLOAD_MOMENT_PARAMS[3], text);

        String createMomentResponse = null;

            createMomentResponse = HttpRequestHandler.post(HttpRequestHandler.UPLOAD_MOMENT_URL, params);
            Long result = Long.parseLong(createMomentResponse.trim());
            return result;

    }

    public String toString(){
        String textContentString = "null";

        if(textContent != null){
            textContentString = textContent;
        }

        String imageContentString = "null";

        if(imageContent != null){
            imageContentString = Long.toString(imageContent.getId());
        }


        return "Moment: id: " + id + ", textContent = " + textContentString + ", imageContent = " + imageContentString + ", creator: " + getCreator().toString();
    }


}
