package xjtlu.geomomentsdev.tools;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

/**
 * Created by VRLAB_DevThree on 7/8/2016.
 */
public class ImageRequest {
    private long imageId;
    private ImageView imageView;
    private ProgressBar progressBar;
    private int reqWidth;


    public ImageRequest(long imageId, ImageView imageView, ProgressBar progressBar, int reqWidth) {
        this.imageId = imageId;
        this.imageView = imageView;
        this.progressBar = progressBar;
        this.reqWidth = reqWidth;
        showProgressBar(true);

    }

    public void showProgressBar(boolean show){
        if(show){
            progressBar.setVisibility(View.VISIBLE);
        }else{
            progressBar.setVisibility(View.GONE);
        }
    }

    public void setBitmap(Bitmap bitmap){
        imageView.setImageBitmap(bitmap);

        showProgressBar(false);
    }

    public long getImageId() {
        return imageId;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }


    public int getReqWidth() {
        return reqWidth;
    }
}
