package xjtlu.geomomentsdev.tools;

import android.content.Context;
import android.provider.ContactsContract;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by tmp on 6/15/2016.
 */
public class User {
    private long id;
    private String uniqueName;
    private String name;
    private String email;
    private String status;
    private ImageHandle profilePicture;

    /**
     * Constructor
     * @param id
     * @param uniqueName;
     * @param name
     * @param email
     * @param status
     * @param profilePicture
     */
    public User(long id, String uniqueName, String name, String email, String status, ImageHandle profilePicture) {
        this.id = id;
        this.uniqueName = uniqueName;
        this.name = name;
        this.email = email;
        this.status = status;
        this.profilePicture = profilePicture;
    }

    /**
     * Constructor
     * @param id
     * @param name
     * @param status
     * @param profilePicture
     */

    public User(long id, String uniqueName, String name, String status, ImageHandle profilePicture){
        this.id = id;
        this.uniqueName = uniqueName;
        this.name = name;
        this.status = status;
        this.profilePicture = profilePicture;
    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    public ImageHandle getProfilePicture() {
        return profilePicture;
    }

    private static DatabaseHandler userDatabase;

    public static void initializeUserDatabase(Context context){
        userDatabase =   DatabaseHandler.getInstance(context);
    }

    public static DatabaseHandler getUserDatabase(){
        return userDatabase;
    }

    /**
     * parses a User from a given JSONObject as it is included in server responses.
     * @param jObj the JSONObject to parse.
     * @return the User Object parsed from the JSONObject.
     * @throws JSONException
     */
    public static User parseUser(JSONObject jObj) throws JSONException {

        Object nullCheck = jObj.get(DatabaseHandler.USERS_PROFILE_PICTURE);

        ImageHandle userProfilePicture;
        if(nullCheck != JSONObject.NULL) {
            Long imageId = jObj.getLong(DatabaseHandler.USERS_PROFILE_PICTURE);

            userProfilePicture = new ImageHandle(imageId);
        }else {
            userProfilePicture = null;
        }

        //download image if exists

        Long userId = jObj.getLong(DatabaseHandler.USERS_ID);
        String uniqueName = jObj.getString(DatabaseHandler.USERS_UNIQUE_NAME);
        String userName = jObj.getString(DatabaseHandler.USERS_NAME);

        String userEmail = null;
        if(jObj.has(DatabaseHandler.USERS_EMAIL)) {
            userEmail = jObj.getString(DatabaseHandler.USERS_EMAIL);
        }
        String userStatus = jObj.getString(DatabaseHandler.USERS_STATUS);


        return new User(userId, uniqueName, userName, userEmail, userStatus, userProfilePicture);

    }

    /**
     * downloads the User with the given id from the server.
     * @param userId the id of the User to download.
     * @return the downloaded User.
     */
    public static User downloadUser(long userId)  throws Exception, ServerException {
        //set php params
        HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put(HttpRequestHandler.DOWNLOAD_USER_PARAMS[0], Long.toString(userId));

        //send post request
        String downloadUserResponse = null;
        downloadUserResponse = HttpRequestHandler.post(HttpRequestHandler.DOWNLOAD_USER_URL, postParams);
        // parse response to User
        JSONObject jObj = null;
            jObj = new JSONObject(downloadUserResponse);
            return parseUser(jObj);

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setProfilePicture(long uploadedImageId) {
        profilePicture = new ImageHandle(uploadedImageId);
    }




    /**
     * retrieves the user data for the user with the given credentials from server if login credentials are valid.
     * sets the logged in user to the user with the retrieved credentials.
     * @param identifier the identifier, i.e. email address or unique user name of the user to be logged in.
     * @param password the password of the user to be logged in.
     * @return  true if login successful, false if not .
     * @throws IOException
     * @throws ServerException is thrown if a server side exception occurred.
     */
    public static Boolean login(String identifier, String password) throws Exception, ServerException {

        //set params for php script
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(HttpRequestHandler.SIGNIN_PARAMS[0],identifier);
        params.put(HttpRequestHandler.SIGNIN_PARAMS[1], password);

        //send post request to server
        String loginResponse = null;

        loginResponse = HttpRequestHandler.post(HttpRequestHandler.SIGNIN_URL, params);

            User user = parseUser( new JSONObject(loginResponse));

            userDatabase.insertOrUpdateUser(user);
            PreferenceHandler.setLogin(true);
            PreferenceHandler.setLoggedInUser(user.getId());

            return true;

    }

    /**
     * retrieves the currently logged in User using the id stored in sharedPreferences and the user information.
     * stored in an SQLite Database.
     * @return the currently logged in User.
     */
    public static User getLoggedInUser(){
        //get logged in user id from shared preferences, -1 if none found
        long userId = PreferenceHandler.getLoggedInUser();

        //get logged in User from Database if userId is valid
        if(userId == -1){

            return null;
        }else {

            return(userDatabase.getUser(userId));
        }
    }


    /**
     * loges out the currently logged in User.
     */
    public static void logout(){
        User loggedInUser = getLoggedInUser();
        //delete user from SQLite database
        userDatabase.deleteUser(loggedInUser);
        //resets login flag
        PreferenceHandler.setLogin(false);
        //remove logged in user id
        PreferenceHandler.setLoggedInUser(null);

    }


    public String getUniqueName() {
        return uniqueName;
    }

    public String toString(){
        String profilePictureId;
        if(profilePicture == null){
            profilePictureId = "null";

        }else{
            profilePictureId = ""+profilePicture.getId();
        }

        return "id: " + id + ", unique_name: " + uniqueName + ", name: " + name + ", status: " + status + ", email: " + email + ", profilePicture: " + profilePictureId;

    }
}
