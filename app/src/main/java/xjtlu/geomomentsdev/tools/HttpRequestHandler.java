package xjtlu.geomomentsdev.tools;

import android.app.DownloadManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by tmp on 6/21/2016.
 */
public class HttpRequestHandler {
    private static String logTag = "HttpRequestHandler";
    private final static String SERVER_IP = "103.41.134.68";
    private final static String SERVER_URL = "http://" + SERVER_IP + "/geomoments/";

    // php login script info
    public final static String SIGNIN_URL = SERVER_URL + "signin_user.php";
    public final static String[] SIGNIN_PARAMS = {"email", "password"};


    // php register script info
    public final static String SIGNUP_URL = SERVER_URL + "signup_user.php";
    public final static String[] SIGNUP_PARAMS = {"unique_name", "email", "password"};

    //php download user info
    public final static String DOWNLOAD_USER_URL = SERVER_URL + "download_user.php";
    public final static String[] DOWNLOAD_USER_PARAMS = {"userId"};

    //php update user info
    public final static String UPDATE_USER_URL = SERVER_URL + "update_user.php";
    public final static String[] UPDATE_USER_PARAMS = {"userId", "unique_name", "name", "image", "status", "email"};


    //php imageUpload script info
    public final static String UPLOAD_IMAGE_URL = SERVER_URL + "upload_image.php";
 //   public final static String UPLOAD_IMAGE_URL = SERVER_URL + "upload_image_test.php";
    public final static String[] UPLOAD_IMAGE_PARAMS = {"filename", "image"};

    //php imageDownload script info
    public final static String DOWNLOAD_IMAGE_URL = SERVER_URL + "download_image.php";
    public final static String[] DOWNLOAD_IMAGE_PARAMS = {"imageId", "thumbOrFull"};


    //php insertMoment script info
    public final static String UPLOAD_MOMENT_URL = SERVER_URL + "upload_moment.php";
    public final static String[] UPLOAD_MOMENT_PARAMS = {"creator", "lat", "lng", "text", "image"};

    //php download filtered moments script info
    public final static String DOWNLOAD_MOMENTS_URL = SERVER_URL + "download_moments.php";
    public final static String[] DOWNLOAD_MOMENTS_PARAMS = {"whereStmt", "orderByStmt"};

    //php delete moment info
    public final static String DELETE_MOMENT_URL = SERVER_URL + "delete_moment.php";
    public final static String[] DELETE_MOMENT_PARAMS = {"momentId"};

    //php checkUsername available info
    public final static String USERNAME_AVAILABLE = SERVER_URL + "unique_name_available.php";
    public final static String[] USERNAME_AVAILABLE_PARAMS = {"unique_name"};


    //php check email available info
    public final static String EMAIL_AVAILABLE = SERVER_URL + "email_available.php";
    public final static String[] EMAIL_AVAILABLE_PARAMS = {"email"};

    public final static String RESPONSE_ERROR = "\"<%$#ERROR#$%>\"";



    public static String getMomentRangeFilter(double lat, double lng){

        String range = String.format("%.2f",(float) ( ((float) LocationHandler.RANGE_IN_METERS)/1000));

        return  "(111.1111  * DEGREES(ACOS(COS(RADIANS(" + lat + "))\n" +
                "         * COS(RADIANS(" + DatabaseHandler.MOMENTS_LAT + "))\n" +
                "         * COS(RADIANS(" + lng + " - " + DatabaseHandler.MOMENTS_LNG + "))\n" +
                "         + SIN(RADIANS(" + lat + "))\n" +
                "         * SIN(RADIANS(" + DatabaseHandler.MOMENTS_LAT + "))))) <" + range;
    }

    public static String post(String url, HashMap<String, String> params) throws Exception, ServerException {
        Log.d(logTag, "url = " + url);

        OkHttpClient client = new OkHttpClient();
        FormBody.Builder builder = new FormBody.Builder();

        for (String key : params.keySet()) {
            builder.add(key, params.get(key));
        }

        RequestBody formBody = builder.build();

        Request request = new Request.Builder().url(url).post(formBody).build();

     //   Log.d(logTag, "request: " + request.toString());
    //    Log.d(logTag, "post: " + builder.toString());
     //   Log.d(logTag, "params: " + params);

            Response response = null;
        if(! InternetConnectionHandler.isConnected()){
            NoConnectionException e = new NoConnectionException("no connection");
            throw e;
        }

        try {


            response = client.newCall(request).execute();
            String responseString = response.body().string();
        //    Log.d(logTag, "response: " + responseString);

            if(responseString.equals(RESPONSE_ERROR)){
                throw(new ServerException());
            }else {

                return responseString;
            }
        }catch(Exception e){
            throw e;
        }
    }

    public static String post(String url) throws Exception {
        Log.d(logTag, "url = " + url);

        OkHttpClient client = new OkHttpClient();
        FormBody.Builder builder = new FormBody.Builder();


        RequestBody formBody = builder.build();

        final Request request = new Request.Builder().url(url).post(formBody).build();

      //  Log.d(logTag, "request: " + request.toString());
      //  Log.d(logTag, "post: " + builder.toString());

        final Response[] response = {null};
        if(! InternetConnectionHandler.isConnected()){
            NoConnectionException e = new NoConnectionException("no connection");
            throw e;
        }

           try{
                Call call = client.newCall(request);
                response[0] = client.newCall(request).execute();
                String responseString = response[0].body().string();
         //       Log.d(logTag, "response: " + responseString);
               if(responseString.equals(RESPONSE_ERROR)){
                   throw(new IOException());
               }else {
                   return responseString;
               }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }




        //TODO: handle network connection exception
    }



}
