package xjtlu.geomomentsdev.tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.CameraPosition;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

import xjtlu.geomomentsdev.FullImageActivity;
import xjtlu.geomomentsdev.MainNavigationActivity;
import xjtlu.geomomentsdev.R;
import xjtlu.geomomentsdev.ShowUserActivity;

/**
 * Created by VRLAB_DevThree on 6/28/2016.
 */
public class MomentArrayAdapter extends ArrayAdapter {


    private ImageDownloadQuery imageDownloadQuery;
    private HashMap<View, Marker> markerMap;

    ViewGroup viewContainer;
    Long loggedInUserId;

    private HashMap<Long, Integer> momentPosition = new HashMap<Long, Integer>();

    public MomentArrayAdapter(Context context, ArrayList<Moment> moments, long loggedInUserId, ViewGroup container) {
      super(context, R.layout.list_item_moment, moments);
        momentPosition = new HashMap<Long, Integer>();
        int i = 0;
        for(Moment m : moments){
            momentPosition.put(m.getId(),i);
            i++;
        }
        this.loggedInUserId = loggedInUserId;
        this.viewContainer = container;
        this.imageDownloadQuery = new ImageDownloadQuery();
        this.markerMap = new HashMap<View, Marker>();
    }

    public MomentArrayAdapter(Context context, ArrayList<Moment> moments, long loggedInUserId) {
        super(context, R.layout.list_item_moment, moments);
        momentPosition = new HashMap<Long, Integer>();
        int i = 0;
        for(Moment m : moments){
            momentPosition.put(m.getId(),i);
            i++;
        }
        this.loggedInUserId = loggedInUserId;
        this.viewContainer = null;
        this.imageDownloadQuery = new ImageDownloadQuery();
        this.markerMap = new HashMap<View, Marker>();
    }



    public int getMomentPosition(long MomentId){
        if(momentPosition == null){
            Log.d("getMomentPosition" ,"momentPosition is null");
        }
        Log.d("xx1", "gettingMomentPosition for: " + MomentId +" found: " + momentPosition.get(MomentId));
        return momentPosition.get(MomentId);
    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());

        final View customItem;
        if(convertView != null){ // if view is recycled


            // remover markers on recycled mapView
            if(markerMap.containsKey(convertView)){

                markerMap.get(convertView).destroy();
                markerMap.remove(convertView);

            }
            loggedInUserId =  User.getLoggedInUser().getId();
            //reset recycled view
            ((Button) convertView.findViewById(R.id.button_moment_item_show_location)).setText(getContext().getResources().getString(R.string.show_location));

            ImageView creatorProfilePicture = (ImageView) convertView.findViewById(R.id.image_view_user_preview_profile);

            creatorProfilePicture.setImageBitmap(ImageHandle.bitmapFromVectorAsset(getContext(), R.drawable.ic_face_grey_96dp, R.color.lightGrey));


            MapView itemMapView = (MapView) convertView.findViewById(R.id.map_view_moment_item_map);
            itemMapView.setVisibility(View.GONE);

            TextView mapLabel = (TextView) convertView.findViewById(R.id.text_view_moment_item_content_text);
            mapLabel.setVisibility(View.GONE);


            ImageView imageContent = (ImageView) convertView.findViewById(R.id.image_view_moment_item_content_image);

            //try to recycle imageContent
            try{
                ((BitmapDrawable)imageContent.getDrawable()).getBitmap().recycle();
            }catch(Exception e){
                e.printStackTrace();
            }
                imageContent.setImageBitmap(ImageHandle.bitmapFromVectorAsset(getContext(), R.drawable.ic_photo_black_96dp, R.color.lightGrey));


            customItem = convertView;


        }else{//not recycled -> inflate new view

            customItem = inflater.inflate(R.layout.list_item_moment, parent, false);
            customItem.setId(position);
        }




        final Moment itemMoment = (Moment) getItem(position);
        //set moment creator info
        if (itemMoment.getCreator() != null) {


            View creatorView = customItem.findViewById(R.id.user_preview_moment_item_creator);

            // set creator profile picture if available
            if (itemMoment.getCreator().getProfilePicture() != null) {

                ImageView creatorProfilePicture = (ImageView) creatorView.findViewById(R.id.image_view_user_preview_profile);
                ProgressBar creatorProgressBar = (ProgressBar) creatorView.findViewById(R.id.progress_bar_user_preview_progress);

                showCreatorImage(itemMoment.getCreator().getProfilePicture().getId(), creatorProfilePicture, creatorProgressBar ,56);
            }

            //set creator name
            final TextView creatorName = (TextView) creatorView.findViewById(R.id.text_view_user_preview_name);
            creatorName.setText(Html.fromHtml(itemMoment.getCreator().getName() + " <small>(" + itemMoment.getCreator().getUniqueName() + ")</small>"));


            //set click user event
            //if has loggedInUser and viewContainer => created from BrowserFragment
            if (loggedInUserId != null && viewContainer != null) {
                creatorView.setOnClickListener(new View.OnClickListener() {

                                                   @Override
                                                   public void onClick(View v) {
                                                       if (itemMoment.getCreator() != null) {
                                                           // logged in user is clicked user and viewContainer available, go to user Fragment
                                                           if (loggedInUserId != null && itemMoment.getCreator().getId() == loggedInUserId.longValue()) {
                                                               ((ViewPager) viewContainer).setCurrentItem(MainNavigationActivity.SectionsPagerAdapter.POSITION_PROFILE_FRAGMENT);

                                                           } else {
                                                               // if clicked user is not logged in user, start showUserActivity

                                                               Intent showUser = new Intent(getContext(), ShowUserActivity.class);
                                                               showUser.putExtra(ShowUserActivity.INTENT_EXTRA_USER_ID, itemMoment.getCreator().getId());
                                                               getContext().startActivity(showUser);
                                                           }
                                                       }

                                                   }
                                               }


                );
            }
            //if has no loggedInUser or has no viewContainer => created from MomentBrowserActivity, which is in turn created by a showUser activity

            else {
                creatorView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((Activity) getContext()).finish(); //> finish MomentBrowserActivity => return to showUSer activity
                    }
                });
            }


        }

        //set text content if available
        TextView textContent = (TextView) customItem.findViewById(R.id.text_view_moment_item_content_text);
        if (itemMoment.getTextContent() != null) {

            textContent.setText(itemMoment.getTextContent());
            textContent.setVisibility(View.VISIBLE);
        } else {
            textContent.setVisibility(View.GONE);
        }

        //set image content if available
        final ProgressBar imageProgress = (ProgressBar) customItem.findViewById(R.id.progress_bar_moment_item_image_progress);
        final ImageView imageView = (ImageView) customItem.findViewById(R.id.image_view_moment_item_content_image);

        if (itemMoment.getImageContent() != null) {


            // set click image content event
            imageView.setOnClickListener(new View.OnClickListener() {

                                             @Override
                                             public void onClick(View v) {

                                                 if (itemMoment.getImageContent() != null) {
                                                     Intent fullImage = new Intent(getContext(), FullImageActivity.class);
                                                     fullImage.putExtra(FullImageActivity.INTENT_EXTRA_IMAGE_ID, itemMoment.getImageContent().getId());
                                                     getContext().startActivity(fullImage);
                                                 } else {
                                                     Toast.makeText(getContext(), getContext().getString(R.string.error_no_image_selcted), Toast.LENGTH_LONG);

                                                 }
                                             }
                                         }

            );
            imageView.setVisibility(View.VISIBLE);

            itemMoment.getImageContent().showThisThumbImageInView(imageView, imageProgress);

        } else {
            imageProgress.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
        }


        //show timestamp and distance
        TextView timestampAndLocation = (TextView) customItem.findViewById(R.id.text_view_moment_item_timestamp);

        float distance = LocationHandler.getMyDistanceTo(itemMoment.getLatitude(), itemMoment.getLongitude());
        String distanceString = "";

        //convert distance to km if large
        if(distance > 999){
            distanceString = String.format("%.1f", distance/1000) + "km away";
        }else{
            distanceString = String.format("%.0f", distance) + "m away";
        }

        timestampAndLocation.setText(itemMoment.getPrettyTimestamp() + ", " + distanceString );


        //set showLocationButton
        Button showLocationButton = (Button) customItem.findViewById(R.id.button_moment_item_show_location);


        showLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                MapView itemMapView = (MapView) ((View) v.getParent()).findViewById(R.id.map_view_moment_item_map);
                if (itemMapView.getVisibility() == View.GONE) {
                    //show map and change button to hide location map is hidden
                    itemMapView.onCreate(null);

                    itemMapView.setVisibility(View.VISIBLE);

                    TextView mapLabel = (TextView) ((View) v.getParent()).findViewById(R.id.text_view_moment_item_content_location_label);
                    mapLabel.setVisibility(View.VISIBLE);

                    AMap itemAMap = itemMapView.getMap();

                    itemAMap.getUiSettings().setMyLocationButtonEnabled(false);
                    itemAMap.setMyLocationEnabled(false);
                    itemAMap.setTrafficEnabled(false);
                    itemAMap.getUiSettings().setAllGesturesEnabled(false);
                    itemAMap.getUiSettings().setZoomControlsEnabled(false);


                    itemAMap.moveCamera(CameraUpdateFactory.zoomTo(18));


                    createMarkerAtLocation(getContext(), itemMoment.getLocation(), itemAMap);
                    ((Button) v).setText(getContext().getResources().getString(R.string.hide_location));

                } else {
                    //hide map and change button to show location if map is already showing

                    itemMapView.setVisibility(View.GONE);
                    TextView mapLabel = (TextView) ((View) v.getParent()).findViewById(R.id.text_view_moment_item_content_location_label);
                    mapLabel.setVisibility(View.GONE);
                    ((Button) v).setText(getContext().getResources().getString(R.string.show_location));


                }
            }


            /**
             * creates a marker at the given location
             * @param context
             * @param latLng the location where the marker is to be created
             * @param aMap the map on wich the marker is to be created
             */
            private void createMarkerAtLocation(Context context, LatLng latLng, AMap aMap) {

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(ImageHandle.bitmapFromVectorAsset(context, R.drawable.ic_place_white_24dp, R.color.black)));
                aMap.addMarker(markerOptions);
                aMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(latLng, aMap.getCameraPosition().zoom)));

            }

        });


        //setup show on map button
        Button showOnMapButton = (Button) customItem.findViewById(R.id.button_moment_item_show_on_map);
        showOnMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set moment as active moment
                ((MainNavigationActivity)(viewContainer.getContext())).setActiveMoment(itemMoment.getId());
                //go to map view fragment
                ((ViewPager) viewContainer).setCurrentItem(MainNavigationActivity.SectionsPagerAdapter.POSITION_MAP_VIEW_FRAGMENT);



            }
        });


        if (loggedInUserId != null && viewContainer != null) {
            //if adapter is created by browser fragment: show on map button is available, show location button is not
            showLocationButton.setVisibility(View.GONE);
            showOnMapButton.setVisibility(View.VISIBLE);
        }else{
            //if adapter is created by browser activity: show location button is available, show location button is not
            showLocationButton.setVisibility(View.VISIBLE);
            showOnMapButton.setVisibility(View.GONE);

        }



        return customItem;

    }


    /**
     * function to show a an image in a given imagView, using the imageDownloadCache
     * @param imageId the id of the image to show
     * @param imageView the image view to show the image in
     * @param progressBar the progress bar to show the progress of downloading the image if necessary
     * @param reqWidth the required width of the image.
     */
    public void showCreatorImage(Long imageId, ImageView imageView, ProgressBar progressBar, int reqWidth) {



        ImageRequest imageRequest = new ImageRequest(imageId, imageView, progressBar, reqWidth );

        try {
            imageDownloadQuery.resolveImageRequest(imageRequest);
        } catch (Exception e) {
            //TODO: replace string with xml string value
            Toast.makeText(getContext(), "image could not be downloaded",  Toast.LENGTH_LONG).show();
        }


    }

}
