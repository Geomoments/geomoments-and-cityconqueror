package xjtlu.geomomentsdev.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import xjtlu.geomomentsdev.R;

/**
 * Created by VRLAB_DevThree on 7/20/2016.
 */
public class InternetConnectionHandler  extends BroadcastReceiver {
    private static AlertDialog alertDialog = null;
    private static Activity waitingActivity = null;
    private static Context alertContext = null;
    private static ConnectivityManager connectivityManager = null;


    public static void setContext(Context newContext){
        alertContext = newContext;
        connectivityManager = (ConnectivityManager) newContext.getSystemService(Context.CONNECTIVITY_SERVICE);
     }

    public static boolean wifiAvailable(){
        NetworkInfo info = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if(info != null){
            return info.isConnected();
        }
        return false;
    }


    public static boolean mobileAvailable(){
        NetworkInfo info = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);


        if(info != null){
            return info.isConnected();
        }
        return false;
    }

    public static boolean isConnected(){
        return wifiAvailable() || mobileAvailable();
    }

    public static void waitForConnection(Context context, Activity activity){
        setContext(context);
        waitingActivity = activity;
        AlertDialog.Builder networkAlert =  new AlertDialog.Builder(context);
        networkAlert.setTitle(context.getString(R.string.please_connect_to_internet));
        networkAlert.setIcon(android.R.drawable.ic_dialog_alert);
        networkAlert.setCancelable(false);
        if(alertDialog == null){
            alertDialog = networkAlert.show();
            alertDialog.show();
        }
        waitingActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);



    }


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("InternetConnection", "Network connectivity change");
       connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);


        AlertDialog.Builder networkAlert =  new AlertDialog.Builder(alertContext);
        networkAlert.setTitle(context.getString(R.string.please_connect_to_internet));
        networkAlert.setIcon(android.R.drawable.ic_dialog_alert);
        networkAlert.setCancelable(false);

        if(isConnected() && alertDialog != null){

                    alertDialog.dismiss();
                    alertDialog = null;
            if(waitingActivity != null){

                waitingActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }


                }

        }

}
