package xjtlu.geomomentsdev.tools;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import xjtlu.geomomentsdev.R;

/**
 * Created by VRLAB_DevThree on 6/23/2016.
 */
public class

ImageHandle {

    private long id;
    // private Bitmap imageBitmap;


    public ImageHandle(long id) {
        this.id = id;
        //   this.imageBitmap = imageBitmap;
    }

    public long getId() {
        return (id);
    }

    public static void showThumbImageInView(long imageId, ImageView imageView) {

        AsyncImageDownloader imageDownloader = new AsyncImageDownloader();
        imageDownloader.execute(imageId, AsyncImageDownloader.THUMB_IMAGE, imageView);


    }

    public static void showThumbImageInView(long imageId, ImageView imageView, ProgressBar progressBar) {

        AsyncImageDownloader imageDownloader = new AsyncImageDownloader(progressBar);
        imageDownloader.execute(imageId, AsyncImageDownloader.THUMB_IMAGE, imageView);


    }

    public static void showFullImageInView(long imageId, ImageView imageView) {

        AsyncImageDownloader imageDownloader = new AsyncImageDownloader();
        imageDownloader.execute(imageId, AsyncImageDownloader.FULL_IMAGE, imageView);

    }

    public static void showFullImageInView(long imageId, ImageView imageView, ProgressBar progressBar) {

        AsyncImageDownloader imageDownloader = new AsyncImageDownloader(progressBar);
        imageDownloader.execute(imageId, AsyncImageDownloader.FULL_IMAGE, imageView);

    }

    public void showThisThumbImageInView(ImageView imageView) {
        AsyncImageDownloader imageDownloader = new AsyncImageDownloader();
        imageDownloader.execute(this.id, AsyncImageDownloader.THUMB_IMAGE, imageView);

    }

    public void showThisThumbImageInView(ImageView imageView, ProgressBar progressBar) {
        AsyncImageDownloader imageDownloader = new AsyncImageDownloader(progressBar);
        imageDownloader.execute(this.id, AsyncImageDownloader.THUMB_IMAGE, imageView);

    }
    public void showThisThumbImageInViewAndMap(ImageView imageView, ProgressBar progressBar, HashMap<Long, Bitmap> imageMap) {
        AsyncImageDownloader imageDownloader = new AsyncImageDownloader(progressBar, imageMap);
        imageDownloader.execute(this.id, AsyncImageDownloader.THUMB_IMAGE, imageView);

    }



    public void showThisFullImageInView(ImageView imageView) {
        AsyncImageDownloader imageDownloader = new AsyncImageDownloader();
        imageDownloader.execute(this.id, AsyncImageDownloader.FULL_IMAGE, imageView);

    }

    public void showThisFullImageInView(ImageView imageView, ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
        AsyncImageDownloader imageDownloader = new AsyncImageDownloader(progressBar);
        imageDownloader.execute(this.id, AsyncImageDownloader.FULL_IMAGE, imageView);

    }
    /*
    public static Bitmap getBitmapThumbForImageView(long imageId, ImageView imageView, ProgressBar progressBar){
        final String logTag = "getBitampThumbp";
        Log.d(logTag, "called");
        AsyncBitmapImageDownloader bitmapImageDownloader = (AsyncBitmapImageDownloader) new AsyncBitmapImageDownloader(imageView);
        try {
            return(bitmapImageDownloader.get());
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e1) {
            e1.printStackTrace();
        }
        return(null);

    }*/
    /**
     * from: http://stackoverflow.com/questions/33325728/android-google-maps-marker-released-unknown-imagedata-reference
     * @param context
     * @param vectorAssetId
     * @return
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static Bitmap bitmapFromVectorAsset(Context context, int vectorAssetId, int tintColorId){
        VectorDrawable vectorDrawable = (VectorDrawable) ContextCompat.getDrawable(context, vectorAssetId);
        vectorDrawable.setTint(tintColorId);
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getMinimumHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }



    public static Bitmap getSampleImage(String filepath, int reqWidth) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;


        BitmapFactory.decodeFile(filepath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;


        return BitmapFactory.decodeFile(filepath, options);
    }


    /**
     * reads the image at the specified path and encodes it to a String.
     * @param imgPath the filepath of the image to encode.
     * @return String representation of the encoded image.
     */
    public static String encodeImageToString(String imgPath){
        BitmapFactory.Options options = null;
        options = new BitmapFactory.Options();
        // options.inSampleSize = 3;

        Bitmap bitmap = BitmapFactory.decodeFile(imgPath, options);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

        byte[] byte_arr = stream.toByteArray();

        return Base64.encodeToString(byte_arr, 0);

    }



    public static int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        Log.d("imageDownloadSample", "height: " + height + " width: " + width);
        float h = new Float(height);
        float w = new Float(width);
        float ratio = h/w;
        Log.d("imageDownloadSample", "ratio: "+ ratio);



        int reqHeight = (int) (ratio * reqWidth);
        Log.d("imageDownloadSample", "ReqHight: " + reqHeight + " ReqWidth: " +reqWidth);
        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    /**
     * decodes an image string to a bitmap with the required width
     * @param encodedImage
     * @param reqWidth
     * @return
     */
    public static Bitmap decodeSampleImage(String encodedImage, int reqWidth) throws IllegalArgumentException{
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Log.d("decodeSampleImage", "encodedImage = '"+ encodedImage + "'");
        byte[] byte_arr = Base64.decode(encodedImage, 0);

        BitmapFactory.decodeByteArray(byte_arr, 0, byte_arr.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;


        return BitmapFactory.decodeByteArray(byte_arr, 0, byte_arr.length, options);
    }

    /**
     * from: http://stackoverflow.com/questions/18573774/how-to-reduce-an-image-file-size-before-uploading-to-a-server
     * @param path
     * @param DESIREDWIDTH
     * @param DESIREDHEIGHT
     * @return
     */
    public static String decodeFile(String path,int DESIREDWIDTH, int DESIREDHEIGHT) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }

            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/TMMFOLDER");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }

            String s = "tmp.png";

            File f = new File(mFolder.getAbsolutePath(), s);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;

    }
}
