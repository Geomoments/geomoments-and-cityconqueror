package xjtlu.geomomentsdev;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import xjtlu.geomomentsdev.tools.InternetConnectionHandler;
import xjtlu.geomomentsdev.tools.LocationHandler;
import xjtlu.geomomentsdev.tools.LocationDependentActivity;
import xjtlu.geomomentsdev.tools.Moment;
import xjtlu.geomomentsdev.tools.MomentArrayAdapter;
import xjtlu.geomomentsdev.tools.NoConnectionException;

/**
 * A fragment used as p[art of the MainNavigationActivity to browse the Geomoments nearby on a Map.
 * Created by VRLAB_DevThree on 6/28/2016.
 */
public class BrowserFragment extends Fragment implements LocationDependentActivity {
    private static final String ARGUMENT_LOGGED_IN_USER_ID = "loggedInUserId";
    public MomentArrayAdapter momentArrayAdapter;
    private View rootView;
    private Long loggedInUserId;
    private ViewGroup viewContainer;
    private Long activeMoment;
    private ListView momentListView;
    private TextView noMomentsTextView;

    public BrowserFragment() {

    }

    public static BrowserFragment newInstance(Long loggedInUserId) {

        BrowserFragment fragment = new BrowserFragment();


        Bundle args = new Bundle();

        args.putLong(ARGUMENT_LOGGED_IN_USER_ID, loggedInUserId);
        fragment.setArguments(args);

        return (fragment);

    }

    @Override
    public void onDestroyView() {
        LocationHandler.dropLocationDependentActivity(this);
        super.onDestroyView();

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();

        //get logged in user if available
        if (args != null && args.containsKey(ARGUMENT_LOGGED_IN_USER_ID)) {
            loggedInUserId = args.getLong(ARGUMENT_LOGGED_IN_USER_ID);
        } else {
            loggedInUserId = null;
        }

        this.viewContainer = container;
        rootView = inflater.inflate(R.layout.fragment_moment_browser, container, false);


        momentArrayAdapter = null;
        momentListView = (ListView) rootView.findViewById(R.id.list_view_moment_browser_content);
        noMomentsTextView = (TextView) rootView.findViewById(R.id.text_view_moment_browser_no_moments);

        //set refresh action
        final SwipeRefreshLayout swipeRefresher = (SwipeRefreshLayout) rootView.findViewById(R.id.siwpe_refresh_moment_browser_content);
        swipeRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                plotData(true);

                swipeRefresher.setRefreshing(false);
            }
        });

        LocationHandler.registerLocationDependentActivity(this);


        return rootView;
    }


    /**
     * scrolls to the active moment in the list view
     */
    public void scrollToActiveMoment() {
        activeMoment = ((MainNavigationActivity) (viewContainer.getContext())).takeActiveMoment();
        if (activeMoment != null) {
            Log.d("scrollTo", "momentArrayAdapter is null");
            momentListView.setSelection(momentArrayAdapter.getMomentPosition(activeMoment));
        }
        activeMoment = null;

    }


    /**
     * gets the nearby moments from the LocationHandler and plots them on the map.
     *
     * @param forceUpdate true if the nearby moments should be forced to be downloaded from the server, false if the current nearbyMoments of the LocationHandler should be used.
     */
    public void plotData(boolean forceUpdate) {

        if (LocationHandler.getInstance() != null && LocationHandler.getMyLat() != null && LocationHandler.getMyLng() != null) {


            try {
                setListViewContent((LocationHandler.getMomentsNearby(this, forceUpdate)));
            } catch (Exception e) {
                handleGetMomentsException(e);
            }


        }

    }

    /**
     * sets the given moment list to be displayed in the list view of this activity
     *
     * @param downloadedMoments the moments to display in the list view
     */
    public void setListViewContent(ArrayList<Moment> downloadedMoments) {

        ((ProgressBar) rootView.findViewById(R.id.progress_bar_moment_browser_progress)).setVisibility(View.VISIBLE);
        momentListView.setVisibility(View.GONE);
        noMomentsTextView.setVisibility(View.GONE);
        if (downloadedMoments != null && downloadedMoments.size() > 0) {

            momentArrayAdapter = new MomentArrayAdapter(rootView.getContext(), downloadedMoments, loggedInUserId, viewContainer);
            momentListView.setAdapter(momentArrayAdapter);
            momentListView.setVisibility(View.VISIBLE);
            scrollToActiveMoment();

        } else {

            momentListView.setVisibility(View.GONE);
            noMomentsTextView.setVisibility(View.VISIBLE);
        }
        ((ProgressBar) rootView.findViewById(R.id.progress_bar_moment_browser_progress)).setVisibility(View.GONE);
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (LocationHandler.isDeprecated(this)) {
                plotData(false);
            }

            scrollToActiveMoment();
        }
    }

    /**
     * shows the given moment MapViewFragment.
     *
     * @param moment the moment to show in the MapViewFragment.
     */
    public void showOnMap(Moment moment) {

        // ((MainNavigationActivity)(container.getContext())).().showMomentInBrowser(moment.getId());
        ((MainNavigationActivity) (viewContainer.getContext())).setActiveMoment(moment.getId());
        ((ViewPager) viewContainer).setCurrentItem(MainNavigationActivity.SectionsPagerAdapter.POSITION_MAP_VIEW_FRAGMENT);


    }

    @Override
    public void locationDependentRefresh() {
        plotData(false);
    }

    @Override
    public void manageMomentsDownloadException(Exception e) {
        handleGetMomentsException(e);

    }


    /**
     * shows a toast notification that moments could not be retrieved
     *
     * @param e
     */
    public void handleGetMomentsException(Exception e) {
        if (e instanceof NoConnectionException) {
            handleException((NoConnectionException) e);
        } else {
            e.printStackTrace();
            //TODO:: replace text with string value
            Toast.makeText(getContext(), "could not get nearby moments", Toast.LENGTH_SHORT).show();
        }
    }

    public void handleException(NoConnectionException e){
        e.printStackTrace(); InternetConnectionHandler.waitForConnection(getContext(), getActivity());
    }

}
