package xjtlu.geomomentsdev;

import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import xjtlu.geomomentsdev.tools.ImageHandle;
import xjtlu.geomomentsdev.tools.InternetConnectionHandler;

public class FullImageActivity extends AppCompatActivity {
    public static String INTENT_EXTRA_IMAGE_ID = "IMAGE_ID";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);

        InternetConnectionHandler.setContext(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            if(extras.containsKey(INTENT_EXTRA_IMAGE_ID)){
                ImageView fullImageView = (ImageView) findViewById(R.id.image_view_full_image_image);
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_full_image_progress);

                ImageHandle imageHandle = new ImageHandle(extras.getLong(INTENT_EXTRA_IMAGE_ID));


                imageHandle.showThisFullImageInView(fullImageView, progressBar);
            }
            else{
                Toast.makeText(this, getString(R.string.error_something_went_wrong), Toast.LENGTH_LONG);
            }
        }else{
            Toast.makeText(this, getString(R.string.error_something_went_wrong), Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onDestroy(){
        ImageView fullImageView = (ImageView) findViewById(R.id.image_view_full_image_image);
        try{
            ((BitmapDrawable)fullImageView.getDrawable()).getBitmap().recycle();

        }catch(Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
